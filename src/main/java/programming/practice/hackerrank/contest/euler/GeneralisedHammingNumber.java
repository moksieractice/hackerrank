package programming.practice.hackerrank.contest.euler;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Project Euler #204
 * https://projecteuler.net/problem=204
 * https://www.hackerrank.com/contests/projecteuler/challenges/euler204/problem
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class GeneralisedHammingNumber {

    public static void main(String[] args) {
//        solution();
        test();
    }

    private static void solution() {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        int k = in.nextInt();
        long result = solveFourth(n, k);
        System.out.println(result);
        in.close();
    }


    /**
     * Instead of checking a number to see whether it fits the criteria or not, just construct and count the numbers less
     * than N from the prime factors less than K by iterative multiplication.
     *
     * @param pN
     * @param pK
     * @return
     */
    public static long solveFourth(final long pN, final long pK) {
        Long[] primes = new Long[]{2L,3L,5L,7L,11L,13L,17L,19L,23L,29L,31L,37L,41L,43L,47L,53L,59L,61L,67L,71L,73L,79L,83L,89L,97L};

        Set<Long> primeSet = new HashSet<>();
        for (int i = 0; i<primes.length; i++) {
            if (primes[i] <= pK) {
                primeSet.add(primes[i]);
            }
        }

        int primeCount = 0;
//        int multiplicationCount = 0;
        Set<Long> resultSet = new HashSet<>();
        resultSet.add(1L);
        boolean shouldContinue = true;
        while (shouldContinue) {
            Set<Long> newResultSet = new HashSet<>(primeSet.size()*resultSet.size(), 1.0f);
            for (Long result : resultSet) {
                for (Long prime : primeSet) {
                    Long multiplication = prime * result;
//                    multiplicationCount++;
                    if (multiplication <= pN) {
                        newResultSet.add(multiplication);
                    }
                }
            }
            shouldContinue = !newResultSet.isEmpty();
            System.out.println(newResultSet);
            primeCount += newResultSet.size();
            System.out.println(primeCount);
            resultSet = newResultSet;
        }

//        System.out.println("Total multiplication "+multiplicationCount);
        return primeCount+1;
    }

    public static long solveThird(final long pN, final long pK) {
        long count = Long.min(pN, pK);
        if (pN > pK) {
            boolean kIsOdd = ((pK & 0x1) == 1);
            long initialDivisor = kIsOdd? pK : pK - 1;
            for (long n=pK+1; n <= pN; n++)  {
                long dividend = ((n & 0x1) == 0)? n>>Long.numberOfTrailingZeros(n) : n;
                long divisor = initialDivisor;
                while (dividend > pK && divisor > 1) {
                    if (dividend % divisor == 0) {
                        dividend = dividend/divisor;
                    }
                    else {
                        divisor -= 2;
                    }
                }
                if (dividend <= pK) {
                    count++;
                }
            }
        }
// 0, 0, 4183, 589654
        return count;
    }

    /**
     * Observation#1: if n is odd, only an odd number can divide it
     * Observation#2: For all k, such that sqrt(n) < k <= K the division will result in zero quotient and non zero remainder
     * Observation#3: No division may be needed for Power of 2s, though there will be only 63 of them at most
     * @param pN
     * @param pK
     * @return
     */
    public static long solveSecond(final long pN, final long pK) {
        long count = Long.min(pN, pK);
        if (pN > pK) {
            for (long n = pK+1; n <= pN; n++) {
                boolean success = checkPrimeFactors(n, Long.min(pK, (long) Math.sqrt(n)), pK);
                if (success) {
                    count++;
                }
            }
        }
// 0, 0, 6522 (6370)
        return count;
    }

    /**
     * For all the number less that N check if all of its prime factors are less than or equal to K. Count all such
     * numbers.
     * Consideration: if N <= K, then it is certain that all the numbers <= N have all prime factors <= K. For this
     * case the result is N.
     * @param pN
     * @param pK
     * @return
     */
    private static long solveFirst(final long pN, final int pK) {
        long count = Long.min(pN, pK);
        if (pN > pK) {
            for (long n = pK+1; n <= pN; n++) {
                boolean success = checkPrimeFactors(n, pK, pK);
                if (success) {
                    count++;
                }
            }
        }
// 0, 0, 6315
// 0, 0, 6490 (6075)
        return count;
    }

    private static boolean checkPrimeFactors(final long pN, final long pInitialDivisor, final long pK) {
        boolean nIsOdd = (pN & 0x1) == 1;
        long divisor = pInitialDivisor;
        long decrementer = 1L;
        if (nIsOdd) {
            if ((pInitialDivisor & 0x1) == 0) {
                divisor = pInitialDivisor - 1;
            }
            decrementer = 2L;
        }
        long dividend = pN;
        while (dividend > pK && divisor > 1) {
            if (dividend % divisor == 0) {
                dividend = dividend/divisor;
                if ((dividend & 0x1) == 1) {
                    decrementer = 2L;
                    if ((divisor & 0x1) == 0) {
                        divisor--;
                    }
                }
            }
            else {
                divisor -= decrementer;
            }
        }

        return dividend <= pK;
    }

    private static void test() {
        long[][] testInput = new long[][]{
//            new long[] {15L, 5L}, //1:0
//            new long[] {4L, 5L},  //1:0
//            new long[] {100000000L, 5L}, //1:6315 2:6522
//            new long[] {1000000000L, 100L}, //1:infinite
            new long[] {10000000000000000L, 100L} //1:infinite
        };

        for (int i = 0; i<testInput.length; i++) {
            long[] input = testInput[i];
            long start = System.nanoTime();
            long result = solveFourth(input[0], (int) input[1]);
//            long result = solveThird(input[0], (int) input[1]);
//            long result = solveSecond(input[0], (int) input[1]);
//            long result = solveFirst(input[0], (int) input[1]);
            long end = System.nanoTime();
            System.out.println(String.format("(%d, %d) -> %d; time -> %d", input[0], input[1], result, TimeUnit.NANOSECONDS.toMillis(end-start)));
        }
    }
}
