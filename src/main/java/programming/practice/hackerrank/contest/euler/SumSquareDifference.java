package programming.practice.hackerrank.contest.euler;

import java.util.Scanner;

/**
 * Hacker rank Project Euler#6
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SumSquareDifference {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            System.out.println(solve(n));
        }

//        test();
    }

    private static long solve(final int pNumber) {
        if (pNumber == 1) return 0;
        if (pNumber == 2) return 4;

        long result = 4L;
        for (int i = 3; i<=pNumber; i++) {
            result = result + (long)(i-1)*i*i;
        }

        return result;
    }

    private static long solve2(final int pNumber) {
        long wholeSquare = 0L;
        long squareSum = 0L;
        for (int i = 1; i<= pNumber; i++) {
            wholeSquare += (long)i;
            squareSum += ((long)i*i);
        }
        wholeSquare = wholeSquare*wholeSquare;
        return wholeSquare-squareSum;
    }

    private static void test() {
        int[] nums = new int[] {1, 2, 3, 4, 5, 10, 100, 1000, 9998, 9999, 10000};
        for (int i=0; i<nums.length; i++) {
            int n = nums[i];
            long start1 = System.nanoTime();
            long result = solve(n);
            long end1 = System.nanoTime();

            long start2 = System.nanoTime();
            long result2 = solve2(n);
            long end2 = System.nanoTime();
            System.out.println(String.format("SumOfSquareDifference(%d):: result1=%d, result2=%d, time1=%d, time2=%d", n, result, result2, (end1-start1), (end2-start2)));
        }
    }
}
