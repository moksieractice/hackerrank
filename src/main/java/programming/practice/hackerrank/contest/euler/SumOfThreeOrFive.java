package programming.practice.hackerrank.contest.euler;

import java.util.Scanner;

/**
 * Euler#1
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SumOfThreeOrFive {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();

        for(int i = 0; i < t; i++){
            int n = in.nextInt();
            System.out.println(solve(n));
//            solve1(n);
        }

//        test();
    }

    private static long solve1(final int pN) {
        long result = 0L;
        for (int i=1; i<pN; i++) {
            if (i%3 == 0 || i%5 == 0) {
                result += (long)i;
            }
        }
        return result;
    }

    private static long solve(final int n) {
        int numberOfThree = n/3;
        numberOfThree = n%3 == 0? numberOfThree-1 : numberOfThree;
        long sumOfThree = 3L*(numberOfThree+1)*numberOfThree/2;

        int numberOfFive = n/5;
        numberOfFive = n%5 == 0? numberOfFive-1 : numberOfFive;
        long sumOfFive = 5L*(numberOfFive+1)*numberOfFive/2;

        int numberOfFifteen = n/15;
        numberOfFifteen = n%15 == 0? numberOfFifteen-1 : numberOfFifteen;
        long sumOfFifteen = 15L*(numberOfFifteen+1)*numberOfFifteen/2;

        long result = sumOfThree + sumOfFive - sumOfFifteen;
        return result;
    }



    private static void test() {
        int[] ints = new int[] {1000000000, 100000000, 10000000, 1000000, 100000, 15000000, 15, 150, 1500};

        for (int i =0; i<ints.length; i++) {
            int n = ints[i];
            System.out.println(String.format("solve(%d)=%d      solve1(%d)=%d", n, solve(n), n, solve1(n)));
        }

    }
}
