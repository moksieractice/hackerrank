package programming.practice.hackerrank.challenges;

public class StairCase {

  static int stepPerms(int n) {

    long totalWays = 0;
    for (int p = 0; p <= n/3; p++) {
      int n1 = n-3*p;
      for (int q = 0; q <= n1/2; q++) {
        int r = n1 - 2*q;
        int steps = p + q + r;
        totalWays += comb(p, steps)*comb(q, q+r);
      }
    }
    return (int)(totalWays%10000000007L);
  }

  private static long comb(int r, int n) {
    if (r == 0 || n == 0 || r == n) return 1;

    r = Math.min(r, n-r);
    long mul = (long)n;
    long div = (long)r;
    while (r > 1) {
      n--;
      r--;
      mul = (mul*n);
      div = (div*r);
    }
    return mul/div;
  }

  public static void main(String[] args) {
    int[] input = new int[] {1, 3, 7};
    for (int n : input) {
      System.out.println(stepPerms(n));
    }
  }
}
