package programming.practice.hackerrank.challenges;

import java.util.Arrays;
import java.util.Comparator;

/**
 *
 * https://www.hackerrank.com/challenges/fraudulent-activity-notifications/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting&h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class FradulantActivityNotification {

  /**
   * Brute Force implementation. Getting TLE.
   * @param expenditure
   * @param d
   * @return
   */
  static int activityNotificationsBrutal(int[] expenditure, int d) {

    int[] copy = new int[expenditure.length];
    System.arraycopy(expenditure, 0, copy, 0, expenditure.length);

    int count = 0;
    int mid1 = d/2;
    int mid2 = (d&0x1)==1? -1 : mid1-1;
    for (int i = 0, j=d; j < expenditure.length; i++, j++) {
      Arrays.sort(expenditure, i, j);
      double mode = expenditure[mid1];
      if (mid2 != -1) {
        mode = (mode + expenditure[mid2])/2;
        mid2++;
      }
      if (expenditure[j] >= 2*mode) {
        count++;
      }

      System.arraycopy(copy, i, expenditure, i, d);
      mid1++;
    }

    return count;
  }

  static int activityNotifications(int[] expenditure, int d) {
    int max = Arrays.stream(expenditure).max().getAsInt();
    int[] count = new int[max+1];
    for (int i = 0; i<d; i++) {
      count[expenditure[i]]++;
    }
    int result = 0;
    for (int i = 0; i<expenditure.length-d; i++) {
      double median = findMedian(count, d);
      if (expenditure[i+d] >= 2*median) {
        result++;
      }

      count[expenditure[i]]--;
      count[expenditure[i+d]]++;
    }
    return result;
  }

  private static double findMedian(int[] pCounter, int d) {

    int c = 0;
    int m = (d+1)/2;
    double med=0;
    int i;
    for (i = 0; i < pCounter.length; i++) {
      c += pCounter[i];
      if (c >= m) {
        med = i;
        break;
      }
    }
    if (d%2 == 0) {
      if (c >= m+1) {
        med = med + i;
      } else {
        i++;
        for (;i<pCounter.length; i++) {
          c += pCounter[i];
          if (c >= m+1) {
            med += i;
            break;
          }
        }
      }
      med = med/2;

    }
    System.out.println(med);
    return med;
  }

  public static void main(String[] args) {
    System.out.println("Expected: 2, Actual: "+activityNotifications(new int[]{2, 3, 4, 2, 3, 6, 8, 4, 5}, 5));//2
    System.out.println("Expected: 1, Actual: "+activityNotifications(new int[]{10, 20, 30, 40, 50}, 3));//1
    System.out.println("Expected: 1, Actual: "+activityNotifications(new int[]{10, 20, 30, 40, 50}, 2));//1
    System.out.println("Expected: 0, Actual: "+activityNotifications(new int[]{1, 2, 3, 4, 4}, 4));//1
  }
}
