package programming.practice.hackerrank.challenges;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class CommonChildString {
  public int commonChild(String s1, String s2) {
    int l = s1.length();
    int[][] table = new int[l+1][l+1];
    int size = 0;
    for (int j = 0; j < l; j++) {
      for (int k = 0; k < l; k++) {
        if (s1.charAt(j) == s2.charAt(k)) {
          table[j+1][k+1] = 1 + table[j][k];
        }
        else {
          table[j+1][k+1] = Math.max(table[j][k+1], table[j+1][k]);
        }
      }
    }
    return table[l][l];
  }

  public static void main(String[] args) {
    CommonChildString solution = new CommonChildString();
    System.out.println(solution.commonChild("HARRY", "SALLY"));
  }
}
