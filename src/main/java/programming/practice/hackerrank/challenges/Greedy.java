package programming.practice.hackerrank.challenges;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/minimum-absolute-difference-in-an-array/problem
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class Greedy {
    public static void main(String[] args) {
//        minimumAbsoluteDifference();
        fightingPit();
    }

    private static void minimumAbsoluteDifference() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for(int index = 0; index < n; index++){
            array[index] = scanner.nextInt();
        }

        Arrays.sort(array);
        int minDiff = Integer.MAX_VALUE;
        for (int i=1; i<n; i++) {
            int diff = Math.abs(array[i]-array[i-1]);
            if (diff < minDiff) {
                minDiff = diff;
            }
        }
        System.out.println(minDiff);
        scanner.close();
    }

    public static void fightingPit() {

    }
}
