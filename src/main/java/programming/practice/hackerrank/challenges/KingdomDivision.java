package programming.practice.hackerrank.challenges;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class KingdomDivision {
  // Complete the kingdomDivision function below.
  static int kingdomDivision(int n, Map<Integer, int[]> roadMap, int[] parent) {
    if (roadMap.containsKey(n)) {
      int[] roads = roadMap.get(n);

      int count = (roads[0]>1 || parent[n] != 0) ? 2 : 1;
      for (int i = 1; i<=roads[0]; i++) {
        count *= kingdomDivision(roads[i], roadMap, parent);
      }

      return count;

    }
    return 1;
  }

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) throws IOException {
//    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

    int n = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    int[] parent = new int[n+1];
    int[] childCount = new int[n+1];

    for (int i = 0; i < n-1; i++) {
      String[] roadsRowItems = scanner.nextLine().split(" ");
      scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

      int u = Integer.parseInt(roadsRowItems[0]);
      int v = Integer.parseInt(roadsRowItems[1]);
      if (parent[v] <= 0) {
        parent[v] = u;
        childCount[u]++;
      }
      else {
        parent[u] = v;
        childCount[v]++;
      }
    }

    HashMap<Integer, int[]> roadMap = new HashMap<>();
    for (int i = 0; i<n; i++) {
      if (childCount[i] > 0) {
        roadMap.put(i, new int[childCount[i]+1]);
      }
    }

    for (int i = 0; i<parent.length; i++) {
      int[] road = roadMap.get(parent[i]);
      if (road != null) {
        road[road[0]+1] = i;
        road[0]++;
      }
    }


    int root = -1;
    for (int i = 1; i < n; i++) {
      if (parent[i] == 0) {
        root = i;
        break;
      }
    }
    int result = kingdomDivision(root, roadMap, parent);

//    bufferedWriter.write(String.valueOf(result));
//    bufferedWriter.newLine();
//
//    bufferedWriter.close();
    System.out.println(result);
    scanner.close();
  }
}

