package programming.practice.hackerrank.challenges;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class CountTriplets {

  static long countTripletsOne(List<Long> arr, long r) {

    int count = 0;
    HashMap<Long, List<Integer>> map = new HashMap<>();
    for (int i = 0; i<arr.size(); i++) {
      map.computeIfAbsent(arr.get(i)*r, k -> new ArrayList<Integer>()).add(i);
    }

    for (int k = 2; k < arr.size(); k++) {
      long third = arr.get(k);
      if (map.containsKey(third)) {
        long second = arr.get(map.get(third).get(0));
        if (map.containsKey(second)) {
          for (int j : map.get(third)) {
            for (int i : map.get(second)) {
              if (i < j && j < k) {
                count++;
              }
            }
          }
        }
      }

    }

    return count;
  }

  static long countTriplets(List<Long> arr, long r) {
    int count = 0;
    final HashMap<Long, Integer> map1 = new HashMap<>();
    final HashMap<Long, Integer> map2 = new HashMap<>();
    for (int i = 0; i<arr.size(); i++) {
      long num = arr.get(i);
      if (map2.containsKey(num)) {
        count += map2.get(num);
      }

      if (map1.containsKey(num)) {
        long third = num*r;
//        final int cnt = map1.get(num);
        map2.put(third, map2.getOrDefault(third, 0)+map1.get(num));
//        map2.computeIfPresent(third, (k,v) -> v+cnt);
//        map2.computeIfAbsent(third, k -> cnt);
      }



      num = num*r;
      map1.put(num, map1.getOrDefault(num, 0)+1);
//      map1.computeIfPresent(num, (k,v) -> v+1);
//      map1.computeIfAbsent(num, k -> 1);
    }


    return count;
  }

  public static void main(String[] args) {
    System.out.println(countTriplets(Arrays.asList(1L, 2L, 2L, 4L), 2));
    System.out.println(countTriplets(Arrays.asList(1L, 1L, 2L, 2L, 4L), 2));
    System.out.println(countTriplets(Arrays.asList(1L, 2L, 1L, 2L, 4L), 2));
    System.out.println(countTriplets(Arrays.asList(2L, 2L, 2L), 1));
    System.out.println(countTriplets(Arrays.asList(2L, 2L, 2L, 2L), 1));
    System.out.println(countTriplets(Arrays.asList(2L, 2L, 2L, 2L, 2L), 1));
  }
}
