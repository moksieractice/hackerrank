package programming.practice.hackerrank.challenges;

import java.util.Arrays;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class CountingInversion {

  static long countInversions(final int[] arr) {
    int[] indices = new int[arr.length];
    for (int i = 0; i<arr.length; i++) {
      indices[i] = i;
    }

    return 0;
  }
}
