package programming.practice.hackerrank.challenges;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SherlockAndValidString {
  static String isValid(String s) {
    int[] freq = new int[26];
    for (int i = 0; i < s.length(); i++) {
      freq[s.charAt(i)-'a']++;
    }

    int min = Integer.MAX_VALUE;
    int max = Integer.MIN_VALUE;
    for (int i = 0; i<26; i++) {
      if (freq[i] > 0) {
        min = min>freq[i]? freq[i] : min;
        max = max<freq[i]? freq[i] : max;
      }
    }

    if (max == min) {
      return "YES";
    }

    int maxCount = 0;
    int minCount = 0;
    for (int f : freq) {
      if (f  != 0) {
        if (f == max) maxCount++;
        else if (f == min) minCount++;
        else return "NO";
      }
    }

    if (maxCount*max + 1 == s.length() || (minCount+1)*min == s.length()-1) {
      return "YES";
    }

    return "NO";
  }
  public static void main(String[] args) {
    String input = "ibfdgaeadiaefgbhbdghhhbgdfgeiccbiehhfcggchgghadhdhagfbahhddgghbdehidbibaeaagaeeigffcebfbaieggabcfbiiedcabfihchdfabifahcbhagccbdfifhghcadfiadeeaheeddddiecaicbgigccageicehfdhdgafaddhffadigfhhcaedcedecafeacbdacgfgfeeibgaiffdehigebhhehiaahfidibccdcdagifgaihacihadecgifihbebffebdfbchbgigeccahgihbcbcaggebaaafgfedbfgagfediddghdgbgehhhifhgcedechahidcbchebheihaadbbbiaiccededchdagfhccfdefigfibifabeiaccghcegfbcghaefifbachebaacbhbfgfddeceababbacgffbagidebeadfihaefefegbghgddbbgddeehgfbhafbccidebgehifafgbghafacgfdccgifdcbbbidfifhdaibgigebigaedeaaiadegfefbhacgddhchgcbgcaeaieiegiffchbgbebgbehbbfcebciiagacaiechdigbgbghefcahgbhfibhedaeeiffebdiabcifgccdefabccdghehfibfiifdaicfedagahhdcbhbicdgibgcedieihcichadgchgbdcdagaihebbabhibcihicadgadfcihdheefbhffiageddhgahaidfdhhdbgciiaciegchiiebfbcbhaeagccfhbfhaddagnfieihghfbaggiffbbfbecgaiiidccdceadbbdfgigibgcgchafccdchgifdeieicbaididhfcfdedbhaadedfageigfdehgcdaecaebebebfcieaecfagfdieaefdiedbcadchabhebgehiidfcgahcdhcdhgchhiiheffiifeegcfdgbdeffhgeghdfhbfbifgidcafbfcd";
    System.out.println(isValid(input));
    System.out.println(isValid("aaaaddddeeeerrr"));
    System.out.println(isValid("aaaacccdd"));
    System.out.println(isValid("acdsews"));
    System.out.println(isValid("aaaadddeeewww"));
    System.out.println(isValid("aaaabbcc"));//NO
    System.out.println(isValid("aaaaabc"));//NO

  }
}
