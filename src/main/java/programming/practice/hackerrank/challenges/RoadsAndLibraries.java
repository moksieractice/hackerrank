package programming.practice.hackerrank.challenges;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.BinaryOperator;

/**
 *
 * https://www.hackerrank.com/challenges/torque-and-development/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=graphs
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class RoadsAndLibraries {
  long roadsAndLibraries(int n, int c_lib, int c_road, int[][] cities) {
    if (c_lib <= c_road) {
      return (long)n*c_lib;
    }

    long result = 0;
    int[] parents = new int[n+1];
    int[] rank = new int[n+1];
    for (int i = 1; i<=n; i++) {
      parents[i] = i;
    }

    int roadCount = 0;
    for (int[] city : cities) {
      int p0 = findSet(city[0], parents);
      int p1 = findSet(city[1], parents);
      if (p0 != p1) {
        if (rank[p0] > rank[p1]) {
          parents[p1] = p0;
        }
        else {
          parents[p0] = p1;
          if (rank[p0] == rank[p1]) {
            rank[p1]++;
          }
        }
        roadCount++;
      }
    }

    int componentCount = 0;
    HashMap<Integer, Integer> map = new HashMap<>();
    for (int i = 1; i<=n; i++) {
      int set = findSet(i, parents);
      map.computeIfPresent(set, (k,v) -> v+1);
      map.computeIfAbsent(set, k -> 1);
      if (set == i) {
        componentCount++;
      }
    }
    if (componentCount != map.size()) {
      System.out.println("Weird set number and component number are not same: com:"+componentCount+"   "+map.size());
    }
    int nodeCount = 0;
    for (int count : map.values()) {
      nodeCount+=count;
    }
    System.out.println(String.format("NOde:%d; component:%d::  %d", nodeCount, componentCount, nodeCount-componentCount));
    System.out.println(Arrays.asList(1,2,3,4,5).stream().reduce((a,b) ->a+b));
    result = c_road*roadCount*1L + c_lib*componentCount*1L;
    return result;
  }

  private int findSet(int pN, int[] pParents) {
    int p = pParents[pN];
    if(p != pN) {
      pParents[pN] = findSet(p, pParents);
    }

    return p;
  }

  long roadsAndLibrariesIncorrectAlgo(int n, int c_lib, int c_road, int[][] cities) {
    if(c_lib <= c_road) {
      return (long)n  *c_lib;
    }

    int cn = 0;
    int rn = 0;
    int[] components = new int[n+1];
    for (int i = 0; i<components.length; i++) {
      components[i] = i;
    }
    Arrays.sort(cities, (a,b) ->{return a[0]-b[0];});
    long roadCost = 0;
    int nn = 0;
    for (int[] city : cities) {
      int comp1 = components[city[0]];
      int comp2 = components[city[1]];
      if (comp1 != comp2) {
//        components[city[0]] = city[0];
        components[city[1]] = comp1;
        roadCost += c_road;
        // costByComponents.computeIfAbsent(city[0], k -> c_lib+c_road)
        rn++;
      }
//      else if (comp1 == 0) {
//        components[city[0]] = components[city[1]];
//        roadCost += c_road;
//        // costByComponents.computeIfPresent(components[city[1]], (k,v) -> v++c_road);
//        rn++;
//      }
//      else if (comp2 == 0) {
//        components[city[1]] = components[city[0]];
//        roadCost += c_road;
//        // costByComponents.computeIfPresent(components[city[0]], (k,v) -> v++c_road);
//        rn++;
//      }
//      else {
//        nn++;
//      }
    }

//    System.out.println("CN::"+cn);
    HashMap<Integer, Long> counts = new HashMap<>();
    for (int comp : components) {
      counts.computeIfPresent(comp, (k,v) ->  v+1L);
      counts.computeIfAbsent(comp, k -> 1L);
    }
    if (n == 84420) {
      long res = 0;
      counts.remove(0);
      for (Map.Entry<Integer, Long> entry : counts.entrySet()) {
        res += (entry.getValue()-1)*c_road+c_lib;
      }
      System.out.println("RES:: "+res+":::SIZE "+ counts.size());
    }
    long libCost = 0;
    for (int i = 1; i < components.length; i++) {
      if (components[i] == 0 || components[i] == i) {
        libCost += c_lib;
        cn++;
      }
    }

    int road_num = 0;
    for (int i = 1; i<= n; i++) {

    }

//    System.out.println("CN::"+cn+"  RN:"+rn+"    nn:"+nn+"    "+cn*c_lib*1L+"  "+rn*c_road*1L+"  "+(((long)cn*c_lib)+((long)rn*c_road)));

    System.out.println(String.format("n: %d, c_road:%d; c_lib:%d; cn:%d; rn:%d; (rn-cn):%d; c_lib*(rn-cn):%d; c_road*(rn-cn):%d", n, c_road, c_lib, cn, rn, (rn-cn), (long)c_lib*(rn-cn), (long)c_road*(rn-cn)));
    return roadCost+libCost;
  }

  public static void main(String[] args) throws FileNotFoundException {
    RoadsAndLibraries solution = new RoadsAndLibraries();

    System.out.println(solution.roadsAndLibraries(4, 6, 1, new int[][]{{1,2}, {1,3}, {1,3}, {3,2},{1,3},{2,4},{3,4}}));
    System.out.println(solution.roadsAndLibraries(5, 6, 1, new int[][]{{1,2}, {1,3}, {1,4}}));
    System.out.println(solution.roadsAndLibraries(3, 2, 1, new int[][]{{1,2}, {1,3}, {2,3}}));
    System.out.println(solution.roadsAndLibraries(9, 91, 84, new int[][]{{2,8}, {2,9}}));
    System.out.println(solution.roadsAndLibraries(5, 92, 23, new int[][]{{1,2},{3,5},{1,5},{3,4},{1,3},{4,5},{1,4},{2,5},{2,4}}));

    Scanner scanner = new Scanner(new File("C:\\Users\\Maks\\Documents\\HackerRankINputs\\roadsandlibraries_input10.txt"));
      int q = scanner.nextInt();
      scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

      long[] results = new long[] {5649516L,8432327200L, 5948772399L,1713294016L,7902963560L,7246913654L,5405604L,1739991079L,787818609L,9896100000L};
      for (int qItr = 0; qItr < q; qItr++) {
        String[] nmC_libC_road = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nmC_libC_road[0]);

        int m = Integer.parseInt(nmC_libC_road[1]);

        int c_lib = Integer.parseInt(nmC_libC_road[2]);

        int c_road = Integer.parseInt(nmC_libC_road[3]);

        int[][] cities = new int[m][2];

        for (int i = 0; i < m; i++) {
          String[] citiesRowItems = scanner.nextLine().split(" ");
          scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

          cities[i][0] = Integer.parseInt(citiesRowItems[0]);
          cities[i][1] = Integer.parseInt(citiesRowItems[1]);
//          if (u < v) {
//            cities[i][0] = u;
//            cities[i][1] = v;
//          }
//          else {
//            cities[i][0] = v;
//            cities[i][1] = u;
//          }
        }

        long result = solution.roadsAndLibraries(n, c_lib, c_road, cities);
        System.out.println(String.format("n:%d, c_lib:%d, c_road:%d, Expected: %d; Actual: %d; Differ: %d", n, c_lib, c_road, results[qItr], result, (long)(result-results[qItr])));
      }
      scanner.close();
    }
}
