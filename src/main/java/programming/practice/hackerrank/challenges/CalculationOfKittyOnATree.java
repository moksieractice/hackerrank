package programming.practice.hackerrank.challenges;

import java.io.*;
import java.util.*;

/**
 * https://www.hackerrank.com/challenges/kittys-calculations-on-a-tree/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
 *
 */
public class CalculationOfKittyOnATree {

    public static void main(String[] args) throws IOException {
      /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
//      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      BufferedReader reader = new BufferedReader(new FileReader("C:/Users/Maks/Documents/HackerRankInputs/kitty_calc_tree"));
      String[] nq = reader.readLine().split(" ");
      int n = Integer.parseInt(nq[0].trim());
      int q = Integer.parseInt(nq[1].trim());

      int[][] dist = new int[n][n];
      int infinite = 3*n;
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          dist[i][j] = infinite;
        }
        dist[i][i] = 0;
      }

      for (int k = 1; k < n; k++) {
        String[] ij = reader.readLine().split(" ");
        int i = Integer.parseInt(ij[0].trim())-1;
        int j = Integer.parseInt(ij[1].trim())-1;
        dist[i][j] = 1;
        dist[j][i] = 1;
      }

      for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
          for (int j = 0; j < n; j++) {
            int cost = dist[i][k]+dist[k][j];
            if (dist[i][j] > cost) {
              dist[i][j] = cost;
            }
          }
        }
      }

      for (int i = 0; i < q; i++) {
        int m = Integer.parseInt(reader.readLine().trim());
        if (m <= 1) {
          reader.readLine();
          System.out.println("0");
        }
        else {
          int[] set = new int[m];
          String[] sets = reader.readLine().split(" ");
          for (int j = 0; j < m; j++) {
            set[j] = Integer.parseInt(sets[j].trim());
          }

          int result = 0;
          for (int p = 0; p < m-1; p++) {
            for (int r = p+1; r < m; r++) {
              result = (result + set[p]*set[r]*dist[set[p]-1][set[r]-1])%1000000007;
            }
          }
          System.out.println(result);
        }
      }
    }
}
