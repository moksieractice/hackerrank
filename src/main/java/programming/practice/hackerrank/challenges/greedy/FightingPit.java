package programming.practice.hackerrank.challenges.greedy;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;


/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class FightingPit {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        solve(scanner, System.out);

        try {
            test();
        } catch (IOException pE) {
            pE.printStackTrace();
        } catch (URISyntaxException pE) {
            pE.printStackTrace();
        }
    }

    private static void solve(final Scanner scanner, final PrintStream pPrintStream) {
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int q = scanner.nextInt();

        List<Integer>[] teams = new ArrayList[k+1];
        long[] totalStrength = new long[k+1];
        for (int i  = 1; i<=k; i++) {
            teams[i] = new ArrayList<>();
        }

        for (int i=0; i<n; i++) {
            int strength = scanner.nextInt();
            int team = scanner.nextInt();
            teams[team].add(strength);
            totalStrength[team] += strength;
        }

        for (int i=1; i<=k; i++) {
            teams[i].sort(Comparator.naturalOrder());
        }

        for (int i=0; i<q; i++) {
            int query = scanner.nextInt();
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            if (query == 1) {
                List<Integer> team = teams[y];
                int s = x;
                int index = team.size()>0 && team.get(team.size()-1)<=s? -1 : Collections.binarySearch(team, s);
                if (index < 0) {
                    team.add(s);
                }
                else {
                    team.add(index, s);
                }
                totalStrength[y] += s;
            }
            else if (query == 2) {
                int teamX = teams[x].size()-1;
                int teamY = teams[y].size()-1;
                long strengthX = totalStrength[x];
                long strengthY = totalStrength[y];

                while(teamX>=0 && teamY>=0) {
                    if (strengthX >= strengthY) {
                        break;
                    }

                    int s = teams[x].get(teamX);
                    teamY = teamY-s; //team x's turn to attack
                    if (teamY < 0) {
                        break;
                    }
                    for (int j=teamY+1; j<=teamY+s; j++) {
                        strengthY -= teams[y].get(j);
                    }

                    if (strengthY >= strengthX) {
                        teamX = -1;
                        break;
                    }
                    s = teams[y].get(teamY);
                    teamX = teamX-s; //team y's turn to attack
                    if (teamX < 0) {
                        break;
                    }
                    for (int j=teamX+1; j<=teamX+s; j++) {
                        strengthX -= teams[x].get(j);
                    }
                }
                pPrintStream.println(teamX >= 0? x : y);
            }
        }
    }

    private static void test() throws IOException, URISyntaxException {
        int testCase = 3;
        Scanner scanner = new Scanner(FightingPit.class.getResourceAsStream(String.format("FightingPitsInput%d.txt", testCase)));
        Path outputFile = Files.createTempFile("hackerrank.practice.fightingpit", String.format("testcase%d.txt", testCase));
        PrintStream stream = new PrintStream(new FileOutputStream(outputFile.toFile()));
        long start = System.currentTimeMillis();
        solve(scanner, stream);
        System.out.println("Time:: "+(System.currentTimeMillis()-start));
        List<String> expected = Files.readAllLines(Paths.get(FightingPit.class.getResource(String.format("FightingPitsOutput%d.txt", testCase)).toURI()));
        List<String> result = Files.readAllLines(outputFile);
        for (int i = 0; i<expected.size(); i++) {
            if (!expected.get(i).equals(result.get(i))) {
                System.out.println(String.format("Mismatch!! Index:%d; expected:%s; result:%s", (i+1), expected.get(i), result.get(i)));
            }
        }
    }
}
