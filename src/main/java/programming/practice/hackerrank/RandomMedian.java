package programming.practice.hackerrank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomMedian {
  PriorityQueue<Integer> mMinHeap;
  PriorityQueue<Integer> mMaxHeap;

  public RandomMedian() {
    mMaxHeap = new PriorityQueue<>((a, b) -> b-a);
    mMinHeap = new PriorityQueue<>();
  }

  public void insert(int number) {
    if (mMinHeap.size() == mMaxHeap.size()) {
      if (!mMinHeap.isEmpty() && number > mMinHeap.peek()) {
        mMaxHeap.offer(mMinHeap.poll());
        mMinHeap.offer(number);
      }
      else {
        mMaxHeap.offer(number);
      }
    }
    else {
      if (!mMaxHeap.isEmpty() && number < mMaxHeap.peek()) {
        mMinHeap.offer(mMaxHeap.poll());
        mMaxHeap.offer(number);
      }
      else {
        mMinHeap.offer(number);
      }
    }
  }

  public int median() {
    if (mMinHeap.size() == mMaxHeap.size()) {
      if (mMaxHeap.size() == 0) {
        return 0;
      }
      return (mMaxHeap.peek()+mMinHeap.peek())/2;
    }

    return mMaxHeap.peek();
  }

  public static void main(String[] args) {

    new ArrayList<Integer>().sort((c, b) -> c-b);
    System.out.println(IntStream.of(0, 2, 1, -1, 4, 2, 3, 1, 2, 2).boxed().distinct().collect(Collectors.toList()));
    RandomMedian rd = new RandomMedian();
    int[] numbers = {10, 20, 30, 40, 10, 50, 30};
    for (int m: numbers) {
      rd.insert(m);
      System.out.println(String.format("Min Peek: %d; Max Peek: %d; Median: %d", rd.mMinHeap.peek(), rd.mMaxHeap.peek(), rd.median()));
    }
  }
}
