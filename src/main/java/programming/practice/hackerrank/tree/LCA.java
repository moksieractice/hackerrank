package programming.practice.hackerrank.tree;

import java.util.Scanner;

public class LCA {

  static  class Node {
    Node left;
    Node right;
    int data;

    Node(int data) {
      this.data = data;
      left = null;
      right = null;
    }
  }

  public static Node lca(Node root, int v1, int v2) {
    // Write your code here.
    if (root == null) return null;

    Node[] result = new Node[1];
    lca(root, v1, v2, result);
    return result[0];
  }

  public static int lca(Node root, int v1, int v2, Node[] result) {
    if (root == null) {
      return 0;
    }

    int count = root.data == v1 || root.data == v2 ? 1 : 0;
    count += lca(root.left, v1, v2, result);
    count += lca(root.right, v1, v2, result);

    if (count == 2 && result[0] == null) {
      result[0] = root;
    }

    return count;
  }

  public static Node insert(Node root, int data) {
    if (root == null) {
      return new Node(data);
    } else {
      Node cur;
      if (data <= root.data) {
        cur = insert(root.left, data);
        root.left = cur;
      } else {
        cur = insert(root.right, data);
        root.right = cur;
      }
      return root;
    }
  }

  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int t = scan.nextInt();
    Node root = null;
    while (t-- > 0) {
      int data = scan.nextInt();
      root = insert(root, data);
    }
    int v1 = scan.nextInt();
    int v2 = scan.nextInt();
    scan.close();
    Node ans = lca(root, v1, v2);
    System.out.println(ans.data);
  }
}