package programming.practice.hackerrank;

/**
 *
 * https://www.hackerrank.com/challenges/special-palindrome-again/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=strings
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SpecialString {
  static long substrCountBrutalWay(int n, String s) {
    long count = s.length();
    for (int i = 2; i<=s.length(); i++) { //substrings of length i
      for (int j = 0; j <= s.length()-i; j++) {
        if (isValid(s, j, i)) {
          count++;
        }
      }
    }

    return count;
  }

  static boolean isValid(String s, int b, int l) {
    int m = l/2;
    int e = (l&0x1) == 0? b+m : b+m+1;
    char c = s.charAt(b);
    for (int i = 0; i < m; i++) {
      if (s.charAt(b+i) != s.charAt(e+i) || s.charAt(b+i) != c) {
        System.out.println("TEXT: "+s.substring(b, b+l)+" :: false");
        return false;
      }
    }
    System.out.println("TEXT: "+s.substring(b, b+l)+" :: true");
    return true;
  }

  long substrCount(int n, String s) {
    long count = s.length();
    for (int i = 0; i < s.length(); i++) {
      int k = 0;
      char c = s.charAt(i);
      while(k<=i && i+k+1 < s.length()) {
        char c1 = s.charAt(i-k);
        char c2 = s.charAt(i+k+1);
        if (c1 == c2 && c1 == c) {
          count++;
          k++;
        }
        else {
          break;
        }
      }
      k = 1;
      while(i-k >= 0 && i+k < s.length()) {
        char c1 = s.charAt(i-k);
        char c2 = s.charAt(i+k);
        if (c1 == c2 && s.charAt(i-1) == c1) {
          count++;
          k++;
        }
        break;
      }
    }
    return count;
  }

  public static void main(String[] args) {
    SpecialString solution = new SpecialString();
    System.out.println(solution.substrCount(4, "aaaa"));
    System.out.println(solution.substrCount(7, "abcbaba"));
    System.out.println(solution.substrCount(4, "abcb"));
    System.out.println(solution.substrCount(4, "abba"));
    System.out.println(solution.substrCount(4, "abca"));
  }
}
