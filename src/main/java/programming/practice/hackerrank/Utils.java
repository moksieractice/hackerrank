package programming.practice.hackerrank;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class Utils {
  public static final void printArray(int[] pNums) {
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for (int o : pNums) {
      sb.append(o).append(", ");
    }
    sb.setLength(sb.length()-2);
    sb.append("]");
    System.out.println(sb.toString());
  }
}
