package programming.practice.hackerrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SuffixTree {
  static class Node {
    char mValue;
    HashMap<Character, Node> mChildrenMap = new HashMap<>();
    ArrayList<Integer> mIndices = new ArrayList<>();

    public void insert(String s, int index) {
      mIndices.add(index);

      if (s == null || s.length() == 0) return;

      char c = s.charAt(0);
      mValue = c;
      Node child = mChildrenMap.get(c);
      if (child == null) {
        child = new Node();
        mChildrenMap.put(c, child);
      }
      child.insert(s.substring(1), index+1);
    }

    public List<Integer> getIndices(String s) {
      if (s == null || s.length() == 0) return mIndices;

      char c = s.charAt(0);
      if (mChildrenMap.containsKey(c))  {
        return mChildrenMap.get(c).getIndices(s.substring(1));
      }

      return null;
    }
  }

  private Node mRoot = new Node();
  public SuffixTree(String s) {
    for (int i = 0; i<s.length(); i++) {
      mRoot.insert(s.substring(i), i);
    }
  }


  public static void main(String[] args) {
    String testString = "mississippi";
    String[] stringList = {"is", "sip", "hi", "sis"};
    SuffixTree tree = new SuffixTree(testString);
    for (String s : stringList) {
      List<Integer> list = tree.getIndices(s);
      if (list != null) {
        System.out.println(s + ": " + list.toString());
      }
    }
  }

  private List<Integer> getIndices(final String pS) {
    return mRoot.getIndices(pS);
  }
}
