package programming.practice.hackerrank;

import java.io.*;
import java.util.*;

/**
 *
 * https://www.hackerrank.com/challenges/find-the-nearest-clone/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=graphs
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class NearestClone {

  // Complete the findShortest function below.

  /*
   * For the unweighted graph, <name>:
   *
   * 1. The number of nodes is <name>Nodes.
   * 2. The number of edges is <name>Edges.
   * 3. An edge exists between <name>From[i] to <name>To[i].
   *
   */
  static int findShortest(int graphNodes, Map<Integer, List<Integer>> adjacent, long[] ids, long val) {
    // solve here
    System.out.println("graphNOde: "+graphNodes+"  "+adjacent);
    int min = graphNodes;
    for (int i = 1; i<=graphNodes; i++) {
      if (ids[i-1] == val) {
        boolean[] visited = new boolean[graphNodes+1];
        int[] dist = new int[graphNodes+1];
        Arrays.fill(dist, graphNodes);
        LinkedList<Integer> queue = new LinkedList<>();
        queue.offer(i);
        dist[i] = 0;
        while (!queue.isEmpty()) {
          int node = queue.poll();
          visited[node] = true;
          for (int n : adjacent.get(node)) {
            if (visited[n] == false) {
              queue.offer(n);
              dist[n] = Math.min(dist[n], 1+dist[node]);
            }
          }
        }
        for (int j = 1; j<=graphNodes; j++) {
          System.out.print(dist[j]+"  ");
          if(i != j && ids[j-1] == val && dist[j] < min) {
            min = dist[j];
          }
        }
        System.out.println(" Dist from "+i);
      }
    }

    System.out.println(min);

    return min < graphNodes ? min : -1;
  }

  public static void main(String[] args) throws IOException {

    Map<Integer, List<Integer>> map = new HashMap<>();
    map.put(1, Arrays.asList(2, 3));
    map.put(2, Arrays.asList(1, 4));
    map.put(3, Arrays.asList(3, 1));
    map.put(4, Arrays.asList(4, 2));
    System.out.println(findShortest(4, map, new long[]{1L, 2L, 1L, 1L}, 1L));


    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String[] graphNodesEdges = reader.readLine().split(" ");
    int graphNodes = Integer.parseInt(graphNodesEdges[0].trim());
    int graphEdges = Integer.parseInt(graphNodesEdges[1].trim());

    HashMap<Integer, List<Integer>> adjList = new HashMap<>();
    for (int i = 0; i < graphEdges; i++) {
      String[] graphFromTo = reader.readLine().split(" ");
      int one = Integer.parseInt(graphFromTo[0].trim());
      int two = Integer.parseInt(graphFromTo[1].trim());

      List<Integer> neighbors = adjList.get(one);
      if (neighbors == null) {
        neighbors = new ArrayList<>();
        adjList.put(one, neighbors);
      }
      neighbors.add(two);

      neighbors = adjList.get(two);
      if (neighbors == null) {
        neighbors = new ArrayList<>();
        adjList.put(two, neighbors);
      }
      neighbors.add(one);
    }

    long[] ids = new long[graphNodes];
    String[] idsItems = reader.readLine().split(" ");

    for (int i = 0; i < graphNodes; i++) {
      long idsItem = Long.parseLong(idsItems[i]);
      ids[i] = idsItem;
    }

    long val = Long.parseLong(reader.readLine().trim());

    System.out.println(findShortest(graphNodes, adjList, ids, val));

    reader.close();
  }
}

