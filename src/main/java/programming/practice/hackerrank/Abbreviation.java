package programming.practice.hackerrank;

/**
 * https://www.hackerrank.com/challenges/abbr/editorial?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=dynamic-programming
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class Abbreviation {

  String abbreviation(String a, String b) {
    int diff = 'a'-'A';

    boolean[][] dp = new boolean[b.length()+1][a.length()+1];
    dp[0][0] = true;
    for (int i = 0; i < b.length(); i++) {
      char cB = b.charAt(i);
      for (int j = 0; j < a.length(); j++) {
        char cA = a.charAt(j);
        if (cA == cB || cA-diff == cB) {
          dp[i+1][j+1] = dp[i][j];
        }
        if ('a' <= cA && cA <= 'z') {
          dp[i][j+1] = dp[i][j];
        }
      }
    }

    return dp[b.length()][a.length()]? "YES" : "NO";
  }

  String abbreviation1(String a, String b) {
    int diff = (int)('a'-'A');

    int i = a.length()-1;
    int j = b.length()-1;
    while(i >= 0 && j >= 0) {
      char cA = a.charAt(i);
      char cB = b.charAt(j);
      if (cA == cB || cA-diff == cB) {
        j--;
      }
      else if (cA >= 'A' && cA <= 'Z') {
        return "NO";
      }
      i--;
    }
    if (j < 0) {
      return "YES";
    }

    return "NO";
  }

  public static void main(String[] args) {
    Abbreviation solution = new Abbreviation();
    String[][] inputs = new String[][]{
        new String[]{"XXVVnDEFYgYeMXzWINQYHAQKKOZEYgSRCzLZAmUYGUGILjMDET",
                     "XXVVDEFYYMXWINQYHAQKKOZEYSRCLZAUYGUGILMDETQVWU", "NO", "ONE"},
        new String[]{"PVJSNVBDXABZYYGIGFYDICWTFUEJMDXADhqcbzva",
                     "PVJSNVBDXABZYYGIGFYDICWTFUEJMDXAD", "YES", "TWO"},
        new String[]{"QOTLYiFECLAGIEWRQMWPSMWIOQSEBEOAuhuvo",
                     "QOTLYFECLAGIEWRQMWPSMWIOQSEBEOA", "YES", "TRE"},
        new String[]{"DRFNLZZVHLPZWIupjwdmqafmgkg",
                     "DRFNLZZVHLPZWI", "YES", "FOU"},
        new String[]{"SLIHGCUOXOPQYUNEPSYVDaEZKNEYZJUHFXUIL",
                     "SLIHCUOXOPQYNPSYVDEZKEZJUHFXUIHMGFP", "NO", "FIV"},
        new String[]{"RYASPJNZEFHEORROXWZFOVDWQCFGRZLWWXJVMTLGGnscruaa",
                     "RYASPJNZEFHEORROXWZFOVDWQCFGRZLWWXJVMTLGG", "YES", "SIX"},
        new String[]{"AVECtLVOXKPHIViTZViLKZCZAXZUZRYZDSTIHuCKNykdduywb",
                     "AVECLVOXKPHIVTZVLKZCZAXZUZRYZDSTIHCKN", "YES", "SEV"},
        new String[]{"wZPRSZwGIMUAKONSVAUBUgSVPBWRSTJZECxMTQXXA",
                     "ZPRSZGIMUAKONSVAUBUSVPBWRSTJZECMTQXXA", "YES", "EIG"},
        new String[]{"SYIHDDSMREKXOKRFDQAOZJQXRIDWXPYINFZCEFYyxu",
                     "SYIHDDSMREKXOKRFDQAOZJQXRIDWXPYINFZCEFY", "YES", "NIN"},
        new String[]{"EIZGAWWDCSJBBZPBYVNKRDEWVZnSSWZIw",
                     "EIZGAWWDCSJBBZPBYVNKRDEWVZSSWZI", "YES", "TEN"},
        new String[]{"KXzQ","K", "NO", "EX1"},
        new String[]{"KXzQ","K", "NO", "EX2"},
        new String[]{"XzKQ","K", "NO", "EX3"},
        new String[]{"XzQK","K", "NO", "EX4"},
        new String[]{"asfKznq","K", "YES", "EX5"},
        new String[]{"sYOCa","YOCN", "NO", "EX6"},
    };

    for (String[] input : inputs) {
      System.out.println(String.format("%s::Expected: %s; Actual: %s", input[3], input[2], solution.abbreviation(input[0], input[1])));
    }

    int i = 8;
    System.out.println(String.format("Atr:%s; Match:%s; Expected: %s; Actual: %s", "dcCExFYyxu", "CEFY", "YES", solution.abbreviation("dcCExFYyxu", "CEFY")));
//    System.out.println(String.format("Atr:%s; Match:%s; Expected: %s; Actual: %s", inputs[i][0], inputs[i][1], inputs[i][2], solution.abbreviation(inputs[i][0], inputs[i][1])));
  }
}
