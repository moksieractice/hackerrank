import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.BiFunction;


/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class JSONMerger {

  private final Map<String, BiFunction<Collection, Object, Boolean>> handlers;

  public JSONMerger() {
    handlers = new HashMap<>();
    handlers.put("add", JSONMerger.this::add);
    handlers.put("remove", JSONMerger.this::remove);
    handlers.put("update", JSONMerger.this::update);
  }

  private void mergeAndSave(String pOriginal, String pChange, String pOutput) throws IOException {

//    Load the files.
    ObjectMapper objectMapper = new ObjectMapper();
    Map<String, Object> objects = objectMapper.readValue(new File(pOriginal), Map.class);
    Map<String, Map<String, List>> changes = objectMapper.readValue(new File(pChange), Map.class);

//    Apply the changes
    for (Map.Entry<String, Map<String, List>> change : changes.entrySet()) {
      if (objects.containsKey(change.getKey())) {
        Object object = objects.get(change.getKey());
        handlers.get("remove").apply(change.getValue().get("remove"), object);
        handlers.get("add").apply(change.getValue().get("add"), object);
        handlers.get("update").apply(change.getValue().get("update"), object);
      }
    }

//    Store the merged document
    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    objectMapper.writeValue(new File(pOutput), objects);
  }

  private Boolean remove(Collection<String> pConfig, Object pObject) {
    if (pConfig == null || pObject == null) return Boolean.FALSE;
    if (!(pObject instanceof Collection)) {
      throw new IllegalArgumentException("Incompatible object is passed. The object to remove");
    }

    Collection list = (Collection) pObject;
    Iterator iterator = list.iterator();
    boolean removed = false;
    while (iterator.hasNext()) {
      String key = getKey(iterator.next());
      if (pConfig.contains(key)) {
        iterator.remove();
        removed = true;
      }
    }

    return removed;
  }

  private String getKey(Object pObject) {
    if (pObject instanceof String) {
      return pObject.toString();
    }
    else if (pObject instanceof Map) {
      return ((Map)pObject).get("id").toString();
    }

    throw new IllegalArgumentException("At this point, either a String or a Map is expecting");
  }

  private Boolean add(Collection pConfig, Object pObject) {
    if (pConfig == null || pObject == null) return Boolean.FALSE;
    if (!(pObject instanceof Collection)) {
      throw new IllegalArgumentException("Incompatible object is passed. Expecting a collection of Maps.");
    }

    ((Collection)pObject).addAll(pConfig);

    return !pConfig.isEmpty();
  }

  private Boolean update(Collection pConfig, Object pObject) {
    if (pConfig == null || pConfig.isEmpty()) return false;

    Iterator iterator = pConfig.iterator();

    boolean changed = false;
    if (pObject instanceof Map) {
      return update((Map)pObject, (Map)iterator.next());
    }
    else if (pObject instanceof Collection){
      while (iterator.hasNext()) {
        Map with = (Map) iterator.next();
        Map object = findObject((Collection<Map>) pObject, with.get("id"));
        changed |= update(object, with);
      }
    }
    return changed;
  }

  private Map findObject(Collection<Map> pObject, Object pId) {
    for (Map map : pObject) {
      if (map.get("id").equals(pId) ) {
        return map;
      }
    }

    return Collections.emptyMap();
  }

  private Boolean update(Map<String, Object> pObject, Map<String, Object> pWith) {
    boolean modified = false;
    for (Map.Entry<String, Object> entry : pWith.entrySet()) {
      if (pObject.containsKey(entry.getKey())) {
        Object obj = pObject.get(entry.getKey());
        if (obj instanceof String) {
          pObject.put(entry.getKey(), entry.getValue());
        }
        else if (obj instanceof Map) {
          if (pObject.get(entry.getKey()) instanceof Map) {
            modified = update((Map)pObject.get(entry.getKey()), (Map)entry.getValue());
          }
          else {
            throw new IllegalStateException(String.format("The object with id \"%s\" must be compatible with the change", entry.getKey()));
          }
        }
        else if (obj instanceof Collection) {
          Map<String, Collection> change = (Map<String, Collection>) entry.getValue();
          modified |= handlers.get("remove").apply(change.get("remove"), obj);
          modified |= handlers.get("add").apply(change.get("add"), obj);
          modified |= handlers.get("update").apply(change.get("update"), obj);
        }
      }
    }

    return modified;
  }

  public static void main(String[] args) throws IOException {
    FileWriter fw = new FileWriter(new File("output.json"));
    new PrintWriter(fw).close();

    if (args.length < 3) {
      System.out.println("Not enough inputs. Please be sure to give original document, change document and output file name. Thank you.");
      System.exit(1);
    }

//    JSONMerger jsonTexting = new JSONMerger();
//    jsonTexting.mergeAndSave(args[0], args[1], args[2]);
  }
}
