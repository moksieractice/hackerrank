package code.google.codejam.y2020.roundb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;

public class RobotPathDecoding {
  private static class Node {
    private final int times;
    private final long column;
    private final long row;

    public Node(int times, long column, long row) {
      this.times = times;
      this.column = column;
      this.row = row;
    }
  }
  public static void main(String[] as) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    int testCases = Integer.parseInt(reader.readLine().trim());
    for (int t = 0; t < testCases; t++) {
      long w = 1;
      long h = 1;

      Deque<Node> stack = new LinkedList<>();
      String input = reader.readLine().trim();
      long max = 1000000000;
      for (int i = 0; i<input.length(); i++) {
        char c = input.charAt(i);
        if (Character.isDigit(c)) {
          stack.push(new Node(c-'0', w, h));
          i++;
        }
        else if (c == ')') {
          Node starting = stack.pop();
          long diffW = w - starting.column;
          long diffH = h - starting.row;
          for (int k = 1; k < starting.times; k++) {
            w += diffW;
            h += diffH;
            if (w > max) {
              w -= max;
            }
            else if (w < 0) {
              w += max;
            }

            if (h > max) {
              h -= max;
            }
            else if (h < 0) {
              h += max;
            }
          }
        }
      }
      System.out.printf("Case #%d: %d %d%n", (t+1), w, h);
    }
  }
}
