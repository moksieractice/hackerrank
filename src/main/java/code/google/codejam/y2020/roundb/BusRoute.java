package code.google.codejam.y2020.roundb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BusRoute {
  public static void main(String[] as) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    int testCases = Integer.parseInt(reader.readLine().trim());
    for (int t = 0; t < testCases; t++) {
      long d = Long.parseLong(reader.readLine().split(" ")[1].trim());
      String[] nums = reader.readLine().trim().split(" ");
      long result = d;
      for (int i = nums.length-1; i >= 0; i--) {
        long next = Long.parseLong(nums[i]);
        d -= d % next;
        if (result > d) {
          result = d;
        }
      }

      System.out.printf("Case #%d: %d%n", (t+1), result);
    }
  }

//  3
//  3 10
//  3 7 2
//  4 100
//  11 10 5 50
//  1 1
//  1

}
