package code.google.codejam.y2019;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class RoundATraining {

  public static void main(String[] args) throws IOException {
    RoundATraining training = new RoundATraining();
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    int testCases = Integer.parseInt(reader.readLine().trim());
    for (int i = 0; i < testCases; i++) {
      String[] npParts = reader.readLine().trim().split(" ");
      int n = Integer.parseInt(npParts[0]);
      int p = Integer.parseInt(npParts[1]);

      int[] skillLevels = new int[n];
      npParts = reader.readLine().trim().split(" ");
      for (int j = 0; j < n; j++) {
        skillLevels[j] = Integer.parseInt(npParts[j]);
      }

      npParts = null;
      System.out.println(training.computeTrainingTime(n, p, skillLevels));
    }
  }

  private int computeTrainingTime(final int n, final int p, final int[] skills) {
    Arrays.sort(skills);

    int time = 0;
    for (int i = n-1; i >= n-p; i--) {
      time += skills[n-1]-skills[i];
    }
    int minTime = time;
    int end = n-2;
    int begin = n-p-1;
    while (begin >= 0) {
      if (skills[end] == skills[end+1]) {
        time += skills[end]-skills[begin];
      }
      else {
        time -= (skills[end+1]-skills[end])*(p-1);
        time += (skills[end]-skills[begin]);
      }

      if (time < minTime) {
        minTime = time;
      }
      end--;
      begin--;
    }

    return minTime;
  }
}
