package code.practice.euler;

import java.util.function.Function;

public class NumberSplitting {

  public static void main(String[] args) {
    NumberSplitting solution = new NumberSplitting();

    long start = System.currentTimeMillis();
    System.out.println("10000L:"+solution.solveBruteString(1000000000000L)+"\tTime: "+(System.currentTimeMillis()-start));

    start = System.currentTimeMillis();
    System.out.println("10000L:"+solution.solveBruteNumber(1000000000000L)+"\tTime: "+(System.currentTimeMillis()-start));
  }

  private long solveBruteString(final long pNumber) {
    return solve(pNumber, this::isInteresting);
  }

  private long solveBruteNumber(final long pNumber) {
    return solve(pNumber, this::isInterestingBruteNumber);
  }

  private boolean isInterestingBruteNumber(final long pNumber) {
    return copBruteNumber(pNumber, pNumber*pNumber);
  }

  private boolean copBruteNumber(final long pTotal, final long pNumber) {
    if (pTotal == pNumber) return true;

    long div = 10;
    while(div < pTotal) {
      long remaining = pNumber/div;
      long current = pNumber%div;
      if (current < pTotal && remaining > 0) {
        if (copBruteNumber(pTotal-current, remaining)) {
          return true;
        }
      }
      div *= 10;
    }
    return false;
  }

  private long solve(final long pNumber, Function<Long, Boolean> pInteresting) {
    long limit = (long) Math.sqrt(pNumber);
    long count = 0;
    for (long i = 1; i <= limit; i++) {
      if (pInteresting.apply(i)) {
        count+= i*i;
      }
    }
    return count-1;
  }

  private boolean isInteresting(final long pNumber) {
    String squared = Long.toString(pNumber*pNumber);
    for (int i = 1; i <= squared.length(); i++) {
      if (compute(Long.parseLong(squared.substring(0, i)), i, squared, pNumber)) {
        return true;
      }
    }
    return false;
  }

  private boolean compute(final long pLeft, final int pIndex, final String pSquared, final long pNumber) {
    if (pIndex == pSquared.length()) return pLeft==pNumber;

    long right = Long.parseLong(pSquared.substring(pIndex));
    if (pLeft+right == pNumber) return true;
    for (int i = pIndex+1; i<=pSquared.length(); i++) {
      if (compute(pLeft+Long.parseLong(pSquared.substring(pIndex, i)), i, pSquared, pNumber)) {
        return true;
      }
    }

    return false;
  }
}
