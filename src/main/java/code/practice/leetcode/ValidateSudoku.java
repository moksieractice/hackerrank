package code.practice.leetcode;

/**
 *
 * https://leetcode.com/problems/valid-sudoku/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ValidateSudoku {

  /**
   * Runtime: 1 ms, faster than 100.00% of Java online submissions for Valid Sudoku.
   * Memory Usage: 43.4 MB, less than 94.20% of Java online submissions for Valid Sudoku.
   * @param board
   * @return
   */
  public boolean isValidSudoku(char[][] board) {
    "".compareTo("");
    int[] references = new int[]{0, 1, 2, 4, 16, 32, 64, 128, 256, 512};
    for (int i = 0; i < 9; i++) {
      int row = 0;
      for (int j = 0; j < 9; j++) {
        int num = board[i][j]-'0';
        if (board[i][j] != '.') {
          if ((row & references[num]) != 0) {
            return false;
          }
          row = row | references[num];
        }
      }
    }

    for (int i = 0; i < 9; i++) {
      int col = 0;
      for (int j = 0; j < 9; j++) {
        int num = board[j][i]-'0';
        if (board[j][i] != '.') {
          if ((col & references[num]) != 0) {
            return false;
          }
          col = col | references[num];
        }
      }
    }

    for (int i = 0; i < 9; i+=3){
      for (int j = 0; j < 9; j+=3) {
        int sq = 0;
        for (int m = i; m < i+3; m++) {
          for (int n = j; n < j+3; n++) {
            int num = board[m][n]-'0';
            if (board[m][n] != '.') {
              if((sq & references[num]) != 0) {
                return false;
              }
              sq = sq | references[num];
            }
          }
        }
      }
    }

    return true;
  }

  public static void main(String[] args) {
    ValidateSudoku ob = new ValidateSudoku();

    char[][] input = new char[][] {
      new char[]{'5','3','.','.','7','.','.','.','.'},
      new char[]{'6','.','.','1','9','5','.','.','.'},
      new char[]{'.','9','8','.','.','.','.','6','.'},
      new char[]{'8','.','.','.','6','.','.','.','3'},
      new char[]{'4','.','.','8','.','3','.','.','1'},
      new char[]{'7','.','.','.','2','.','.','.','6'},
      new char[]{'.','6','.','.','.','.','2','8','.'},
      new char[]{'.','.','.','4','1','9','.','.','5'},
      new char[]{'.','.','.','.','8','.','.','7','9'}
    };
    System.out.println(ob.isValidSudoku(input));


    input = new char[][]{
        new char[]{'.','.','.','.','5','.','.','1','.'},
        new char[]{'.','4','.','3','.','.','.','.','.'},
        new char[]{'.','.','.','.','.','3','.','.','1'},
        new char[]{'8','.','.','.','.','.','.','2','.'},
        new char[]{'.','.','2','.','7','.','.','.','.'},
        new char[]{'.','1','5','.','.','.','.','.','.'},
        new char[]{'.','.','.','.','.','2','.','.','.'},
        new char[]{'.','2','.','9','.','.','.','.','.'},
        new char[]{'.','.','4','.','.','.','.','.','.'}};
//                 [[".",".",".",".","5",".",".","1","."],
//                  [".","4",".","3",".",".",".",".","."],
//                  [".",".",".",".",".","3",".",".","1"],
//                  ["8",".",".",".",".",".",".","2","."],
//                  [".",".","2",".","7",".",".",".","."],
//                  [".","1","5",".",".",".",".",".","."],
//                  [".",".",".",".",".","2",".",".","."],
//                  [".","2",".","9",".",".",".",".","."],
//                  [".",".","4",".",".",".",".",".","."]]
    System.out.println(ob.isValidSudoku(input));
  }
}
