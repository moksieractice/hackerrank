package code.practice.leetcode.concurrency;

import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

/**
 *
 * https://leetcode.com/problems/building-h2o/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
class H2O {

  private int mOxygen = 0;
  private int mHydrogen = 0;
  public H2O() {

  }

  public synchronized void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
    while (mHydrogen == 2) {
      if (mOxygen == 0) {
        wait();
      }
      else if (mOxygen == 1){
        mOxygen = 0;
        mHydrogen = 0;
        notifyAll();
      }
      else {
        throw new IllegalStateException("This should not happen");
      }
    }

    if (mHydrogen < 2) {
      releaseHydrogen.run();
      mHydrogen++;
    }
  }

  public synchronized void oxygen(Runnable releaseOxygen) throws InterruptedException {
    while (mOxygen == 1) {
      if (mHydrogen != 2) {
        wait();
      }
      else if (mHydrogen == 2){
        mOxygen = 0;
        mHydrogen = 0;
        notifyAll();
      }
      else {
        throw new IllegalStateException("This should not happen");
      }
    }

    if (mOxygen == 0) {
      releaseOxygen.run();
      mOxygen++;
    }
  }

  private static final class Semaphore1 {
    private final Object mLock = new Object();
    private int mCount;

    public Semaphore1(int pCount) {
      mCount = pCount;
    }

    public void acquire() throws InterruptedException {
      synchronized (mLock) {
        if (mCount > 0) {
          mCount--;
        }
        else {
          mLock.wait();
        }
      }
    }

    public void release(int pCount) {
      mCount = pCount;
      mLock.notifyAll();
    }

  }

  public static void main(String[] args) {

  }
}