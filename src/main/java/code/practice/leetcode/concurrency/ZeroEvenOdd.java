package code.practice.leetcode.concurrency;

import java.util.function.IntConsumer;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ZeroEvenOdd {

    private int n;
    private int state = 0;

    public ZeroEvenOdd(int n) {
      this.n = n;
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public synchronized void zero(IntConsumer printNumber) throws InterruptedException {
      if (n <= 0) return;
      if (state == 0 || state == 2) {
        printNumber.accept(0);
        state++;
        notifyAll();
      }
      else {
        wait();
      }

      "".contains("");
    }

    public synchronized void even(IntConsumer printNumber) throws InterruptedException {
      if (n <= 0) return;
      if (state == 1) {
        printNumber.accept(1);
        state++;
        n--;
        notifyAll();
      }
      else {
        wait();
      }
    }

    public synchronized void odd(IntConsumer printNumber) throws InterruptedException {
      if (n <= 0) return;
      if (state == 3 && n > 0) {
        printNumber.accept(2);
        state = 0;
        n--;
        notifyAll();
      }
      else {
        wait();
      }
    }
}
