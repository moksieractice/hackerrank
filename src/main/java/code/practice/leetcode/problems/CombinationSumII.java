package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class CombinationSumII {

  public List<List<Integer>> combinationSum2(int[] candidates, int target) {
    List<List<Integer>> answer = new ArrayList<>();

    Arrays.sort(candidates);
    compute(0, 0, new ArrayList<>(), candidates, target, answer);
    return answer;
  }

  private void compute(int pStart, int pCurrentSum, List<Integer> pCurrent, int[] pCandidates, int pTarget, List<List<Integer>> pAnswer) {
    if (pCurrentSum == pTarget) {
      pAnswer.add(new ArrayList<>(pCurrent));
      return;
    }

    for (int i = pStart; i < pCandidates.length; i++) {
      if (pCurrentSum + pCandidates[i] <= pTarget) {
        pCurrent.add(pCandidates[i]);
        compute(i+1, pCurrentSum+pCandidates[i], pCurrent, pCandidates, pTarget, pAnswer);
      }
      
    }
  }

  // Given subset {... nums[i-1]} as prefix and curSum as its sum
  // Consider all subset from nums[i ... N-1] such that curSum + new subset sum <= target
  private void backtrack(int i, int curSum, List<Integer> prefix) {
//    if (curSum == target) {
//      ans.add(new ArrayList<>(prefix));
//      return;
//    }
//
//    for (int j = i; j < nums.length; j++) {
//      // Skip duplicate
//      if (j > i && nums[j] == nums[j - 1]) {
//        continue;
//      }
//
//      if (curSum + nums[j] <= target) {
//        prefix.add(nums[j]);
//        backtrack(j + 1, curSum + nums[j], prefix);
//        prefix.remove(prefix.size() - 1);
//      }
//    }
  }

  public static void main(String[] args) {
    CombinationSumII combinationSum = new CombinationSumII();
    System.out.println(combinationSum.combinationSum2(new int[] {10,1,2,7,6,1,5}, 8));
    System.out.println(combinationSum.combinationSum2(new int[] {2,5,2,1,2}, 5));
  }
}
