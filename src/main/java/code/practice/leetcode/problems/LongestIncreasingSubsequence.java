/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code.practice.leetcode.problems;

import java.util.Arrays;

/**
 * 
 * 
 * @author Maks
 */
public class LongestIncreasingSubsequence {
 
    public int lengthOfLIS(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        
        int[] lis = new int[nums.length];
        lis[0] = nums[0];
        int size = 1;
        for (int i = 1; i < nums.length; i++) {
            int index = Arrays.binarySearch(lis, 0, size, nums[i]);
            if (index < 0) {
                index = -index-1;
            }
            lis[index] = nums[i];
            if (index == size) {
                size++;
            }
        }
        
        return size;
    }

    public int lengthOfLISN2(int[] nums) {
        if (nums == null || nums.length == 0) return 0;

        int[] dp = new int[nums.length];
        dp[0] = 1;
        // int maxLis = 0;
        int maxLis = 1;
        for (int i = 1; i < nums.length; i++) {
            int max = 0;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i] && max <= dp[j]) {
                    max = 1+dp[j];
                }
            }
            dp[i] = max > 1? max : 1;
            // dp[i] = max > dp[i-1]? max : dp[i-1];
            maxLis = dp[i]>maxLis? dp[i] : maxLis;
        }
        // return dp[nums.length-1];
        return maxLis;
    }
    
    public static void main(String[] args) {
        LongestIncreasingSubsequence lis = new LongestIncreasingSubsequence();
        System.out.println("lis([]) = "+lis.lengthOfLIS(new int[]{}));
        System.out.println("lis([0]) = "+lis.lengthOfLIS(new int[]{0}));
        System.out.println("lis([10, 9, 2, 5, 3, 7, 101, 18]) = "+lis.lengthOfLIS(new int[]{10, 9, 2, 5, 3, 7, 101, 18}));
        System.out.println("lis([1, 3, 6, 7, 9, 4, 10, 5, 6]) = "+lis.lengthOfLIS(new int[]{1, 3, 6, 7, 9, 4, 10, 5, 6}));
    }
}
