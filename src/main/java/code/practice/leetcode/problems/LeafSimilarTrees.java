package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.BiFunction;

/**
 * Consider all the leaves of a binary tree.  From left to right order, the values of those leaves form a leaf value sequence.
 *
 * For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).
 * Two binary trees are considered leaf-similar if their leaf value sequence is the same.
 * Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar
 *
 * https://leetcode.com/contest/weekly-contest-94/problems/leaf-similar-trees/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class LeafSimilarTrees {
    private class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(final int x) {
          this(x, null, null);
      }

      TreeNode (final int x, final TreeNode pLeft) {
          this(x, pLeft, null);
      }

      TreeNode (final int x, final TreeNode pLeft, final TreeNode pRight) {
          val = x;
          left = pLeft;
          right = pRight;
      }
    }

    private class LeafIterator {
        private TreeNode current;
        private Stack<TreeNode> mStack;

        private TreeNode next;
        public LeafIterator(final TreeNode pRoot) {
            current = pRoot;
            mStack = new Stack<>();
        }

        public TreeNode next() {
            if (current == null) return null;

            while (true) {
                if (current.left != null) {
                    mStack.push(current);
                    current = current.left;
                }
                else if (current.right != null) {
                    current = current.right;
                }
                else {
                    next = current;
                    if (mStack.empty()) {
                        current = null;
                    }
                    while (!mStack.empty()) {
                        current = mStack.pop().right;
                        if (current != null) {
                            break;
                        }
                    }
                    break;
                }
            }

            return next;
        }

    }

    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        if (root1 == null || root2 == null) return false;

        List<Integer> leaves1 = new ArrayList<>();
        List<Integer> leaves2 = new ArrayList<>();
        findLeaves(root1, leaves1);
        findLeaves(root2, leaves2);

        if (leaves1.size() != leaves2.size()) {
            return false;
        }
        for (int i = 0; i < leaves1.size(); i++) {
            if (leaves1.get(i) != leaves2.get(i)) return false;
        }
        return true;
    }

    private void findLeaves(final TreeNode pRoot, final List<Integer> pLeaves) {
        if (pRoot == null) return;
        if (pRoot.left == null && pRoot.right == null) {
            pLeaves.add(pRoot.val);
            return;
        }
        findLeaves(pRoot.left, pLeaves);
        findLeaves(pRoot.right, pLeaves);
    }

    public boolean leafSimilar2(TreeNode root1, TreeNode root2) {
        if (root1 == null || root2 == null) return false;

        LeafIterator iterator1 = new LeafIterator(root1);
        LeafIterator iterator2 = new LeafIterator(root2);
        TreeNode leaf1 = iterator1.next();
        TreeNode leaf2 = iterator2.next();
        while (leaf1 != null && leaf2 != null) {
            if (leaf1.val != leaf2.val) return false;

            leaf1 = iterator1.next();
            leaf2 = iterator2.next();
        }

        return leaf1 == null && leaf2 == null;
    }

    public static void main(String[] args) {
        LeafSimilarTrees leafSimilarTrees = new LeafSimilarTrees();

        TreeNode tree1 = leafSimilarTrees.buildTree1();
        TreeNode tree2 = leafSimilarTrees.buildTree2();
        TreeNode tree3 = leafSimilarTrees.buildTree3();
        TreeNode tree4 = leafSimilarTrees.buildTree4();
        TreeNode tree6 = leafSimilarTrees.buildTree6();

        BiFunction<TreeNode, TreeNode, Boolean> function = leafSimilarTrees::leafSimilar2;
        long start = System.nanoTime();
        System.out.println(String.format("Tree1 and Tree2:: expected='True' found='%s'", function.apply(tree1, tree2)));
        System.out.println(String.format("Tree1 and Tree3:: expected='False' found='%s'", function.apply(tree1, tree3)));
        System.out.println(String.format("Tree1 and Tree4:: expected='False' found='%s'", function.apply(tree1, tree4)));
        System.out.println(String.format("Tree4 and Tree3:: expected='False' found='%s'", function.apply(tree4, tree3)));
        System.out.println(String.format("Tree4 and Tree4:: expected='True' found='%s'", function.apply(tree4, tree4)));
        System.out.println(String.format("Tree1 and Null:: expected='False' found='%s'", function.apply(tree1, null)));
        System.out.println(String.format("Tree3 and Tree5:: expected='True' found='%s'", function.apply(tree3, leafSimilarTrees.buildTree5())));
        System.out.println(String.format("Tree6 and Tree6:: expected='True' found='%s'", function.apply(tree6, tree6)));
        System.out.println(String.format("Tree6 and Tree7:: expected='True' found='%s'", function.apply(tree6, leafSimilarTrees.buildTree7())));
        System.out.println(String.format("Tree8 and Tree4:: expected='True' found='%s'", function.apply(leafSimilarTrees.buildTree8(), tree4)));
        System.out.println(String.format("Time Taken: %d", (System.nanoTime()-start)));
    }

    private TreeNode buildTree8() {
        return new TreeNode(1, new TreeNode(2, new TreeNode(3, new TreeNode(4, new TreeNode(3)))));
    }

    private TreeNode buildTree7() {
        return new TreeNode(1,null, new TreeNode(2));
    }

    private TreeNode buildTree6() {
        return new TreeNode(1, new TreeNode(2));
    }

    private TreeNode buildTree1() {
        return new TreeNode(3,
                new TreeNode(5,
                        new TreeNode(6),
                        new TreeNode(2,
                                new TreeNode(7),
                                new TreeNode(4))),
                new TreeNode(1,
                        new TreeNode(9),
                        new TreeNode(8))
                );
    }

    private TreeNode buildTree2() {
        return new TreeNode(3,
                new TreeNode(5,
                        new TreeNode(6),
                        new TreeNode(2,
                                new TreeNode(7))),
                new TreeNode(1,
                        new TreeNode(4),
                        new TreeNode(10,
                                new TreeNode(9),
                                new TreeNode(8)))
                );
    }

    private TreeNode buildTree3() {
        return new TreeNode(3,
                    new TreeNode(5),
                    new TreeNode(1));
    }

    private TreeNode buildTree4() {
        return new TreeNode(3);
    }
    private TreeNode buildTree5() {
        return new TreeNode(3,
                null,
                new TreeNode(6,
                        new TreeNode(5),
                        new TreeNode(1)));
    }
}
