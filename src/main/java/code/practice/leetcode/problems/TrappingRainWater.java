package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Trapping Rain Water
 *
 * https://leetcode.com/problems/trapping-rain-water/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class TrappingRainWater {
  public static void main(String[] args) {
    TrappingRainWater solution = new TrappingRainWater();
    
    int[][] inputs = new int[][] {
//        new int[] {5, 5, 5, 5, 5}, //0
//        new int[] {5, 3, 3, 3, 3, 8}, //8
//        new int[] {5, 3, 3, 3, 3, 4}, //4
//        new int[] {5, 3, 3, 4, 4, 2, 2, 3}, //4
        new int[] {0, 1, 2, 3, 1, 2, 1, 0, 0, 0, 4}, //14
        new int[] {0,1,0,2,1,0,1,3,2,1,2,1}, //6
        new int[] {10, 8, 6, 9, 12}
    };
    for (int[] input : inputs) {
      System.out.println(solution.trap(input));
//      System.out.println(solution.trappingRainWaterLevel(input));
//      System.out.println(solution.solveByReservoirs(input));
    }
  }

  private int trappingRainWaterLevel(int[] pElevations) {
    int result = 0;
    Stack<Integer> leftHighs = new Stack<>();

    for (int i = 0; i<pElevations.length-1; i++) {
      int elevation = pElevations[i];
      int nextElevation = pElevations[i+1];
      if (elevation < nextElevation) {
        while (!leftHighs.isEmpty()) {
          int prevHighIndex = leftHighs.peek();
          int prevHigh = pElevations[prevHighIndex];
          if (prevHigh <= nextElevation ) {
            leftHighs.pop();
            result += (prevHigh-elevation)*(i-prevHighIndex);
            elevation = prevHigh;
          }
          else {
            result += (nextElevation - elevation)*(i-prevHighIndex);
            break;
          }
        }
      }
      else if (elevation > nextElevation) {
        leftHighs.push(i);
      }
    }
    return result;
  }

  public int solveByReservoirs(int[] pElevations) {
    int result = 0;
    Stack<Integer> leftHighs = new Stack<>();
    for (int i = 0; i < pElevations.length-1; i++) {
      int it = pElevations[i];
      int next = pElevations[i+1];
      if (it == next) {

      }
      else if (it < next) {
        int prevHighIndex = -1;
        while (!leftHighs.isEmpty() && pElevations[leftHighs.peek()] <= next) {
          prevHighIndex = leftHighs.pop();
        }
        if (leftHighs.isEmpty() && prevHighIndex >= 0) {
//          A reservoir is detected.
          System.out.println(String.format("Reservoir is detected: [%d, %d]"));
          int reservoirElevation = pElevations[prevHighIndex];
          for (int j = prevHighIndex+1; j<=i; j++) {
            result += (reservoirElevation-pElevations[j]);
          }
        }
      }
      else {
        leftHighs.push(i);
      }
    }
    return result;
  }


  /**
   * https://leetcode.com/problems/trapping-rain-water/discuss/128902/Easy-JAVA-solution-using-two-pointers
   * @param height
   * @return
   */
  public int trap(int[] height) {
    if (height.length == 0) return 0;
    int left = 0;
    int right = height.length - 1;
    int area = 0, leftHeight = height[0], rightHeight = height[height.length - 1];
    while (left < right){
      if (height[left] < height[right]){
        left++;
        leftHeight = Math.max(leftHeight, height[left]);
        area += leftHeight - height[left];
      }
      else{
        right--;
        rightHeight = Math.max(rightHeight, height[right]);
        area += rightHeight - height[right];
      }
    }
    return area;
  }
}
