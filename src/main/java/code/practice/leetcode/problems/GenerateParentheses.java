package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

/**
 * https://leetcode.com/problems/generate-parentheses/
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class GenerateParentheses {

  public List<String> generateParenthesis(int n) {
//    HashMap<Integer, List<String>> map = new HashMap<>();
//    map.put(0, new ArrayList<String>(){{add("");}});
//    return generateParenthesis(n, map);

    List<String> result = new ArrayList<>();
    dfs(n, n, n, "", result);
    return result;

  }


  /**
   * Runtime: 1 ms, faster than 93.30% of Java online submissions for Generate Parentheses.
   * Memory Usage: 36.2 MB, less than 100.00% of Java online submissions for Generate Parentheses
   * @param pOpen    Number of remaining opening parenthesis
   * @param pClose   Number of remaining closing parenthesis
   * @param pNumber       The number
   * @param pPattern  The generated string so far
   * @param pResult The list to collect result
   */
  private void dfs(int pOpen, int pClose, int pNumber, String pPattern, List<String> pResult) {
    if (pOpen == 0 && pClose == 0) {
      pResult.add(pPattern);
    }

    if (pOpen > 0 && pOpen <= pNumber) {
      dfs(pOpen-1, pClose, pNumber, pPattern+"(", pResult);
    }
    if (pClose > 0 && pClose > pOpen) {
      dfs(pOpen, pClose-1, pNumber, pPattern+")", pResult);
    }
  }

  public List<String> generateParenthesis(int n, HashMap<Integer, List<String>> memo) {
    List<String> result = new ArrayList<>();

    for (int i = 0; i < n; i++) {
      for (String left : getParenthesisList(i, memo)) {
        for (String right : getParenthesisList(n-i-1, memo)) {
          result.add(String.format("(%s)%s", left, right));
        }
      }
    }
    return result;
  }

  public List<String> getParenthesisList(int n, HashMap<Integer, List<String>> memo) {
    List<String> result = memo.get(n);
    if (result == null) {
      result = generateParenthesis(n, memo);
      memo.put(n, result);
    }
    return result;
  }

  public static void main(String[] args) {
    GenerateParentheses solution = new GenerateParentheses();

    System.out.println(solution.generateParenthesis(0));
    System.out.println(solution.generateParenthesis(1));
    System.out.println(solution.generateParenthesis(2));
    System.out.println(solution.generateParenthesis(3));
    System.out.println(solution.generateParenthesis(4));
  }

}
