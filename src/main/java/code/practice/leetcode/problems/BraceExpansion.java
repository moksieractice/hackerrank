package code.practice.leetcode.problems;


import java.util.*;
import java.util.stream.Collectors;

/**
 * https://leetcode.com/problems/brace-expansion-ii/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class BraceExpansion {

    public List<String> braceExpansionII(String expression) {
      expression += "2";
      Stack<Set<String>> stack = new Stack<>();
      Stack<Integer> modes = new Stack<>();

      boolean concatMode = false;
      stack.push(new LinkedHashSet<>());
      String text = "";
      for (int i = 0; i < expression.length()-1; i++) {
        char c = expression.charAt(i);
        char next = expression.charAt(i + 1);
        if (c >= 'a' && c <= 'z') {
          text = String.format("%s%c", text, c);
          if (next == ',' || next == '}') {
            stack.peek().add(text);
            text = "";
          } else if (next == '{') {
            Set<String> s = new LinkedHashSet<>();
            s.add(text);
            text = "";
            stack.push(s);
            concatMode = true;
          }
        }
        if (c == '{') {
          modes.push(concatMode ? 2 : (next == '{' ? 0 : 1));
          stack.push(new LinkedHashSet<>());
          concatMode = false;
        } else if (c == '}') {
          Set<String> concatenated = null;
          int mode = modes.pop();
          if (mode == 2) {
            Set<String> second = stack.pop();
            Set<String> first = stack.pop();
            concatenated = concat(first, second);
          } else {
            concatenated = stack.pop();
          }

          if (next == '{') {
            concatMode = true;
            stack.push(concatenated);
          } else {
            stack.peek().addAll(concatenated);
          }
        }
      }

      Set<String> output = stack.pop();
      while (!stack.isEmpty()) {
        output = concat(stack.pop(), output);
      }

      ArrayList result = new ArrayList(output);
      Collections.sort(result);
      return result;
     }

    private Set<String> concat(Collection<String> pFirst, Collection<String> pSecond) {
      Set<String> output = new LinkedHashSet<>();
      if (pFirst != null && pSecond != null) {
        for (String first : pFirst) {
          for (String second : pSecond) {
            output.add(first+second);
          }
        }
      }
      else if (pFirst != null) {
        output.addAll(pFirst);
      }
      else {
        output.addAll(pSecond);
      }

      return output;
    }

  //  https://leetcode.com/discuss/interview-question/354944/Google-or-Phone-Screen-or-Brace-Expansion
  public List<String> googlePhone(String pInput) {
    StringBuilder sb = new StringBuilder();
    ArrayList<String[]> elements = new ArrayList<>();

//    1. Finding the braces and the elements inside those
    int start = 0;
    for (int i = 0; i < pInput.length(); i++) {
      if (pInput.charAt(i) == '{') {
        sb.append(pInput.substring(start, i));
        start = i+1;
        while(i < pInput.length() && pInput.charAt(i) != '}') {
          i++;
        }
        elements.add(pInput.substring(start, i).split(","));
        sb.append("%s");
        start = i+1;
      }
    }
    if (start < pInput.length()) {
      sb.append(pInput.substring(start, pInput.length()));
    }

//    2. Finding the combinations / cartesian products of elements
    List<String> combinations = Arrays.asList(elements.get(0));
    for (int i = 1; i < elements.size(); i++) {
      List<String> incrementingCombination = new ArrayList<>();
      for (String comb : combinations) {
        for (String str : elements.get(i)) {
          incrementingCombination.add(String.format("%s,%s", comb, str));
        }
      }
      combinations = incrementingCombination;
    }

//    3. Generate output
    List<String> output = new ArrayList<>(combinations.size());
    String template = sb.toString();
    for (String combination : combinations) {
      String[] strs = combination.split("");
      output.add(String.format(template, strs));
    }

    return output;
  }

  public List<String> braceExpansionIIS(String expression) {
      List<String> result = null;

      Stack<Set<String>> resultStack = new Stack<>();
      Stack<Character> operationStack = new Stack<>();
      int concatCount = 0;

      resultStack.push(new HashSet<>());
      int index = 0;
      StringBuilder sb = new StringBuilder();
      while (index < expression.length()) {
        char c = expression.charAt(index);
        if (c >= 'a' && c <= 'z') {
          sb.append(c);
        }
        else if (c == ',') {
          if (sb.length() != 0) {
            resultStack.peek().add(sb.toString());
            sb.setLength(0);
          }
//          else {
//            Set<String> top = resultStack.pop();
//            resultStack.peek().addAll(top);
//          }
        }
        else if (c == '{') {
          char prev = 0;
          if (index > 0) {
            prev = expression.charAt(index-1);
          }

          if (prev == ',') {
            operationStack.push('U');
          }
          else if (prev == '}') {
            operationStack.push('C');
          }
          else if (prev == '{') {
            operationStack.push('N');
          }
          else {
            resultStack.push(new HashSet<>(1));
            resultStack.peek().add(sb.toString());
            sb.setLength(0);
            operationStack.push('C');
          }
          resultStack.push(new HashSet<>());
        }
        else if (c == '}') {
          char mode = operationStack.pop();
          if (mode == 'U') {

          }
        }
      }
      return result;
  }


  public List<String> braceExpansionIIR(String expression) {
      if (expression == null || expression.length() == 0) return Arrays.asList("");
      Set<String> result = expand(expression, new int[]{0});
//      if (index != expression.length()) {
//        throw new IllegalStateException("Whole string is not scanned");
//      }
      return result.stream().sorted().collect(Collectors.toList());
  }

  private Set<String> expand(String pExpression, int[] pIndex) {
      Set<String> result = new HashSet<>();

      StringBuilder currentText = new StringBuilder();
      Set<String> currentSet = null;
      Set<String> right;

      while (pIndex[0] < pExpression.length()) {
        char c = pExpression.charAt(pIndex[0]++);
        if (c >= 'a' && c <= 'z') {
          currentText.append(c);
        }
        else {
          if (currentText.length() != 0) {
            if (currentSet == null) {
              currentSet = new HashSet<>();
              currentSet.add(currentText.toString());
            }
            currentSet = concat(currentSet, Arrays.asList(currentText.toString()));
            currentText.setLength(0);
          }

          if (c == '{') {
            right = expand(pExpression, pIndex);
            currentSet = concat(currentSet, right);
          }
          else if (c == '}') {
            if (currentSet != null){
              result.addAll(currentSet);
            }

            return result;
          }
          else if (c == ',') {
            if (currentSet != null){
              result.addAll(currentSet);
            }
            currentSet = null;
          }
        }
      }

    if (currentText.length() != 0) {
      currentSet = concat(currentSet, Arrays.asList(currentText.toString()));
    }
    result.addAll(currentSet);
    return result;
  }

  public static void main(String[] args) {
    BraceExpansion be = new BraceExpansion();
//    System.out.println(be.googlePhone("a_{cat,dog}_is_with_{sarah,mike}"));

//    System.out.println(be.braceExpansionII("{a,b}{c,{d,e}}"));
//    System.out.println(be.braceExpansionII("{{a,z},a{b,c},{ab,z}}"));
//    System.out.println(be.braceExpansionII("a,b,{c,d}{e,f,{g,h},{i,j}}"));
//    System.out.println(be.braceExpansionII("{{{a{bcd,e}}}}"));


    System.out.println(be.braceExpansionIIR("{a,b}{c,{d,e}}"));
    System.out.println(be.braceExpansionIIR("{a,b}f{c,{d,e}}"));
    System.out.println(be.braceExpansionIIR("k,{a,b}f{c,{d,e}}g,h"));
    System.out.println(be.braceExpansionIIR("{{a,z},a{b,c},{ab,z}}"));
    System.out.println(be.braceExpansionIIR("a,b,{c,d}{e,f,{g,h},{i,j}}"));
    System.out.println(be.braceExpansionIIR("{{{a{bcd,e}}}}"));
    System.out.println(be.braceExpansionIIR("abcd"));
    System.out.println(be.braceExpansionIIR(""));
    System.out.println(be.braceExpansionIIR("{}"));
  }
}
