package code.practice.leetcode.problems.arraysandstrings;

import java.util.HashMap;
import java.util.Map;

public class MinWindowSubstring {
  public static void main(String[] args) {
    MinWindowSubstring mws = new MinWindowSubstring();

    Map<String, String> inputs = new HashMap<String, String>() {{
      put("ADOBECODEBANC", "ABC");
      put("ADOBECODEBAND", "A");
      put("ADOBECODEBANE", "AA");
      put("ADOBECODEBANF", "AAA");
      put("ADOBECODENF", "END");
      put("ADOBECODENFG", "BEND");
      put("ADOBE", "OE");
      put("ADOBECODE", "EBOD");
    }};

    for (Map.Entry<String, String> entry: inputs.entrySet()) {
      System.out.println(String.format("minWindow('%s', '%s'): '%s'", entry.getKey(), entry.getValue(),
          mws.minWindow(entry.getKey(), entry.getValue())));
    }
  }

  private String minWindow(final String s, final String t) {
    //Find the counts of the individual characters of t
    Map<Character, Integer> tMap = new HashMap<>();
    for (int i = 0; i<t.length(); i++) {
      tMap.put(t.charAt(i), tMap.getOrDefault(t.charAt(i), 0)+1);
    }
    System.out.println(tMap);

    int b = 0;
    int e = 0;
    int r = 0;
    Map<Character, Integer> rMap = new HashMap<>();

    int bMin = -1;
    int eMin = s.length();

    //shrink left and expand right to find next intervals
    while(e < s.length()) {
      while(r != tMap.size() && e < s.length()) {
        char c = s.charAt(e++);
        int count = rMap.getOrDefault(c, 0)+1;
        rMap.put(c, count);

        if (count == tMap.getOrDefault(c, -1)) {
          r++;
        }
        if (r == tMap.size()) {
          break;
        }
      }

      while(r == tMap.size()) {
        char c = s.charAt(b);
        //shrink left end
        int count = rMap.get(c)-1;
        if (count < tMap.getOrDefault(c, -1)) {
          if (e-b < eMin-bMin) {
            eMin = e;
            bMin = b;
          }
          r--;
        }
        rMap.put(c, count);
        b++;
      }
    }

    return bMin < 0 ? "" : s.substring(bMin, eMin);
  }
}
