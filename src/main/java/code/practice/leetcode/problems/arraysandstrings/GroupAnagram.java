package code.practice.leetcode.problems.arraysandstrings;

import java.util.*;

public class GroupAnagram {
  public static void main(String[] args) {
    String[] input = {"eat","tea","tan","ate","nat","bat"};
    GroupAnagram ga = new GroupAnagram();
    System.out.println(ga.groupAnagrams(input));
  }

  private List<List<String>> groupAnagrams(final String[] strs) {
    Map<String, List<String>> result = new HashMap<>();
    for (String str : strs) {
      String hash = anagramHash(str);
      if (!result.containsKey(hash)) {
        result.put(hash, new ArrayList<>());
      }
      result.get(hash).add(str);
    }
    return new ArrayList<>(result.values());
  }

  private String anagramHash(final String str) {
    char[] chars = str.toCharArray();
    Arrays.sort(chars);
    return new String(chars);
  }
}
