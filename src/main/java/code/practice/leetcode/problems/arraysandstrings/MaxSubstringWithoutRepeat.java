package code.practice.leetcode.problems.arraysandstrings;

import java.util.HashMap;
import java.util.Map;

public class MaxSubstringWithoutRepeat {
  public static void main(String[] args) {
    MaxSubstringWithoutRepeat sl = new MaxSubstringWithoutRepeat();

    Map<String, Integer> inputs = new HashMap<>();
    inputs.put("", 0);
    inputs.put("a", 1);
    inputs.put("ab", 2);
    inputs.put("aab", 2);
    inputs.put("abb", 2);
    inputs.put("aabb", 2);
    inputs.put("aba", 2);
    inputs.put("abaa", 2);
    inputs.put("abba", 2);
    inputs.put("abbac", 3);

    for (Map.Entry<String, Integer> entry: inputs.entrySet()) {
      System.out.println(String.format("'%s':: Expected:%d, Actual:%d", entry.getKey(), 
          entry.getValue(), sl.lengthOfLongestSubstring(entry.getKey())));
    }
  }

  private int lengthOfLongestSubstring(final String s) {
    if (s == null) return 0;
    if (s.length() <= 1) return s.length();

    int[] indices = new int[256];
    int start = 0;
    int answer = 0;
    for (int i = 0; i<s.length(); i++) {
      char current = s.charAt(i);
      int index = indices[current];
      if (index != 0 && index >= start) {
        if (answer < i-start) {
          answer = i-start;
        }
        start = indices[current];
      }
      indices[current] = i+1;
    }


    return Math.max(answer, s.length()-start);
  }
}
