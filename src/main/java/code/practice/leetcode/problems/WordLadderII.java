package code.practice.leetcode.problems;

import java.util.*;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class WordLadderII {
  public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
    List<List<String>> result = new ArrayList<>();

    Map<String, List<String>> graph = new HashMap<>(wordList.size()+1, 1.0f);
    graph.put(beginWord, new ArrayList<>());
    findNeighbors(beginWord, graph, wordList);

    for (String word : wordList) {
      graph.put(word, new ArrayList<>());
      findNeighbors(word, graph, wordList);
    }

    Map<String, Integer> distances = computeDistance(beginWord, graph);

    List<String> path = new ArrayList<>();
    path.add(beginWord);
    findPaths(graph, distances, beginWord, endWord, path, result);
    return result;
  }

  private void findPaths(Map<String, List<String>> pGraph, Map<String, Integer> pDistances, String pWord, String pEndWord, List<String> pPath, List<List<String>> pResult) {
    if (pWord.equals(pEndWord)) {
      pResult.add(new ArrayList<>(pPath));
    }

    int dist = pDistances.get(pWord);
    for (String neighbor : pGraph.get(pWord)) {
      if (dist < pDistances.get(neighbor)) {
        pPath.add(neighbor);
        findPaths(pGraph, pDistances, neighbor, pEndWord, pPath, pResult);
        pPath.remove(pPath.size()-1);
      }
    }
  }

  private Map<String, Integer> computeDistance(String pBeginWord, Map<String, List<String>> pGraph) {

    HashMap<String, Integer> distances = new HashMap<>();
    distances.put(pBeginWord, 0);

    LinkedList<String> list = new LinkedList<>();
    list.offer(pBeginWord);
    while(!list.isEmpty()) {
      String node = list.poll();
      List<String> neighbors = pGraph.get(node);
      int dist = distances.get(node);
      for (String neighbor : neighbors) {
        if (distances.getOrDefault(neighbor, Integer.MAX_VALUE) > dist+1) {
          list.offer(neighbor);
          distances.put(neighbor, dist+1);
        }
      }
    }

    return distances;
  }

  private void findNeighbors(String pWord, Map<String, List<String>> pGraph, List<String> pWordList) {
    List<String> neighbors = pGraph.get(pWord);
    for (String word : pWordList) {
      if (!pWord.equals(word)) {
        int diff = 0;
        for (int i = 0; i< pWord.length(); i++) {
          diff +=(pWord.charAt(i) - word.charAt(i) == 0? 0 : 30);
        }
        if (diff <= 30) {
          neighbors.add(word);
        }
      }
    }
  }

  public static void main(String[] args) {
    WordLadderII solution = new WordLadderII();

    System.out.println(solution.findLadders("hit", "cog", Arrays.asList("hot","dot","dog","lot","log","cog")));
  }
}
