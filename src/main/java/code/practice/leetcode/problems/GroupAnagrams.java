package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * https://leetcode.com/problems/group-anagrams/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class GroupAnagrams {
  public static final int[] sHashTable = new int[26];
  static {
    int hash = 0x1;
    for (int i = 0; i<sHashTable.length; i++) {
      sHashTable[i] = hash;
      hash = hash << 1;
    }
  }

  /**
   * Runtime: 8 ms, faster than 97.04% of Java online submissions for Group Anagrams.
   * Memory Usage: 41 MB, less than 95.32% of Java online submissions for Group Anagrams.
   * @param strs
   * @return
   */
  public List<List<String>> groupAnagrams(String[] strs) {
    List<List<String>> result = new ArrayList<>();

    HashMap<String, List<String>> hashesMap = new HashMap<>();
    for (String str : strs) {
      char[] chars = str.toCharArray();
      Arrays.sort(chars);
      String hash = new String(chars);
      List<String> list = hashesMap.get(hash);
      if (list == null) {
        list = new ArrayList<>();
        hashesMap.put(hash, list);
      }
      list.add(str);
    }
    result.addAll(hashesMap.values());
    return result;
  }

  /**
   * Wrong output
   * @param strs
   * @return
   */
  public List<List<String>> groupAnagramsRepHashing(String[] strs) {
    List<List<String>> result = new ArrayList<>();

    HashMap<String, List<String>> hashesMap = new HashMap<>();
    for (String str : strs) {
      char[] chars = str.toCharArray();
      HashMap<Character, Integer> characterMap = new HashMap<>();
      for (int i = 0; i<chars.length; i++) {
        characterMap.put(chars[i], characterMap.getOrDefault(chars[i], 0)+1);
      }
      String hash = characterMap.entrySet().stream().map(e -> String.format("%d%c", e.getValue(), e.getKey())).collect(Collectors.joining(""));
      List<String> list = hashesMap.get(hash);
      if (list == null) {
        list = new ArrayList<>();
        hashesMap.put(hash, list);
      }
      list.add(str);
    }
    result.addAll(hashesMap.values());
    return result;
  }


  public static void main(String[] args) {
    GroupAnagrams solution = new GroupAnagrams();
    System.out.println(solution.groupAnagramsRepHashing(new String[]{"eat", "tea", "tan", "ate", "nat", "bat", "eee", "ee"}));
    System.out.println(solution.groupAnagramsRepHashing(new String[]{"chi","nip","lab","mud","fan","yak","kid","lox","joy","rob","cad","hug","ken","oaf","pus","hos","ton","any","sac","mid","nip","ron","tux","set","jug","axe","ago","sob","ode","dot","nit","pug","sue","new","rub","sup","ohs","ski","oaf","don","cob","kin","ark","gay","jay","bur","dot","eat","rca","wad","maj","luz","gad","dam","eon","ark","del","sin","tat"
  }));

//    [["joy"],["gay"],["axe"],["sac"],["ago"],["sob"],["eon"],["nit"],["sue"],["any"],["cob"],["ski"],["jay"],["yak"],["sin"],["mud"],["ron"],["rob"],["rub"],["lab"],["lox"],["mid"],["kid"],["ode"],["dam"],["del"],["jug"],["gad"],["tux"],["hug"],["wad"],["tat"],["ton"],["pug"],["ark","ark"],["maj"],["nip","nip"],["eat"],["bur"],["fan"],["luz"],["ken"],["oaf","oaf"],["rca"],["set"],["new"],["hos","ohs"],["dot","dot"],["chi"],["kin"],["don"],["cad"],["pus","sup"]]
//[["tat"],["sin"],["del"],["wad"],["rca"],["eat"],["ron"],["mid"],["nit"],["gay"],["ton"],["bur","rub"],["pus","sup"],["hug"],["oaf","oaf"],["mud"],["ken"],["dam"],["any"],["cad"],["sue"],["jay"],["lab"],["rob"],["chi"],["pug"],["ark","ark"],["fan"],["maj"],["nip","nip"],["kid"],["yak"],["dot","dot"],["tux"],["joy"],["sob"],["kin"],["set"],["gad"],["jug"],["eon"],["ago"],["luz"],["ski"],["ode"],["hos","ohs"],["new"],["sac"],["lox"],["don"],["axe"],["cob"]]

  }
}
