package code.practice.leetcode.problems;

import java.util.HashMap;

/**
 *
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 *
 * Related Problems::
 *  https://leetcode.com/problems/longest-substring-with-at-most-two-distinct-characters/
 *  https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/
 *  https://leetcode.com/problems/subarrays-with-k-different-integers/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class LongestSubstringWithoutRepeatingCharacter {

  /**
   * Runtime: 6 ms, faster than 85.59% of Java online submissions for Longest Substring Without Repeating Characters.
   Memory Usage: 36.1 MB, less than 99.76% of Java online submissions for Longest Substring Without Repeating Characters.
   *
   * @param s
   * @return
   */
  public int lengthOfLongestSubstring(String s) {
    if (s.isEmpty()) return 0;
    HashMap<Character, Integer> indexTable = new HashMap<>();

    int start = 0;
    int maxLength = 1;
    indexTable.put(s.charAt(0), 0);
    for (int i = 1; i < s.length(); i++) {
      char c = s.charAt(i);
      int index = indexTable.getOrDefault(c, -1);
      if (index >= start) {
        maxLength = Math.max(maxLength, i-start);
        start = index+1;
      }
      indexTable.put(c, i);
    }
    maxLength = Math.max(maxLength, s.length()-start);

    return maxLength;
  }

  public static void main(String[] args) {
    LongestSubstringWithoutRepeatingCharacter solution = new LongestSubstringWithoutRepeatingCharacter();

    System.out.println(String.format("%s --> %d", "abcabcbb", solution.lengthOfLongestSubstring("abcabcbb")));
    System.out.println(String.format("%s --> %d", "bbbb", solution.lengthOfLongestSubstring("bbbb")));
    System.out.println(String.format("%s --> %d", "pwwkew", solution.lengthOfLongestSubstring("pwwkew")));
    System.out.println(String.format("%s --> %d", "p", solution.lengthOfLongestSubstring("p")));
    System.out.println(String.format("%s --> %d", "\"\"", solution.lengthOfLongestSubstring("")));
    System.out.println(String.format("%s --> %d", " ", solution.lengthOfLongestSubstring(" ")));
    System.out.println(String.format("%s --> %d", "abc def", solution.lengthOfLongestSubstring("abc def")));
  }
}
