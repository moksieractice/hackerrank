package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * https://leetcode.com/problems/3sum/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ThreeSum {


  /**
   * Runtime: 97 ms, faster than 28.28% of Java online submissions for 3Sum.
   * Memory Usage: 48 MB, less than 83.39% of Java online submissions for 3Sum.
   * @param nums
   * @return
   */
  public List<List<Integer>> threeSum(int[] nums) {
    List<List<Integer>> result = new ArrayList();

    Arrays.sort(nums);

    HashMap<Integer, Integer> numberIndices = new HashMap<>();
    for (int i = 0; i < nums.length; i++) {
      numberIndices.put(nums[i], i);
    }

    for (int i = 0; i < nums.length-1; i++) {
      //Skip the duplicates
      if (i > 0 && nums[i] == nums[i-1]) continue;
      for (int j = i+1; j<nums.length-1; j++) {
        int key = - nums[i] - nums[j];
        if (numberIndices.containsKey(key)) {
          int k = numberIndices.get(key);
          if (k > j) {
            List<Integer> tuple = new ArrayList<>(3);
            tuple.add(nums[i]);
            tuple.add(nums[j]);
            tuple.add(nums[k]);
            result.add(tuple);
          }
        }
        //Skip the duplicates. If j and j+1 have the same value then considering that will create same i, j pair.
        while (j < nums.length-1 && nums[j] == nums[j+1]) j++;
      }
    }
    return result;
  }

  public static void main(String[] args) {
    ThreeSum solution = new ThreeSum();
    System.out.println(solution.threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
    System.out.println(solution.threeSum(new int[]{-1, 0, -1, 2, -1, -4, 2, 5, 4}));
    System.out.println(solution.threeSum(new int[]{-1, 0, -1, 2, -1, -4, 2, 5, -2, 4}));
    System.out.println(solution.threeSum(new int[]{0, 0, 0}));
    System.out.println(solution.threeSum(new int[]{0, 0, 0, 0}));
  }
}
