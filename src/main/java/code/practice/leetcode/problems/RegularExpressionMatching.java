package code.practice.leetcode.problems;

/**
 * https://leetcode.com/problems/regular-expression-matching/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class RegularExpressionMatching {
  public static void main(String[] args) {
    RegularExpressionMatching matching = new RegularExpressionMatching();
    String[] strings = new String[]{
      "aa", "a",
        "a", "ab*",
        "aa", "a*",
        "ab", ".*",
        "abc", ".*c",
        "abc", ".*d",
        "abbaac", ".*c",
        "aab", "c*a*b*d*",
        "aab", "c*a*b*d*e*f*g*",
        "aab", "c*a*b*d*e*f*g*",
        "aab", "c*a*b*d*e*fg*",
        "aaa", "c*a*b*d*ae*a"
    };

    for (int i = 0; i<strings.length; i+=2) {
      String str = strings[i];
      String regex = strings[i+1];
      System.out.println(String.format("'%s' matches '%s': %s", regex, str, matching.matches(str, regex)));
//      System.out.println(String.format("'%s' matches '%s': %s", regex, str, matching.isMatch(str, regex)));
    }

    System.out.println(String.format("'c*a*b*d*' matches '%s': %s", "aab", matching.matches("aab", "c*a*b*d*")));
  }

  private boolean isMatch(String s, String p) {
    if (p.isEmpty()) return s.isEmpty();

    char[] sChars = s.toCharArray();
    char[] pChars = p.toCharArray();

    int si = 0;
    int pi = 0;

    char pc = pChars[pi];
    char sc = sChars[si];
    boolean wildcard = (pi < (pChars.length-1) && pChars[pi+1] == '*')? true : false;
    while (si < sChars.length && pi < pChars.length) {
      if (pc == '.' || sc == pc) {
        si++;
        sc = si < sChars.length? sChars[si] : '0';
      }

      if (wildcard) {
        if (sc != pc || pi + 2 < pChars.length && (pChars[pi + 2] == '.' || sc == pChars[pi + 2])) {
          pi += 2;
        }
      }
      else {
        pi++;
      }

      if (pi < pChars.length) {
        pc = pChars[pi];
        wildcard = (pi < (pChars.length - 1) && pChars[pi + 1] == '*') ? true : false;
      }
    }
    while (pi < pChars.length-1) {
      if(pChars[pi+1] == '*') {
        pi += 2;
      }
      else {
        return false;
      }
    }

    return si == sChars.length && (pi == pChars.length || pi == pChars.length-2 && pChars[pi+1]=='*');
  }

  /**
   * Runtime: 109 ms, faster than 5.02% of Java online submissions for Regular Expression Matching.
   * Memory Usage: 39.8 MB, less than 37.37% of Java online submissions for Regular Expression Matching.
   * @param s
   * @param p
   * @return
   */
  private boolean matches(String s, String p) {
    if (p.isEmpty()) return s.isEmpty();
    char[] sc = s.toCharArray();
    char[] pc = p.toCharArray();

    int si = 0;
    int pi = 0;

    boolean wildcard = false;
    while(si < sc.length && pi < pc.length) {
      wildcard = pi < pc.length-1 && pc[pi+1]=='*';
      if (wildcard) {
          return matches(s.substring(si), p.substring(pi+2)) ||
              ((sc[si] == pc[pi] || pc[pi] == '.') && matches(s.substring(si+1), p.substring(pi)));
      }
      else if (sc[si] == pc[pi] || pc[pi] == '.') {
        si++;
        pi++;
      }
      else {
        return false;
      }
    }
    wildcard = pi < pc.length-1 && pc[pi+1]=='*';
    while(wildcard && pi < pc.length) {
      pi += 2;
      wildcard = pi < pc.length-1 && pc[pi+1]=='*';
    }
    return si == sc.length && pi == pc.length;
  }
}
