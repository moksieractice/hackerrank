package code.practice.leetcode.problems;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class GameOfLife {
    public static void main(String[] args) {
        GameOfLife solution = new GameOfLife();
        solution.testGameOfLife();
    }

    public void testGameOfLife() {
        int[][] board = new int[][]{
                {0, 1, 0},
                {0, 0, 1},
                {1, 1, 1},
                {0, 0, 0}
        };
        gameOfLife(board);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(String.format("%4d", board[i][j]));
            }
            System.out.println("");
        }
    }

    /**
     * Computes the next state of the board in-place. It uses two pass to compute. In first pass the next states are
     * computed. During this phase it keeps track both current and next states. In the second pass, the next state is
     * made its current state. To store the both states, it uses least significant two bits of the state of a cell.
     * Where least significant bit is used to store the current state and 2nd least significant bit is used to store the
     * next state during computation. In the second phase, the second bit is shifted to the first bit.
     *
     * @param pBoard
     */
    public void gameOfLife(final int[][] pBoard) {
        if (pBoard == null || pBoard.length == 0) return;
        int m = pBoard.length-1;
        int n = pBoard[0].length-1;
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                int aliveNeighbor = calculateAliveNeighbors(pBoard, Math.max(0, i-1), Math.min(i+1, m), Math.max(0, j-1), Math.min(j+1, n));
                if (pBoard[i][j] == 1) {
                    //Since calculateAliveNeighbors includes the current cell we should increase the number by one for
                    //conditions 1, 2 and 3.
                    if(aliveNeighbor < 3 || aliveNeighbor > 4) {
                        pBoard[i][j] = 1; //current=1 and next = 0 ==> 01
                    }
                    else {
                        pBoard[i][j] = 3;//current = 1 and next = 1 ==> 11
                    }
                }
                else if (aliveNeighbor == 3){
                    pBoard[i][j] = 2; // current = 0 and next = 1 ==> 10
                }
            }
        }

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                pBoard[i][j] >>= 1;
            }
        }
    }

    private int calculateAliveNeighbors(final int[][] pBoard, final int pRowFrom, final int pRowTo, final int pColumFrom, final int pColumTo) {
        int alive = 0;
        for (int i = pRowFrom; i <= pRowTo; i++) {
            for (int j = pColumFrom; j <= pColumTo; j++) {
                alive += pBoard[i][j]&1;
            }
        }
        return alive;
    }
}
