package code.practice.leetcode.problems;

/**
 * https://leetcode.com/problems/median-of-two-sorted-arrays/
 *
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MedianOfSortedArrays {

  public static void main(String[] args) {
    MedianOfSortedArrays ob = new MedianOfSortedArrays();

    System.out.println(ob.findMedianSortedArrays(new int[]{1, 2, 5, 7, 8, 10, 11, 13, 14}, new int[]{3, 4, 6, 9, 12}));
    System.out.println(ob.findMedianSortedArrays(new int[]{3, 4, 6, 9, 12}, new int[]{1, 2, 5, 7, 8, 10, 11, 13, 14}));
    System.out.println(ob.findMedianSortedArrays(new int[]{1, 2, 5, 7, 8, 10, 11, 13}, new int[]{3, 4, 6, 9, 12}));
    System.out.println(ob.findMedianSortedArrays(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, new int[]{11, 12, 13}));
    System.out.println(ob.findMedianSortedArrays(new int[]{5, 6, 7, 8, 9, 10, 11, 12, 13, 13}, new int[]{1, 2, 3, 4}));
  }

  private double findMedianSortedArrays(int[] nums1, int[] nums2) {
    int[] A, B;
    if (nums1.length <= nums2.length) {
      A = nums1;
      B = nums2;
    } else {
      A = nums2;
      B = nums1;
    }

    int m = A.length;
    int n = B.length;

    int mid = (m+n+1)/2;

    int start = 0;
    int end = m;
    while(start <= end) {
      int i = (start+end)/2;
      int j = mid - i;

      if (i < end && B[j-1] > A[i]) {
        start = i + 1;
      }
      else if (i > start && A[i-1] > B[j]) {
        end = i - 1;
      }
      else {
        int leftMax;
        if (i == 0) {
          leftMax = B[j-1];
        }
        else if (j == 0) {
          leftMax = A[i-1];
        }
        else {
          leftMax = Math.max(A[i-1], B[j-1]);
        }

        if ((m+n)%2 == 1) {
          return leftMax;
        }

        int rightMin;
        if (i == m) {
          rightMin = B[j];
        }
        else if (j == n) {
          rightMin = A[i];
        }
        else {
          rightMin = Math.min(A[i], B[j]);
        }

        return (leftMax+rightMin)/2.0;
      }
    }
    return 0;
  }
}
