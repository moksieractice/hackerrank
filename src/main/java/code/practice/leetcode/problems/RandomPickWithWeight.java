package code.practice.leetcode.problems;

import java.util.Arrays;
import java.util.Random;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class RandomPickWithWeight {

  private int[] distribution;
  private int last;
  private Random generator;
  public RandomPickWithWeight(int[] w) {
    int[] cumulativeSum = new int[w.length];
    cumulativeSum[0] = w[0];
    for (int i = 1; i<w.length; i++) {
      cumulativeSum[i] = cumulativeSum[i-1]+w[i];
    }
    distribution = cumulativeSum;
    last = distribution[distribution.length - 1];
    generator = new Random(2^32-1);
  }

  public int pickIndex() {
    int random = generator.nextInt(last)+1;
    System.out.println("Generated: "+random);
    int index = Arrays.binarySearch(distribution, random);
    return index<0? -index-1 : index;
  }

  public static void main(String[] args) {
    RandomPickWithWeight solution = new RandomPickWithWeight(new int[]{1, 3, 2});
    for (int i = 0; i<6; i++) {
      System.out.println(solution.pickIndex()+" ");
    }
  }
}
