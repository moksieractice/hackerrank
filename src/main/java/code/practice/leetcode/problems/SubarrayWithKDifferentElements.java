package code.practice.leetcode.problems;

import java.util.HashMap;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SubarrayWithKDifferentElements {

  /**
   * Runtime: 60 ms, faster than 64.79% of Java online submissions for Subarrays with K Different Integers.
   * Memory Usage: 38.1 MB, less than 100.00% of Java online submissions for Subarrays with K Different Integers.
   * @param A
   * @param K
   * @return
   */
  public int subarraysWithKDistinct(int[] A, int K) {
    int count = 0;

    SlidingWindow leftWindow = new SlidingWindow();
    SlidingWindow rightWindow = new SlidingWindow();

    int left = 0;
    int right = 0;

    for (int i = 0; i<A.length; i++) {
      int x = A[i];
      leftWindow.add(x);
      rightWindow.add(x);

      while(leftWindow.unique() > K) {
        leftWindow.remove(A[left++]);
      }
      while (rightWindow.unique() >= K) {
        rightWindow.remove(A[right++]);
      }

      count += right-left;
    }

    return count;
  }

  private static class SlidingWindow {
    private HashMap<Integer, MutableInteger> map = new HashMap<Integer, MutableInteger>();

    public void add(int x) {
      MutableInteger number = map.get(x);
      if (number == null) {
        number = new MutableInteger();
        map.put(x, number);
      }
      number.increment();
    }

    public void remove(int x) {
      MutableInteger number = map.get(x);
      if (number != null) {
        number.decrement();
      }
      if (number.get() == 0) {
        map.remove(x);
      }
    }

    public int unique() {
      return map.size();
    }
  }

  private static final class MutableInteger {
    int num = 0;

    public int increment() {
      return ++num;
    }

    public int get() {
      return num;
    }

    public int decrement() {
      return --num;
    }
  }


  public static void main(String[] args) {
    SubarrayWithKDifferentElements solution = new SubarrayWithKDifferentElements();
    System.out.println(solution.subarraysWithKDistinct(new int[]{1,2,1,2,3}, 2));
    System.out.println(solution.subarraysWithKDistinct(new int[]{1,2,1,3, 4}, 3));
    System.out.println(solution.subarraysWithKDistinct(new int[]{1,2,1,2,3}, 2));
  }
}
