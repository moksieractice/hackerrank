package code.practice.leetcode.problems;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/squares-of-a-sorted-array/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SortedSquares {

  public int[] sortedSquares(int[] A) {
//    Solution 1: Naive, square and sort

//    Solution 2: Linear and In-Place
//    Step 1: find the index i of the last negative number
//    Step 2: reverse teh subarray from 0 to i inclusive
//    Step 3: compute the squares
//    Step 4: merge the subarrays [0, i] and [i+1, n)

    int[] result = new int[A.length];
    int index = -1;
    while(A[index+1] < 0) {
      index++;
    }

//    Step 2: Reverse
    for (int i = 0; i <= index/2; i++) {
      int t = A[i];
      A[i] = A[index-i];
      A[index-i] = t;
    }

//    Step 3: square
    for (int i = 0; i < A.length; i++) {
      A[i] = A[i]*A[i];
    }

//    Step 4: Merge
    int left = 0;
    int right = index+1;

    while (left <= index && right < A.length) {
      if (A[left] <= A[right]) {
        left++;
      }
      else {
        int t = A[left];
        A[left] = A[right];
        A[right] = t;
        
      }
    }


    if (A[index] == 0) {
      result[0] = 0;
      left = index-1;
      right = index + 1;
      index = 1;
    }
    else {
      left = index - 1;
      index = 0;
    }
    index = 0;
    while (left >= 0 && right<A.length) {
      if (-A[left] < A[right]) {
        result[index++] = A[left]*A[left];
        left--;
      }
      else {
        result[index++] = A[right]*A[right];
        right++;
      }
    }

    while(left >= 0) {
      result[index++] = A[left]*A[left];
      left--;
    }
    while(right < A.length) {
      result[index++] = A[right]*A[right];
      right++;
    }
    return result;
  }


  public static void main(String[] args) {
    SortedSquares solution = new SortedSquares();

//    printArray(solution.sortedSquares(new int[]{-4,-1,0,3,10}));
//    printArray(solution.sortedSquares(new int[]{-7,-3,2,3,11}));

  }
}
