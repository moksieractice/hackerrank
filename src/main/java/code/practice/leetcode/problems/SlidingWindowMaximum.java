package code.practice.leetcode.problems;

import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SlidingWindowMaximum {
  public static void main(String[] args) {
    SlidingWindowMaximum swm = new SlidingWindowMaximum();

    System.out.printf("[%s]%n", IntStream.of(swm.max(new int[]{7,2,4}, 5)).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
    System.out.printf("[%s]%n", IntStream.of(swm.max(new int[]{7,2,4}, 2)).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
    System.out.printf("[%s]%n", IntStream.of(swm.max(new int[]{7,2,4}, 3)).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
    System.out.printf("[%s]%n", IntStream.of(swm.max(new int[]{-7,-8,7,5,7,1,6,0}, 2)).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
    System.out.printf("[%s]%n", IntStream.of(swm.max(new int[]{-7,-8,7,5,7,7,5,0}, 2)).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
    System.out.printf("[%s]%n", IntStream.of(swm.max(new int[]{-7,-8,7,5,7,1,6,0}, 3)).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
  }

  private int[] max(final int[] nums, final int k) {
    int[] result = new int[nums.length-k+1];

    LinkedList<Integer> queue = new LinkedList<>();
    for (int i = 0; i < nums.length; i++) {
      if (i >= k && nums[i-k] == queue.getFirst()) {
        queue.removeFirst();
      }

      while(!queue.isEmpty() && nums[i] > queue.getLast()) {
        queue.removeLast();
      }

      queue.add(nums[i]);
      if (i >= k-1) {
        result[i-k+1] = queue.getFirst();
      }
    }

    return result;
  }
}
