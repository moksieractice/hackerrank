package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SkylineProblem {
  public List<List<Integer>> getSkyline(int[][] buildings) {
    List<int[]> points = new ArrayList<>(buildings.length*2);
    for (int[] b : buildings) {
      points.add(new int[]{b[0], b[2], 1});
      points.add(new int[]{b[1], b[2], 0});
    }

    points.sort((a,b) -> {
      if (a[0] == b[0]) {
        if (a[2]==0 && b[2]==0) return a[1]-b[1];
        else  return b[1]-a[1];
      }
      else {
        return a[0]-b[0];
      }
    });

    List<List<Integer>> result = new ArrayList<>();
    TreeMap<Integer, Integer> maxQueue = new TreeMap<>();
    int prevMax = 0;
    maxQueue.put(0, 1);
    for(int[] p : points) {
      if (p[2] == 1) {
        maxQueue.put(p[1], maxQueue.getOrDefault(p[1], 0)+1);
      }
      else if (p[2] == 0) {
        maxQueue.compute(p[1], (k, v) -> {
          if  (v == 1) return null;
          return v-1;
        });
      }
      if (maxQueue.lastKey() != prevMax) {
        prevMax = maxQueue.lastKey();
        result.add(Arrays.asList(p[0], prevMax));
      }
    }

    return result;
  }

  public static void main(String[] args) {
    SkylineProblem solution = new SkylineProblem();

    System.out.println(solution.getSkyline(new int[][]{{2,9,10},{3,7,15},{5,12,12},{15,20,10},{19,24,8}}));
  }
}
