package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ConsecutiveDifference {

  public int[] numsSameConsecDiff(int N, int K) {
    if (N == 0) return new int[]{};
    else if (N == 1) return new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    List<Integer> reference = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
    List<Integer> current = new ArrayList<>(reference.size()*2);
    int length = 2;
    while (length <= N) {
      for (int num : reference) {
        int d = num % 10;
        int shifted = num*10;
        if (d - K >= 0) {
          current.add(shifted + d - K);
        }
        if (d + K <= 9 && K != 0) {
          current.add(shifted + d + K);
        }
      }
      reference = current;
      current = new ArrayList<>();
      length++;
    }
    return reference.stream().mapToInt(Integer::intValue).toArray();
  }

  public static void main(String[] args) {
    ConsecutiveDifference cd = new ConsecutiveDifference();

    printArray(cd.numsSameConsecDiff(3, 7));
    printArray(cd.numsSameConsecDiff(2, 1));
    printArray(cd.numsSameConsecDiff(2, 0));
  }

  private static void printArray(int[] pNums) {
    System.out.println("Printing Nums:: ");
    for(int n : pNums) {
      System.out.print(n + " ");
    }
  }
}
