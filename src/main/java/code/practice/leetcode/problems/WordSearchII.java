package code.practice.leetcode.problems;

import java.util.*;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class WordSearchII {

  public List<String> findWords(char[][] board, String[] words) {
    Map<Character, List<Integer[]>> map = new HashMap<>();
    for (char c : "abcdefghijklmnopqrstuvwxyz".toCharArray()) {
      map.put(c, new ArrayList<>());
    }

    for (int i = 0; i<board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        map.get(board[i][j]).add(new Integer[]{i,j});
      }
    }


    List<String> result = new ArrayList<>();
    for (String word : words) {
      for (Integer[] loc : map.get(word.charAt(0))) {
        if (lookUp(word, loc[0], loc[1], board)) {
          result.add(word);
          break;
        }
      }
    }

    return result;
  }

  boolean lookUp(String word, int i, int j, char[][] board) {
    board[i][j] = '1';
    boolean found = lookUpHelper(word, 1, i, j, board);
    board[i][j] = word.charAt(0);

    return found;
  }

  boolean lookUpHelper(String word, int o, int i, int j, char[][] board) {
    System.out.println(String.format("%s::%d:%d:%d:%c", word, o, i, j, board[i][j]));
    if (o == word.length()) return true;
    char c = word.charAt(o);
    if (i > 0 && c == board[i-1][j]) {
      board[i-1][j] = '1';
      boolean found = lookUpHelper(word, o+1, i-1, j, board);
      board[i-1][j] = c;
      if (found)   return true;
    }


    if (i+1 < board.length && c == board[i+1][j]) {
      board[i+1][j] = '1';
      boolean found = lookUpHelper(word, o+1, i+1, j, board);
      board[i+1][j] = c;
      if (found)    return true;
    }
    if (j > 0 && c == board[i][j-1]) {
      board[i][j-1] = '1';
      boolean found = lookUpHelper(word, o+1, i, j-1, board);
      board[i][j-1] = c;
      if (found)    return true;

    }
    if (j < board[0].length-1 && c == board[i][j+1]) {
      board[i][j+1] = '1';
      boolean found = lookUpHelper(word, o+1, i, j+1, board);
      board[i][j+1] = c;
      if (found)    return true;

    }

    return false;
  }

  public static void main(String[] args) {
    WordSearchII wordSearchII = new WordSearchII();

    PriorityQueue<Integer> queue = new PriorityQueue<>();
    queue.offer(10);
    queue.offer(10);
    queue.offer(1);
    queue.add(11);
    System.out.println(queue.remove(10));
    System.out.println(queue.remove(10));
    System.out.println(queue.remove(10));
    System.out.println(queue.poll());


    System.out.println(wordSearchII.findWords(new char[][]{{'a','b'},{'c','d'}},
      new String[]{"ab","cb","ad","bd","ac","ca","da","bc","db","adcb","dabc","abb","acb"}));
  }
}
