package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * https://leetcode.com/problems/merge-intervals/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MergeIntervals {

  public int[][] merge(int[][] intervals) {
    Arrays.sort(intervals, (a, b) -> a[0]-b[0]);

    int count = 0;
    int[][] merged = new int[intervals.length][2];
    int[] current = intervals[0];
    for (int i = 1; i<intervals.length; i++) {
      if (intervals[i][0] <= current[1]&& intervals[i][1] > current[1]) {
        current[1] = intervals[i][1];
      }
      else {
        merged[count][0] = current[0];
        merged[count++][1] = current[1];
        current = intervals[i];
      }
    }
    merged[count][0] = current[0];
    merged[count++][1] = current[1];

    int[][] result = new int[count][2];
    System.arraycopy(merged, 0, result, 0, count);

    return result;
  }
}
