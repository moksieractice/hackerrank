package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class BasicCalculatorII {

    String[] inputs = new String[]{
            "  3    ",
            "3",
            "3+2*2",
            " 4*2/4",
            " 4*2-4*6*1+4",
            "120/2/3/1/4",
            " 4*2  +4+   6*1-4",
            "1-1+1",
            "1*2-3/4+5*6-7*8+9/10"
    };

    HashMap<String, BiFunction<Integer, Integer, Integer>> operations = new HashMap<String, java.util.function.BiFunction<Integer, Integer, Integer>>(){{
        put("+", (r, l) -> l+r);
        put("-", (r, l) -> l-r);
        put("*", (r, l) -> l*r);
        put("/", (r, l) -> l/r);
    }};

    HashMap<String, Integer> precedence = new HashMap<String, Integer>() {{
        put("+", 0);
        put("-", 0);
        put("*", 1);
        put("/", 1);
    }};

    private int calculateWithStack(final String s) {
        List<String> tokens = parseExpression(s);
        Stack<Integer> values = new Stack<>();
        Stack<String> operators = new Stack<>();
        for (String token : tokens) {
            if (precedence.containsKey(token)) {
                int prec = precedence.get(token);
                while (!operators.empty() && prec <= precedence.get(operators.peek())) {
                    //Left association
                    values.push(operations.get(operators.pop()).apply(values.pop(), values.pop()));
                }
                operators.push(token);
            }
            else {
                values.push(Integer.valueOf(token));
            }
        }

        while (!operators.empty()) {
            values.push(operations.get(operators.pop()).apply(values.pop(), values.pop()));
        }
        return values.pop();
    }

    private List<String> parseExpression(final String s) {
        String expression = s.trim();
        List<String> tokens = new ArrayList<>();
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("\\+|\\-|\\*|/");
        java.util.regex.Matcher matcher = pattern.matcher(expression.trim());
        int index = 0;
        while (matcher.find()) {
            tokens.add(expression.substring(index, matcher.start()).trim());
            tokens.add(expression.substring(matcher.start(), matcher.end()));
            index = matcher.end();
        }
        if (index < expression.length()) {
            tokens.add(expression.substring(index).trim());
        }
        return tokens;
    }

    private void testCalculate(final Function<String, Integer> pEvaluator) {
        for(String input : inputs) {
            System.out.println(String.format("'%s' = %d", input, pEvaluator.apply(input)));
        }
    }

    public static void main(String[] args) {
        BasicCalculatorII solution = new BasicCalculatorII();
        System.out.println("Calculation with stack");
        solution.testCalculate(solution::calculateWithStack);

        System.out.println("\n\nCalculate without stack");
        solution.testCalculate(solution::calculateWithoutStack);
    }

    private Integer calculateWithoutStack(final String s) {
        String[] tokens = parseExpression(s).toArray(new String[1]);
        
        char[] chars = s.trim().toCharArray();
        if (tokens.length == 1) return Integer.parseInt(tokens[0]);
        int index = 1;

        int left = Integer.parseInt(tokens[0]);
        while (index < tokens.length-1) {
            String operator = tokens[index];
            if (operator.equals("*") || operator.equals("/")){
                left = operations.get(operator).apply(Integer.parseInt(tokens[index+1]), left);
                index +=2;
            }
            else {
                int left1 = Integer.parseInt(tokens[index+1]);
                index += 2;
                while (index < tokens.length-1 && precedence.get(tokens[index]) == 1) {
                    left1 = operations.get(tokens[index]).apply(Integer.parseInt(tokens[index+1]), left1);
                    index += 2;
                }
                left = operations.get(operator).apply(left1, left);
            }
        }

        return left;
    }
}
