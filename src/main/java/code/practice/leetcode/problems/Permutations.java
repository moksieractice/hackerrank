package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class Permutations {

  public List<List<Integer>> permute(int[] nums) {
    List<List<Integer>> result = new ArrayList<>();
    computePermutations(nums, 0, new ArrayList<Integer>(nums.length), result, false);
    return result;
  }

  public List<List<Integer>> permuteUnique(int[] nums) {
    List<List<Integer>> result = new ArrayList<>();
    Arrays.sort(nums);
    computePermutations(nums, 0, new ArrayList<Integer>(nums.length), result, true);
    return result;
  }

  public void computePermutations(int[] nums, int index, List<Integer> current, List<List<Integer>> result, boolean unique) {
    if (index == nums.length) {
      result.add(new ArrayList<>(current));
      return;
    }

    for (int i = index; i < nums.length; i++) {

      current.add(nums[i]);
      int t = nums[i];
      nums[i] = nums[index];
      nums[index] = t;

      computePermutations(nums, index+1, current, result, unique);

      t = nums[i];
      nums[i] = nums[index];
      nums[index] = t;

      current.remove(current.size()-1);
      while (unique && i < nums.length-1 && nums[i] == nums[i+1]) {
        i++;
      }
    }
  }


  public List<List<Integer>> permuteII(int[] nums) {
    boolean[] used = new boolean[nums.length];
    List<List<Integer>> result = new ArrayList<>();
    Arrays.sort(nums);
    computePermutationsII(nums, used, new ArrayList<>(), result);
    return result;
  }

  private void computePermutationsII(int[] pNums, boolean[] pUsed, List<Integer> pCurrent, List<List<Integer>> pResult) {
    if (pCurrent.size() == pNums.length) {
      pResult.add(new ArrayList<>(pCurrent));
      return;
    }

    for (int i = 0; i < pNums.length; i++) {
      if (!pUsed[i]) {

        if (i > 0 && pNums[i] == pNums[i-1] && !pUsed[i-1]) {
          continue;
        }

        pCurrent.add(pNums[i]);
        pUsed[i] = true;
        computePermutationsII(pNums, pUsed, pCurrent, pResult);
        pUsed[i] = false;
        pCurrent.remove(pCurrent.size()-1);
      }
    }
  }


  public static void main(String[] args) {
    Permutations permutations = new Permutations();

//    System.out.println(permutations.permute(new int[]{1, 2, 1, 1, 3}));
//    System.out.println(permutations.permuteUnique(new int[]{1, 2, 1, 1, 3}));

    System.out.println(permutations.permute(new int[]{0, 2, 0, 0, 3}));
    System.out.println(permutations.permuteII(new int[]{0, 2, 0, 0, 3}));
  }
}
