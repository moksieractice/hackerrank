package code.practice.leetcode.problems;

/**
 *
 * https://leetcode.com/problems/longest-palindromic-substring/
 *
 *
 *
 * Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
 * Example 1:
 * Input: "babad"
 * Output: "bab"
 * Note: "aba" is also a valid answer.
 *
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class LongestPalindromicSubstring {

  public String longestPalindrome(String s) {
//    return longestPalindrome(s, 0, s.length());


/**
 * Runtime: 6 ms, faster than 87.14% of Java online submissions for Longest Palindromic Substring.
 * Memory Usage: 35.9 MB, less than 100.00% of Java online submissions for Longest Palindromic Substring.
 */
    if (s == null || s.length() == 0) return "";
    if (s.length() == 1) return s;

    int maxI = -1;
    int maxLen = 0;
    for (int i = 0; i<s.length()-1; i++) {
      int len1 = makePalindromicSubstring(s, i, i);
      int len2 = makePalindromicSubstring(s, i, i+1);
      if (len1 >= len2) {
        if (len1 > maxLen) {
          maxLen = len1;
          maxI = i;
        }
      }
      else {
        if (len2 > maxLen) {
          maxLen = len2;
          maxI = i;
        }
      }
    }

    int b;
    int e;
    if ((maxLen & 0x1) == 1) {
      int diff = (maxLen-1)/2;
      b = maxI-diff;
      e = maxI+diff;
    }
    else {
      int diff = (maxLen-2)/2;
      b = maxI-diff;
      e = maxI+1+diff;
    }

    return s.substring(b, e+1);
  }

  public String longestPalindrome(String s, int b, int e) {
    if (isPalindrome(s, b, e)) {
      return s.substring(b, e);
    }

    String left = longestPalindrome(s, b, e-1);
    String right = longestPalindrome(s, b+1, e);

    if (left == null && right == null) {
      return null;
    }

    if (left == null) {
      return right;
    }
    else if (right == null) {
      return left;
    }

    return (left.length() >= right.length())? left : right;
  }

  public boolean isPalindrome(String s, int b, int e) {
    int i = b;
    int j = e-1;
    while (i < j) {
      if (s.charAt(i) != s.charAt(j)) {
        return false;
      }

      i++;
      j--;
    }

    return true;
  }

  public int makePalindromicSubstring(String s, int i, int j) {
    if (s.charAt(i) != s.charAt(j)) return 0;

    while (i >= 0 && j < s.length() && s.charAt(i) == s.charAt(j)) {
      i--;
      j++;
    }

    if (i<0 || j >= s.length() || s.charAt(i) != s.charAt(j)) {
      i++;
      j--;
    }


    return j-i+1;
  }


  public static void main(String[] args) {
    LongestPalindromicSubstring lps = new LongestPalindromicSubstring();

    System.out.println(lps.longestPalindrome("babad"));
    System.out.println(lps.longestPalindrome("cbbd"));
    System.out.println(lps.longestPalindrome("c"));
    System.out.println(lps.longestPalindrome("cddd"));
    System.out.println(lps.longestPalindrome(""));
  }
}
