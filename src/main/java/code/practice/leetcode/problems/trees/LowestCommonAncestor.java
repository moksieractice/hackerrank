package code.practice.leetcode.problems.trees;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class LowestCommonAncestor {
  /**
   * Definition for a binary tree node.
   * public class TreeNode {
   *     int val;
   *     TreeNode left;
   *     TreeNode right;
   *     TreeNode(int x) { val = x; }
   * }
   */
    public TreeNode lowestCommonAncestor(TreeNode root, int p, int q) {
      Deque<TreeNode> pathToP = new LinkedList<>();
      Deque<TreeNode> pathToQ = new LinkedList<>();

      find(root, p, pathToP);
      find(root, q, pathToQ);

      System.out.println(pathToP);
      System.out.println(pathToQ);

      Deque<TreeNode> walker = pathToP.size() > pathToQ.size() ? pathToP : pathToQ;
      int diff = Math.abs(pathToQ.size() - pathToP.size());
      while(diff > 0) {
        System.out.println(walker.poll());
        diff--;
      }

      while(pathToP.peek() != pathToQ.peek()) {
        System.out.print(pathToP.poll()+"   ");
        System.out.println(pathToQ.poll());
      }

      return pathToP.peek();
    }

    void printStack(Deque<TreeNode> list) {
      while (!list.isEmpty()) {
        TreeNode node = list.poll();
        System.out.print(node+"("+node.val+"), ");
      }
      System.out.println("");
    }

    private boolean find(TreeNode node, int target, Deque<TreeNode> path) {
      if (node == null) return false;

      if (node.val == target) {
        path.offer(node);
        return true;
      }

      if (find(node.left, target, path)) {
        path.offer(node);
        return true;
      }

      if (find(node.right, target, path)) {
        path.offer(node);
        return true;
      }

      return false;
    }

  public TreeNode lowestCommonAncestorDirect(TreeNode root, TreeNode p, TreeNode q) {
    if(p == q) return p;
    if (p == null) return q;
    if (q == null) return p;

    TreeNode[] answer = new TreeNode[1];
    int res = findAncestor(root, p, q, answer);

    if (res >= 2) return answer[0];

    return null;
  }

  public int findAncestor(TreeNode current, TreeNode p, TreeNode q, TreeNode[] result) {
    int cur = (p == current || q == current) ? 1 : 0;
    int left = findAncestor(current.left, p, q, result);

    if (cur + left >=  2) {
      result[0] = current;
      return 2;
    }

    int right = findAncestor(current.right, p, q, result);
    int total = cur + left + right;
    if ( total >= 2 && result[0] == null) {
      result[0] = current;
    }

    return total;
  }

  public TreeNode lowestCommonAncestorSimplest(TreeNode root, int p, int q) {
      if ( root == null || root.val == p || root.val == q) return root;

      TreeNode left = lowestCommonAncestorSimplest(root.left, p, q);
      TreeNode right = lowestCommonAncestorSimplest(root.right, p, q);

      if (left == null) return right;
      if (right == null) return left;

      return root;
  }



  public static void main(String[] args) {
    LowestCommonAncestor lca = new LowestCommonAncestor();

      int[] tree = new int[]{3,5,1,6,2,0,8,-1,-1,7,4};
      TreeNode root = buildTree(tree);

    System.out.println(lca.lowestCommonAncestor(root, 6, 4).val);
//    System.out.println(lca.lowestCommonAncestorDirect(root, 6, 4).val);
  }

  private static TreeNode buildTree(int[] tree) {
    TreeNode root = new TreeNode(tree[0]);
    Deque<TreeNode> list = new LinkedList<>();
    list.push(root);
    for(int i = 1; i<tree.length; i+= 2) {
      TreeNode node = list.poll();
      if (tree[i] != -1) {
        node.left = new TreeNode(tree[i]);
        list.offer(node.left);
      }

      if (tree[i+1] != -1) {
        node.right = new TreeNode(tree[i+1]);
        list.offer(node.right);
      }

    }

    return root;
  }

  private static class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }

}