package code.practice.leetcode.problems.trees;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SerializeDeserialize {

  private static final TreeNode sNullNode = new TreeNode(-1);

  // Encodes a tree to a single string.
  public String serialize(TreeNode root) {
    Queue<TreeNode> queue = new LinkedList<>();
    queue.add(root);
    StringBuilder sb = new StringBuilder();
    while (!queue.isEmpty()) {
      TreeNode node = queue.poll();
      if (node != null) {
        sb.append(node.val).append(",");
        queue.add(node.left);
        queue.add(node.right);
      }
      else {
        sb.append("null,");
      }
    }

    sb.setLength(sb.length()-1);

    return String.format("[%s]", sb.toString());
   }

  // Decodes your encoded data to tree.
  public TreeNode deserialize(String data) {
    String[] parts = null;
    if (data.startsWith("[") && data.endsWith("]")) {
      data = data.substring(1, data.length()-1);
    }

    parts = data.split(",");
    TreeNode[] nodes = new TreeNode[parts.length];
    for (int i = 0; i < nodes.length; i++) {
      nodes[i] = "null".equals(parts[i]) ? null : new TreeNode(Integer.parseInt(parts[i]));
    }

    for (int i = 0; i < nodes.length/2; i++) {
      if (nodes[i] != null) {
        nodes[i].left = nodes[i+i+1];
        nodes[i].right = nodes[i+i+2];
      }
    }

    return nodes[0];

  }


  public static class TreeNode {
    int val;
    TreeNode left = null;
    TreeNode right = null;

    TreeNode(int x) {
      val = x;
    }

    TreeNode(int x, TreeNode left, TreeNode right) {
      val = x;
      this.left = left;
      this.right = right;
    }
  }

  public static void main(String[] args) {
    SerializeDeserialize solution = new SerializeDeserialize();

    System.out.println(solution.serialize(solution.deserialize("[1,2,3,null,null,4,5]")));
  }

}
