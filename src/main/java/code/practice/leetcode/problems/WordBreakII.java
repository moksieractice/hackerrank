package code.practice.leetcode.problems;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class WordBreakII {

  /**
   * Mapping and combining. Slow. Received TLE;
   * "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
   * ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
   *
   * @param s
   * @param wordDict
   * @return
   */
  public List<String> wordBreakII(String s, List<String> wordDict) {

    HashMap<Integer, Set<String>> mapStart = new HashMap<>();
    HashMap<Integer, Set<String>> mapEnd = new HashMap<>();
    for (int i = 0; i < s.length(); i++) {
      for (String word : wordDict) {
        int k = word.length()-1;
        if (i >= k && (mapEnd.containsKey(i-k-1) || i==k)) {
          int j = i;
          boolean match = true;
          while(k >= 0) {
            if (s.charAt(j) != word.charAt(k)) {
              match = false;
              break;
            }
            j--;
            k--;
          }
          if (match) {
            mapStart.computeIfAbsent(j+1, p -> new HashSet<String>());
            mapStart.get(j+1).add(word);

            mapEnd.computeIfAbsent(i, p -> new HashSet<String>());
            mapEnd.get(i).add(word);
          }
        }
      }
    }

    List<String> result = new ArrayList<>();
    combine(mapStart, s.length(), new ArrayList<String>(), 0, result);
    return result;
  }

  public void combine(Map<Integer, Set<String>> map, int n, List<String> current, int cN, List<String> result) {
    if (cN == n) {
      result.add(current.stream().collect(Collectors.joining(" ")));
      return;
    }

    if (map.containsKey(cN)) {
      for (String word : map.get(cN)) {
        current.add(word);
        combine(map, n, current, cN+word.length(), result);
        current.remove(current.size()-1);
      }
    }
  }


  /**
   *
   * @param s
   * @param wordDict
   * @return
   */
  public List<String> wordBreak(String s, List<String> wordDict) {
    Set<String> words = new HashSet<>(wordDict);
    int maxLen = 0;
    for (String word : words) {
      if(word.length() > maxLen) {
        maxLen = word.length();
      }
    }

    return findCombine(s, 0, words, maxLen, new HashMap<Integer, List<String>>());
  }

  private List<String> findCombine(String s, int b, Set<String> words, int maxLen, HashMap<Integer, List<String>> map) {
    if (map.containsKey(b)) {
      return map.get(b);
    }

    List<String> current = new ArrayList<>();
    char[] chars = new char[maxLen];
    for (int i = 0; i < maxLen && b+i < s.length(); i++) {
      chars[i] = s.charAt(b+i);
      String sub = new String(chars, 0, i+1);
      if (words.contains(sub)) {
        if (b+sub.length() == s.length()) {
          current.add(sub);
        }
        else {
          for (String p : findCombine(s, b+sub.length(), words, maxLen, map)) {
            current.add(sub +" "+p);
          }
        }
      }
    }
    return current;
  }

  public static void main(String[] args) {
    WordBreakII wordBreakII = new WordBreakII();

    String[]  strings = new String[] {
        "catsanddog",
        "pineapplepenapple",
        "catsandog",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    };
    String[][] words = new String[][]{
        {"cat","cats","and","sand","dog"},
        {"apple", "pen", "applepen", "pine", "pineapple"},
        {"cats", "dog", "sand", "and", "cat"},
        {"a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"}
    };
    for (int i = 0; i< strings.length; i++) {
      System.out.println("************ "+strings[i]+" **********************");
      System.out.println(wordBreakII.wordBreak(strings[i], Arrays.asList(words[i])));
    }

  }

}
