package code.practice.leetcode.problems;

import java.util.HashMap;

/**
 * https://leetcode.com/problems/subarray-sum-equals-k/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SubArraySum {
  public static void main(String[] args) {
    SubArraySum ob = new SubArraySum();
    System.out.println(ob.subarraySum(new int[]{1}, 0));
    System.out.println(ob.subarraySum(new int[]{1, 1, 2, 1, 5}, 4));
  }

  public int subarraySum(int[] nums, int k) {
    int sum = nums[0];
    int count = 0;
    int start = 0;
    for (int end = 1; end < nums.length; end++) {
      sum += nums[end];
      if (sum > k) {
        while (sum > k && start < end) {
          sum -= nums[start];
          start++;
        }
      }

      if (sum == k) {
        count++;
        sum -= nums[start];
        start++;
      }
    }

    return count;
  }

  public int subarraySumBruteForce(int[] nums, int k) {
//    Find all possible ranges and calculate the sum
    int count = 0;
    for (int i = 0; i<nums.length; i++) {
      int sum = 0;
      for (int j = i; j<nums.length; j++) {
        sum += nums[j];
        if (sum == k) {
          count++;
        }
      }
    }
    return count;
  }

  public int subarraySumLinear(int[] nums, int k) {
    int count = 0;

    HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    map.put(0, 1);
    int sum = 0;
    for (int i = 1; i<nums.length; i++) {
      sum += nums[i-1];
      count += map.getOrDefault(sum-k, 0);
      map.put(sum, map.getOrDefault(sum, 0)+1);
    }
    return count;
  }
}