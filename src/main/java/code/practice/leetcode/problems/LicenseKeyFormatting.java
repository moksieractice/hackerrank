package code.practice.leetcode.problems;

import java.util.HashSet;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class LicenseKeyFormatting {

  public String licenseKeyFormatting(String S, int K) {
    String cleaned = S.replaceAll("-", "").toUpperCase();
    int length = cleaned.length();
    if (length == 0) return "";
    int first = length%K;
    if (first == 0) {
      first = K;
    }
    StringBuilder sb = new StringBuilder();
    sb.append(cleaned.substring(0, first));
    for (int i = first; i < cleaned.length(); i+= K) {
      sb.append("-").append(cleaned.substring(i, i+K));
    }

    return sb.toString();
  }

  /**
   * Solution with pattern matching
   * Runtime 48 ms and took 38.7 MB
   *
   * @param emails
   * @return
   */
  public int numUniqueEmails(String[] emails) {
    HashSet<String> uniques = new HashSet<>();
    for (String email : emails ) {
      int index = email.indexOf('@');
      String local = email.substring(0, index);
      String domain = email.substring(index);

      index = local.indexOf('+');
      if (index != -1) {
        local = local.substring(0, index);
      }
      local = local.replaceAll("\\.", "");
      uniques.add(String.format("%s%s", local, domain));
    }
    System.out.println(uniques);
    return uniques.size();
  }

  /**
   * Solution without pattern matching. 585% faster.
   * Runtime 7 ms and took 37.8 MB
   * @param emails
   * @return
   */
  public int numUniqueEmailsWithoutPatternMatching(String[] emails) {
    HashSet<String> uniques = new HashSet<>();
    StringBuilder sb = new StringBuilder();
    for (String email : emails ) {
      int index = email.indexOf('@');
      String local = email.substring(0, index);
      String domain = email.substring(index);

      for (int i = 0; i < local.length(); i++) {
        char c = local.charAt(i);
        if (c == '+') {
          break;
        }
        else if (c != '.') {
          sb.append(c);
        }
      }
      sb.append(domain);
      uniques.add(sb.toString());
      sb.setLength(0);
    }
    System.out.println(uniques);
    return uniques.size();
  }


  /**
   * https://leetcode.com/problems/fruit-into-baskets/
   * @param tree
   * @return
   */
  public int totalFruit(int[] tree) {
    int left = 0;
    int fruit1 = -1;
    int fruit2 = -1;

    int fruit1Count = 0;
    int fruit2Count = 0;

    int maxCount = 0;
    int[] table = new int[tree.length];
    for (int i = 0; i<tree.length; i++) {
      if (tree[i] == fruit1) {
        fruit1Count++;
      }
      else if (tree[i] == fruit2) {
        fruit2Count++;
      }
      else if (fruit1Count == 0) {
        fruit1 = tree[i];
        fruit1Count++;
      }
      else if (fruit2Count == 0) {
        fruit2 = tree[i];
        fruit2Count++;
      }
      else {
        table[left] = i-left;
        maxCount = table[left] > maxCount? table[left] : maxCount;
        while (fruit1Count != 0 && fruit2Count != 0) {
          if (tree[left] == fruit1) {
            fruit1Count--;
          }
          else {
            fruit2Count--;
          }
          table[left] = i-left;
          left++;
        }
        if (fruit1Count == 0) {
          fruit1 = tree[i];
          fruit1Count = 1;
        }
        else {
          fruit2 = tree[i];
          fruit2Count = 1;
        }
      }
    }
    if (table[left] == 0) {
      maxCount = Math.max(maxCount, tree.length-left);
    }

    return maxCount;
  }

  public int totalFruitSimple(int[] tree) {
    int left = 0;
    int maxCount = 0;

    int fruit1 = tree[0];
    int fruit2 = -1;
    for (int i = 1; i<tree.length; i++) {
      if (tree[i] != fruit1 && tree[i] != fruit2) {
        if (fruit1 == -1) {
          fruit1 = tree[i];
        }
        else if (fruit2 == -1) {
          fruit2 = tree[i];
        }
        else {
          int count = i-left;
          maxCount = Math.max(count, maxCount);
          int temp = tree[i-1];
          left = i-1;
          while (tree[left-1] == temp) {
            left--;
          }
          if (temp == fruit1) {
            fruit2 = tree[i];
          }
          else {
            fruit1 = tree[i];
          }
        }
      }
    }
    maxCount = Math.max(maxCount, tree.length-left);

    return maxCount;
  }


  public static void main(String[] args) {
    LicenseKeyFormatting lkf = new LicenseKeyFormatting();
//    System.out.println(lkf.licenseKeyFormatting("5F3Z-2e-9-w", 4));
//    System.out.println(lkf.licenseKeyFormatting("2-5g-3-J", 2));
//    System.out.println(lkf.licenseKeyFormatting("---", 3));

//    System.out.println(lkf.numUniqueEmails(new String[] { "test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"}));

    System.out.println(lkf.totalFruit(new int[] {1,2,1}));
    System.out.println(lkf.totalFruit(new int[] {1,2,2,4,3}));
    System.out.println(lkf.totalFruit(new int[] {0,1,2,2}));
    System.out.println(lkf.totalFruit(new int[] {1,2,3,2,2}));
    System.out.println(lkf.totalFruit(new int[] {3,3,3,1,2,1,1,2,3,3,4}));
    System.out.println(lkf.totalFruit(new int[] {2,3,3,3,1,1,1,4,4,4,4,3}));
    System.out.println(lkf.totalFruit(new int[] {1,2,3,4,5}));

    System.out.println("****************** SIMPLE RESULT ***************************");
    System.out.println(lkf.totalFruitSimple(new int[] {1,2,1}));
    System.out.println(lkf.totalFruitSimple(new int[] {1,2,2,4,3}));
    System.out.println(lkf.totalFruitSimple(new int[] {0,1,2,2}));
    System.out.println(lkf.totalFruitSimple(new int[] {1,2,3,2,2}));
    System.out.println(lkf.totalFruitSimple(new int[] {3,3,3,1,2,1,1,2,3,3,4}));
    System.out.println(lkf.totalFruitSimple(new int[] {2,3,3,3,1,1,1,4,4,4,4,3}));
    System.out.println(lkf.totalFruitSimple(new int[] {1,2,3,4,5}));


  }
}

