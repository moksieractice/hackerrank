package code.practice.leetcode.problems;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class Sort2D {

  public int[][] sort2D(int[][] nums) {

    int n = nums.length;
    int m = nums[0].length;

    int [][] result = new int[n][m];
    int[] rowIndices = new int[n];

    for (int i = 0; i<n; i++) {
      for (int j = 0; j < m; j++) {
        int min = Integer.MAX_VALUE;;
        int minIndex = 0;
        for (int k = 0; k < n; k++) {
          if (rowIndices[k] < m && nums[k][rowIndices[k]] < min) {
            min = nums[k][rowIndices[k]];
            minIndex = k;
          }
        }
        result[i][j] = nums[minIndex][rowIndices[minIndex]];
        rowIndices[minIndex]++;
      }
    }

    return result;
  }


  private void minHeapsort(int[][] nums) {
    int n = nums.length;
    int m = nums[0].length;
    build_min_heap(nums);
    for (int i = 0; i<n; i++) {
      for (int j = 0; j<m; j++) {
        min_heapify(nums, i, j);
      }
    }
  }

  private void build_min_heap(int[][]nums) {
    int n = nums.length;
    int m = nums[0].length;
    for (int i = n/2; i >= 0; i--) {
      for (int j = m-1; j>=0; j--) {
        min_heapify(nums, i, j);
      }
    }
  }

  private void min_heapify(int[][] nums, int i, int j) {
    int leftN = 2*i;
    int leftM = 2*j+1;
    if (leftM >= nums[0].length) {
      leftM -= nums[0].length;
      leftN++;
    }

    int rightN = leftN;
    int rightM = leftM+1;
    if (rightM == nums[0].length) {
      rightN++;
      rightM = 0;
    }

    if (leftN >= nums.length || leftM >= nums[0].length || rightN >= nums.length || rightM >= nums[0].length) return;

    int minN = i;
    int minM = j;

    if (nums[i][j] > nums[leftN][leftM]) {
      minN  = leftN;
      minM = leftM;
    }

    if (nums[minN][minM] > nums[rightN][rightM]) {
      minN = rightN;
      minM = rightM;
    }

    if (minN != i || minM != j) {
      int t = nums[i][j];
      nums[i][j] = nums[minN][minM];
      nums[minN][minM] = t;

      min_heapify(nums, minN, minM);
    }
  }

  private int[][] heapsort(int[][] nums) {
    int n = nums.length;
    int m = nums[0].length;
    int heapSize = m*n;
    build_max_heap(nums, heapSize);
    for (int i = n-1; i>=0; i--) {
      for (int j = m-1; j>=0; j--) {
        int t = nums[0][0];
        nums[0][0] = nums[i][j];
        nums[i][j] = t;
        heapSize--;
        max_heapify(nums, 0, 0, heapSize);
      }
    }

    if (nums[0][0] > nums[0][1]) {
      int t = nums[0][0];
      nums[0][0] = nums[0][1];
      nums[0][1] = t;
    }

    return nums;
  }

  private void build_max_heap(int[][] nums, int heapSize) {
    int n = nums.length;
    int m = nums[0].length;
    for (int i = n/2; i >= 0; i--) {
      for (int j = m-1; j>=0; j--) {
        max_heapify(nums, i, j, heapSize);
      }
    }
  }

  private void max_heapify(int[][] nums, int i, int j, int heapSize) {
    int leftN = 2*i;
    int leftM = 2*j+1;
    if (leftM >= nums[0].length) {
      leftM -= nums[0].length;
      leftN++;
    }

    int rightN = leftN;
    int rightM = leftM+1;
    if (rightM == nums[0].length) {
      rightN++;
      rightM = 0;
    }

//    if (leftN >= nums.length || leftM >= nums[0].length || rightN >= nums.length || rightM >= nums[0].length) return;
    if (rightN*nums[0].length + rightM >= heapSize) return;
    int maxN = i;
    int maxM = j;

    if (nums[i][j] < nums[leftN][leftM]) {
      maxN  = leftN;
      maxM = leftM;
    }

    if (nums[maxN][maxM] < nums[rightN][rightM]) {
      maxN = rightN;
      maxM = rightM;
    }

    if (maxN != i || maxM != j) {
      int t = nums[i][j];
      nums[i][j] = nums[maxN][maxM];
      nums[maxN][maxM] = t;

      max_heapify(nums, maxN, maxM, heapSize);
    }
  }


  public static void main(String[] args) {
    Sort2D solution = new Sort2D();

    print2DArray(solution.sort2D(new int[][]{
        {2, 5, 9},
        {1, 3, 4},
        {9, 9, 9}
    }));


    int[][] nums = new int[][]{
        {2, 5, 9},
        {1, 3, 4},
        {9, 9, 9}
    };
    solution.heapsort(nums);
    print2DArray(nums);

    print2DArray(solution.heapsort(new int[][]{
        { 5, 12, 17, 21, 23},
        { 1,  2,  4,  6,  8},
        { 12, 14, 18, 19, 27},
        { 3,  7,  9, 15, 25}
    }));
  }

  private static void print2DArray(int[][] pNums) {
    for (int[] nums : pNums) {
      System.out.println("");
      for (int num : nums)
        System.out.print(String.format("%4d", num));
    }
    System.out.println("\n");
  }
}
