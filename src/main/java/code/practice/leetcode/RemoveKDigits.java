package code.practice.leetcode;

/**
 * https://leetcode.com/problems/remove-k-digits/
 *
 * Related Problems:
 * https://leetcode.com/problems/monotone-increasing-digits/
 * https://leetcode.com/problems/create-maximum-number/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class RemoveKDigits {

  /**
   * Brute Force. TLE
   * @param num
   * @param k
   * @return
   */
  public String removeKdigits(String num, int k) {
    String num1 = num;
    String min = num+"1234";
    for (int i = 0; i < k; i++) {
      for (int j = 0; j < num1.length(); j++) {
        String str = num1.substring(0, j)+num1.substring(j+1);
        int l = 0;
        while (l < str.length() && str.charAt(l) == '0') {
          l++;
        }
        if (l > 0) {
          str = str.substring(l);
        }

        if (str.length() < min.length()) {
          min = str;
        }
        else if (str.length() == min.length() && str.compareTo(min) < 0) {
          min = str;
        }
      }
      num1 = min;
    }
    return min.isEmpty()? "0" : min;
  }

  /**
   * Optimization ides: smaller prefix of two strings representing numbers result in smaller value
   *
   * Runtime: 3 ms, faster than 91.36% of Java online submissions for Remove K Digits.
   * Memory Usage: 36 MB, less than 100.00% of Java online submissions for Remove K Digits.
   * @param num
   * @param k
   * @return
   */
  public String monotonicStack(String num, int k) {
    if (num.length() <= k) return "0";
    if (k == 0) return num;

    int clip = k;
    char[] result = new char[num.length()];
    int index = 0;
    for (int i = 0; i<num.length(); i++) {
      char c = num.charAt(i);
      while (index > 0 && clip > 0 && result[index-1] > c) {
        index--;
        clip--;
      }
      result[index++] = num.charAt(i);
    }

    index = 0;
    while (index < result.length && result[index] == '0') {
      index++;
    }

    int length = num.length()-k-index;
    if (length <= 0) {
      return "0";
    }

    return new String(result, index, num.length()-k-index);
  }


  public static void main(String[] args) {
    RemoveKDigits solution = new RemoveKDigits();

//    System.out.println("1432219, 3 => "+solution.removeKdigits("1432219", 3));
//    System.out.println("10200, 1 => "+solution.removeKdigits("10200", 1));
//    System.out.println("10, 2 => "+solution.removeKdigits("10", 2));
//    System.out.println("14900002, 1 => "+solution.removeKdigits("14900002", 1));
//    System.out.println("14900002, 2 => "+solution.removeKdigits("14900002", 2));
//    System.out.println("14900002, 3 => "+solution.removeKdigits("14900002", 3));
//    System.out.println("14900002, 4 => "+solution.removeKdigits("14900002", 4));

//    System.out.println("\n\n");
    System.out.println("1432219, 3 => "+solution.monotonicStack("1432219", 3));
    System.out.println("10200, 1 => "+solution.monotonicStack("10200", 1));
    System.out.println("10, 2 => "+solution.monotonicStack("10", 2));
    System.out.println("14900002, 1 => "+solution.monotonicStack("14900002", 1));
    System.out.println("14900002, 2 => "+solution.monotonicStack("14900002", 2));
    System.out.println("14900002, 3 => "+solution.monotonicStack("14900002", 3));
    System.out.println("14900002, 4 => "+solution.monotonicStack("14900002", 4));
    System.out.println("4444444, 4 => "+solution.monotonicStack("4444444", 4));
  }
}
