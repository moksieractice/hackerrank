package code.practice.leetcode.amazon;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * https://leetcode.com/discuss/interview-question/373006
 *
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class FavouriteGenres {

  public Map<String, List<String>> favouriteGenres(Map<String, List<String>> userSongs, Map<String, List<String>> songGenres) {
    Map<String, List<String>> output = new HashMap<>();

    if (userSongs == null || userSongs.isEmpty()) return output;

    if(songGenres == null || songGenres.isEmpty()) {
      userSongs.keySet().forEach(v -> output.put(v, Collections.emptyList()));
      return output;
    }

    Map<String, String> genreSong = new HashMap<>();
    for (Map.Entry<String, List<String>> entry : songGenres.entrySet()) {
      entry.getValue().forEach(v -> genreSong.put(v, entry.getKey()));
    }

    for (Map.Entry<String, List<String>> entry : userSongs.entrySet()) {
      Map<String, Integer> counter = new HashMap<>();
      for (String song : entry.getValue()) {
        String genre = genreSong.get(song);
        counter.put(genre, counter.getOrDefault(genre, 0)+1);
      }
      int max = counter.entrySet().stream().max((e1, e2) -> e1.getValue()-e2.getValue()).get().getValue();
      output.put(entry.getKey(), counter.entrySet().stream().filter(v -> v.getValue() == max).map(v ->  v.getKey()).collect(Collectors.toList()));
    }

    return output;
  }

  public static void main(String[] args) {
    FavouriteGenres fg = new FavouriteGenres();

    Map<String, List<String>> userSongs = new HashMap<>();
    userSongs.put("David", Arrays.asList("song1", "song2", "song3", "song4", "song8"));
    userSongs.put("Emma", Arrays.asList("song5", "song6", "song7"));

    Map<String, List<String>> songGenres = new HashMap<>();
    songGenres.put("Rock", Arrays.asList("song1", "song3"));
    songGenres.put("Dubstep", Arrays.asList("song7"));
    songGenres.put("Techno", Arrays.asList("song2", "song4"));
    songGenres.put("Pop",  Arrays.asList("song5", "song6"));
    songGenres.put("Jazz",    Arrays.asList("song8", "song9"));

    System.out.println(fg.favouriteGenres(userSongs, songGenres));
    System.out.println(fg.favouriteGenres(userSongs, new HashMap<>()));

  }
}
