package code.practice.leetcode.amazon;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * https://leetcode.com/problems/most-common-word/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MostCommonWord {

  /**
   * Runtime: 17 ms, faster than 52.01% of Java online submissions for Most Common Word.
   * Memory Usage: 36.7 MB, less than 92.93% of Java online submissions for Most Common Word.
   *
   * @param paragraph
   * @param banned
   * @return
   */
  public String mostCommonWord(String paragraph, String[] banned) {
    paragraph = paragraph.toLowerCase();
    String[] words = paragraph.split("[?!,.;'\"\\s]+");
    HashSet<String> bannedWords = new HashSet<>(Arrays.asList(banned));
    HashMap<String, Integer> map = new HashMap<>();
    int maxCount = 0;
    String maxWord = null;
    for (String word : words) {
      if (bannedWords.contains(word)) continue;
      int count = map.getOrDefault(word, 0);
      map.put(word, ++count);
      if (count > maxCount) {
        maxCount = count;
        maxWord = word;
      }
    }

    return maxWord;
  }

  public int[] prisonAfterNDays(int[] cells, int N) {
    int occupancy = cells[0];
    for (int i = 1; i < cells.length; i++) {
      occupancy = (occupancy<<1 | cells[i]);
    }

    for (int i = 1; i <= N; i++) {
      occupancy = ~(occupancy<<1 ^ occupancy>>1)&0x7E;
    }

    int[] outCells = new int[cells.length];
    String str = Integer.toBinaryString(occupancy);
    for (int i = str.length()-1, j=outCells.length-1; i>= 0; i--, j--) {
      outCells[j] = str.charAt(i)-'0';
    }
    return outCells;
  }

  /**
   * Runtime: 3 ms, faster than 56.01% of Java online submissions for Prison Cells After N Days.
   * Memory Usage: 36.3 MB, less than 97.62% of Java online submissions for Prison Cells After N Days.
   * @param cells
   * @param N
   * @return
   */
  public int[] prisonAfterNDaysConsideringCycle(int[] cells, int N) {
    int occupancy = cells[0];
    for (int i = 1; i < cells.length; i++) {
      occupancy = (occupancy<<1 | cells[i]);
    }

    HashMap<Integer, Integer> valueToDays = new HashMap<>();
//    valueToDays.put(occupancy, 0);
    int period = -1;
    for (int i = 1; i <= N; i++) {
      occupancy = ~(occupancy<<1 ^ occupancy>>1)&0x7E;
      if (valueToDays.containsKey(occupancy)) {
        period = i-valueToDays.get(occupancy);
        break;
      }

      valueToDays.put(occupancy, i);
    }

    if (period != -1) {
      int rem = N % period;
      rem = rem == 0? period : rem;
      System.out.println(String.format("Period: %d:: Rem: %d", period, rem));
      for (Map.Entry<Integer, Integer> entry : valueToDays.entrySet()) {
        if (entry.getValue() == rem) {
          occupancy = entry.getKey();
          break;
        }
      }
    }
//    System.out.println("********************************");
//    System.out.println(valueToDays);

    int[] outCells = new int[cells.length];
    String str = Integer.toBinaryString(occupancy);
    for (int i = str.length()-1, j=outCells.length-1; i>= 0; i--, j--) {
      outCells[j] = str.charAt(i)-'0';
    }
    return outCells;
  }

  /**
   * Runtime: 1 ms, faster than 99.98% of Java online submissions for Prison Cells After N Days.
   * Memory Usage: 36.6 MB, less than 97.62% of Java online submissions for Prison Cells After N Days.
   * @param cells
   * @param N
   * @return
   */
  public int[] prisonAfterNDaysConsideringCycleDirectly(int[] cells, int N) {
    int occupancy = cells[0];
    for (int i = 1; i < cells.length; i++) {
      occupancy = (occupancy<<1 | cells[i]);
    }

    int remainder = N%14;
    if (remainder == 0) {
      remainder = 14;
    }
    for (int i = 1; i <= remainder; i++) {
      occupancy = ~(occupancy<<1 ^ occupancy>>1)&0x7E;
    }

    int[] outCells = new int[cells.length];
    String str = Integer.toBinaryString(occupancy);
    for (int i = str.length()-1, j=outCells.length-1; i>= 0; i--, j--) {
      outCells[j] = str.charAt(i)-'0';
    }
    return outCells;
  }


  /**
   * Runtime: 55 ms, faster than 32.60% of Java online submissions for K Closest Points to Origin.
   * Memory Usage: 61.9 MB, less than 41.62% of Java online submissions for K Closest Points to Origin.
   * @param points
   * @param K
   * @return
   */
  public int[][] kClosest(int[][] points, int K) {
    int[] dists = new int[points.length];
    Arrays.sort(points, (a,b) -> a[0]*a[0] + a[1]*a[1] - b[0]*b[0] - b[1]*b[1]);
    return Arrays.copyOfRange(points, 0, K);
  }

  /**
   * Runtime: 62 ms, faster than 18.34% of Java online submissions for K Closest Points to Origin.
   * Memory Usage: 62.4 MB, less than 41.00% of Java online submissions for K Closest Points to Origin.
   * @param points
   * @param K
   * @return
   */
  public int[][] kClosestPreDist(int[][] points, int K) {
    int[][] dists = new int[points.length][2];
    for (int i = 0; i<points.length; i++) {
      dists[i] = new int[2];
      int a = points[i][0];
      int b = points[i][1];
      dists[i][0] = a*a + b*b;
      dists[i][1]=i;
    }
    Arrays.sort(dists, (a,b) -> a[0]-b[0]);
    int[][] result = new int[K][2];
    for (int i = 0; i<K; i++) {
      int[] point = points[dists[i][1]];
      result[i] = point;
    }
    return result;
  }

  public static void main(String[] args) {
    MostCommonWord msw = new MostCommonWord();

//    System.out.println(msw.mostCommonWord("Bob hit a ball, the hit BALL flew far after it was hit.", new String[] {"hit"}));
//
//    printArray(msw.prisonAfterNDays(new int[] {0,1,0,1,1,0,0,1}, 7));
//    printArray(msw.prisonAfterNDays(new int[] {0,1,0,1,1,0,0,1}, 28));
//    printArray(msw.prisonAfterNDays(new int[] {1,1,0,1,1,0,1,1}, 6));
//    printArray(msw.prisonAfterNDays(new int[] {0,0,0,1,1,0,1,0}, 574));
//    long start = System.nanoTime();
//    printArray(msw.prisonAfterNDays(new int[] {1,0,0,1,0,0,1,0}, 1000000000));
//    System.out.println("Brute: "+ (System.nanoTime()-start));
//
//
//    printArray(msw.prisonAfterNDaysConsideringCycle(new int[] {0,1,0,1,1,0,0,1}, 7));
//    printArray(msw.prisonAfterNDaysConsideringCycle(new int[] {0,1,0,1,1,0,0,1}, 28));
//    printArray(msw.prisonAfterNDaysConsideringCycle(new int[] {0,1,0,1,1,0,0,1}, 7));
//    printArray(msw.prisonAfterNDaysConsideringCycle(new int[] {0,0,0,1,1,0,1,0}, 574));
//    start = System.nanoTime();
//    printArray(msw.prisonAfterNDaysConsideringCycle(new int[] {1,0,0,1,0,0,1,0}, 1000000000));
//    System.out.println("Cycle: "+ (System.nanoTime()-start));
//
//    printArray(msw.prisonAfterNDaysConsideringCycleDirectly(new int[] {0,0,0,1,1,0,1,0}, 574));
////    printArray(msw.prisonAfterNDays(new int[] {0,0,1,1,1,0,0,0}, 682529619));

    testKClosestPoints();
  }

  private static void testKClosestPoints() {
    MostCommonWord msw = new MostCommonWord();

    long start = System.nanoTime();
    int[][] points = new int[][]{new int[]{1, 3}, new int[] {2, -2}};
    int[][] result = msw.kClosest(points, 1);
    System.out.println(pointsToString(result));

    points = new int[][]{new int[]{3, 3}, new int[] {5, -1}, new int[]{-2, 4}};
    result = msw.kClosest(points, 2);
    System.out.println(pointsToString(result));
    System.out.println(System.nanoTime()-start);

    start = System.nanoTime();
    points = new int[][]{new int[]{1, 3}, new int[] {2, -2}};
    result = msw.kClosestPreDist(points, 1);
    System.out.println(pointsToString(result));

    points = new int[][]{new int[]{3, 3}, new int[] {5, -1}, new int[]{-2, 4}};
    result = msw.kClosestPreDist(points, 2);
    System.out.println(pointsToString(result));
    System.out.println(System.nanoTime()-start);

  }

  private static String pointsToString(int[][] result) {
    StringBuilder sb  = new StringBuilder("[");
    for (int i = 0; i<result.length; i++) {
      sb.append("[").append(result[i][0]).append(",").append(result[i][1]).append("]").append(", ");
    }
    sb.setLength(sb.length()-2);
    sb.append("]");
    return sb.toString();
  }

  private static void printArray(int[] pObjects) {
    StringBuilder sb = new StringBuilder();
    for (int ob : pObjects) {
      sb.append(ob+"").append(", ");
    }
    sb.setLength(sb.length()-2);
    System.out.println(String.format("[%s]", sb.toString()));
  }
}
