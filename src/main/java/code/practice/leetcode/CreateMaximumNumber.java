package code.practice.leetcode;

import programming.practice.hackerrank.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * https://leetcode.com/problems/create-maximum-number/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class CreateMaximumNumber {

  public int[] maxNumber(int[] nums1, int[] nums2, int k) {
    if (k == 0) return new int[]{};
    if (nums1.length + nums2.length < k) throw new IllegalArgumentException("");

    int [] result = new int[k];
    buildMaxNumber(nums1, 0, nums2, 0, k, result, 0);

    return result;
  }

  public int buildMaxNumber(int[] nums1, int s1, int[] nums2, int s2, int k, int[] result, int r) {
    if (nums1.length+nums2.length - s1 - s2 < k) return -1;
    if (s1 >= nums1.length) return nums2[s2];
    if (s2 >= nums2.length) return nums1[s1];
    if (k == 1) return Math.max(nums1[s1], nums2[s2]);

    int r1 = buildMaxNumber(nums1, s1+1, nums2, s2, k-1, result, r+1);
    int r2 = buildMaxNumber(nums1, s1, nums2, s2+1, k-1, result, r+1);
    buildMaxNumber(nums1, s1+1, nums2, s2+1, k, result, r);

    result[r] = Math.max(Math.max(nums1[s1], nums2[s2]), result[r]);
    return result[r];
  }

  public static void main(String[] args) {
      CreateMaximumNumber solution = new CreateMaximumNumber();

      Utils.printArray(solution.maxNumber(new int[]{6, 0 , 7}, new int[] {5, 0, 8}, 3));
  }

}
