package code.practice.leetcode.interview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * https://leetcode.com/discuss/interview-question/357345/uber-phone-screen-sum-to-100
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SumToHundred {

  public List<String> solutionBruteForce() {
    List<String> result = new ArrayList<>();
    AtomicInteger stepsCounter = new AtomicInteger(0);
    dfs("123456789", 0, 0, new StringBuilder(""), result, stepsCounter);
    System.out.println("BruteForce steps performed: "+stepsCounter.get());
    return result;
  }

  private boolean dfs(String pDigits, int pIndex, int pSum, StringBuilder pPath, List<String> pResult, AtomicInteger pCounter) {
    pCounter.incrementAndGet();
    if (pIndex == pDigits.length()) {
      return pSum == 100;
    }

    int num = 0;
    for (int i = pIndex; i < pDigits.length(); i++) {
      num = num*10 + (pDigits.charAt(i)-'0');
      int currentPathLen = pPath.length();

      pPath.append("+").append(num);
      if (dfs(pDigits, i+1, pSum+num, pPath, pResult, pCounter)) {
        pResult.add(pPath.toString());
      }
      pPath.setLength(currentPathLen);

      pPath.append("-").append(num);
      if (dfs(pDigits, i+1, pSum-num, pPath, pResult, pCounter)) {
        pResult.add(pPath.toString());
      }
      pPath.setLength(currentPathLen);
    }

    return false;
  }

//  public int solutionWithMemoization() {
//    List<String> result = new ArrayList<>();
//    AtomicInteger stepsCounter = new AtomicInteger(0);
//    dfsMemoized("123456789", 0, 0, new StringBuilder(""), result, stepsCounter, new HashMap<String, List<String>>());
//    System.out.println("Memoized steps performed: "+stepsCounter.get());
//    return result;
//  }
//
//  private int dfsMemoized(String pDigits, int pIndex, int pSum, AtomicInteger pCounter, HashMap<String, Integer> pMemo) {
//    pCounter.incrementAndGet();
//    if (pIndex == pDigits.length()) {
//      return pSum == 100? 1 : 0;
//    }
//
//    String key = pIndex+"."+pSum;
//    if (!pMemo.containsKey(key)) {
//      return pMemo.get(key);
//    }
//
//    int num = 0;
//    for (int i = pIndex; i < pDigits.length(); i++) {
//      num = num*10 + (pDigits.charAt(i)-'0');
//      int add = dfsMemoized(pDigits, i+1, pSum+num, pCounter, pMemo);
//      int sub = dfsMemoized(pDigits, i+1, pSum-num, pCounter, pMemo);
//
//
//    }
//
//    return false;
//  }

  public static void main(String[] args) {
    SumToHundred ob = new SumToHundred();

    printResult(ob.solutionBruteForce());

    System.out.println("******************* MEMOIZED ***************");
//    printResult(ob.solutionWithMemoization());
  }

  private static void printResult(List<String> pStrings) {
    System.out.println(String.format("Solution size: %d", pStrings.size()));
    for (String path : pStrings) {
      System.out.println(path);
    }
  }
}
