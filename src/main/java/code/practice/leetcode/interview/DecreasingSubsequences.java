package code.practice.leetcode.interview;

/**
 * https://leetcode.com/discuss/interview-question/350233/Google-or-Summer-Intern-OA-2019-or-Decreasing-Subsequences
 *
 * Hint: Optimal solution will be O(NlgN). The Size of the longest non-decreasing subarray is equal to the min number of
 * strictly decreasing partitions.
 *
 * Optimal solution for LIS is explained here
 * https://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class DecreasingSubsequences {

  public int decreasingSubsequence(int... nums) {
    if (nums.length == 0) return 0;

    //Holds the largest/last element of a potential increasing sequence of length i
    int[] largestList = new int[nums.length];
    int size = 0;
    for (int i = 0; i < nums.length; i++) {
      int index = searchLeastGreaterThanTarget(largestList, 0, size, nums[i]);
      largestList[index] = nums[i];
      if (index == size) size++;
    }

    return size;
  }

  private int searchLeastGreaterThanTarget(int[] pList, int pFirst, int pEnd, int pTarget) {
    int low = pFirst;
    int high = pEnd;
    while(low < high) {
      int mid = (low + high)/2;
      if (pList[mid] <= pTarget) {
        low = mid + 1;
      }
      else {
        high = mid;
      }
    }
    return low;
  }

  public static void main(String[] args) {
    DecreasingSubsequences ds = new DecreasingSubsequences();

    System.out.println(ds.decreasingSubsequence(2, 9, 12, 13, 4, 7, 6, 5, 10));
    System.out.println(ds.decreasingSubsequence(5, 2, 4, 3, 1, 6));
    System.out.println(ds.decreasingSubsequence(3,3,3,3));
  }
}
