package code.practice.leetcode.interview.arrays;

import java.util.Scanner;

/**
 * Given an array C of size N-1 and given that there are numbers from 1 to N with one element missing, the missing number is to be found.
 *
 * https://practice.geeksforgeeks.org/problems/missing-number-in-array/0/?ref=self
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MissingNumberInArray {
  public static void main(String[] args) {
    MissingNumberInArray solution = new MissingNumberInArray();
//    GFG solution = new GFG();
    solution.solve();
  }

  private void solve() {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    for (int i = 0; i<n; i++) {
      int numberOfElement = scanner.nextInt();
      System.out.println(readInputAndSolve(numberOfElement, scanner));
    }
  }

  private int readInputAndSolve(int pInputSize, Scanner pScanner) {
    int sum = pScanner.nextInt();
    for (int i = 2; i<pInputSize; i++) {
      sum += pScanner.nextInt();
    }

    return pInputSize*(pInputSize+1)/2 - sum;
  }
}
