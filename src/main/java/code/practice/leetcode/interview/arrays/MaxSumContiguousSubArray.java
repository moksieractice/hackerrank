package code.practice.leetcode.interview.arrays;

import java.util.Scanner;

/**
 * Given an array containing both negative and positive integers. Find the contiguous sub-array with maximum sum.
 *
 * https://practice.geeksforgeeks.org/problems/kadanes-algorithm/0
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MaxSumContiguousSubArray {

  public static void main(String[] args) {
    MaxSumContiguousSubArray solution = new MaxSumContiguousSubArray();
//    GFG solution = new GFG();
    solution.solve();
  }

  private void solve() {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    for (int i = 0; i<n; i++) {
      int numberOfElement = scanner.nextInt();
      System.out.println(readInputAndSolve(numberOfElement, scanner));
    }
  }

  private int readInputAndSolve(int pInputSize, Scanner pScanner) {
    int sum = pScanner.nextInt();
    int maxSum = sum;
    for (int i = 1; i<pInputSize; i++) {
      int x = pScanner.nextInt();
      sum = Math.max(sum+x, x);
      maxSum = Math.max(maxSum, sum);
    }
    return maxSum;
  }


  /**
   * Runtime: 0 ms, faster than 100.00% of Java online submissions for Maximum Subarray.
   * Memory Usage: 37.5 MB, less than 99.53% of Java online submissions for Maximum Subarray.
   * @param nums
   * @return
   */
  public int maxSubArrayNoTemporaryVariable(int[] nums) {
    int sum = 0;
    int maxSum = Integer.MIN_VALUE;
    for (int num : nums) {
      sum = sum+num > num ? sum+num : num;
      maxSum = sum > maxSum ? sum : maxSum;
    }

    return maxSum;
  }

  /**
   * Runtime: 1 ms
   * Memory Usage: 43.7 MB
   * @param nums
   * @return
   */
  public int maxSubArrayWithTemporaryVariable(int[] nums) {
    int sum = 0;
    int maxSum = Integer.MIN_VALUE;
    for (int num : nums) {
      int n = sum+num;
      sum = n > num ? n : num;
      maxSum = sum > maxSum ? sum : maxSum;
    }

    return maxSum;
  }

  /**
   * Runtime: 1 ms
   * Memory Usage: 39.3 MB
   * @param nums
   * @return
   */
  public int maxSubArrayUsingUtilityMaxFunction(int[] nums) {
    int sum = 0;
    int maxSum = Integer.MIN_VALUE;
    for (int num : nums) {
      sum = Math.max(sum+num, num);
      maxSum = Math.max(sum, maxSum);
    }

    return maxSum;
  }
}
