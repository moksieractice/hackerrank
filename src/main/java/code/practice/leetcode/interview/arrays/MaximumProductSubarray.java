package code.practice.leetcode.interview.arrays;

/**
 * https://leetcode.com/problems/maximum-product-subarray/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MaximumProductSubarray {

  /**
   * Brute Force
   *
   * Runtime: 83 ms, faster than 7.36% of Java online submissions for Maximum Product Subarray.
   * Memory Usage: 56.1 MB, less than 6.10% of Java online submissions for Maximum Product Subarray.
   *
   * Improvement:
   *  -- Zeros lower the product to 0 and remain 0 all the time. Avoid zeros by partitioning at zeros
   *  -- Even number of negatives make product positive.
   *  -- Odd number of negatives make product negative. In this case, either product of all numbers upto last negative or
   *  product of all numbers after first negative would give maximum product.
   * @param nums
   * @return
   */
  public int maxProduct(int[] nums) {
    if (nums.length == 0) return 0;
    if (nums.length == 1) return nums[0];

    int prod;
    int maxProd = Integer.MIN_VALUE;
    // int[][] dp = new int[nums.length][nums.length];
    for (int i = 0; i<nums.length; i++) {
      prod = 1;
      for (int j = i; j < nums.length; j++) {
        prod = prod*nums[j];
        // dp[i][j] = prod;
        maxProd = prod > maxProd ? prod : maxProd;
      }
    }

    return maxProd;
  }

  public static void main(String[] args) {
    System.out.println((-1/2)+"::::"+(-1%2));
    System.out.println(8/2/2);
  }
}
