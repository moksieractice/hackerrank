package code.practice.leetcode.interview.arrays;

import java.util.Arrays;
import java.util.Random;

/**
 * https://leetcode.com/problems/search-in-rotated-sorted-array/submissions/
 *
 * variation:
 * https://leetcode.com/problems/search-in-rotated-sorted-array-ii/submissions/
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class BinarySearchRotatingSortedArray {

  /**
   * Runtime: 0 ms, faster than 100.00% of Java online submissions for Search in Rotated Sorted Array.
   * Memory Usage: 39.6 MB, less than 16.35% of Java online submissions for Search in Rotated Sorted Array.
   * @param nums
   * @param target
   * @return
   */
  public int search(int[] nums, int target) {
    int lo = 0;
    int hi = nums.length;
    int mid;
    while (hi > lo) {
      mid = (lo+hi)/2;
      if (nums[mid] == target) return mid;

      if (nums[lo] < nums[mid]) {
        if (target <= nums[mid] && target >= nums[lo]) {
          hi = mid;
        }
        else {
          lo = mid+1;
        }
      }
      else {
        if (target >= nums[mid] && target <= nums[hi-1]) {
          lo = mid+1;
        }
        else {
          hi = mid;
        }
      }
    }

    return -1;
  }


}
