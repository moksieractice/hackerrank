package code.practice.leetcode.interview.arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SortZerosOnesAndTwosArray {
  public static void main(String[] args) throws IOException {
    SortZerosOnesAndTwosArray solution = new SortZerosOnesAndTwosArray();
//    GFG solution = new GFG();
    solution.solve();
  }

  private void solve() throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int n = Integer.parseInt(reader.readLine());
    for (int i = 0; i<n; i++) {
      int numberOfElement = Integer.parseInt(reader.readLine());
      readInputAndSolve2(numberOfElement, reader);
    }
  }

  private void readInputAndSolve2(int pInputSize, BufferedReader pReader) throws IOException {
    int zeroCount = 0;
    int oneCount = 0;
    int twoCount = 0;
    String[] inputs = pReader.readLine().split("\\s+");
    for (int i = 0; i<pInputSize; i++) {
      String x = inputs[i];
      if (x.charAt(0) == '0') zeroCount++;
      else if (x.charAt(0) == '1') oneCount++;
      else twoCount++;
    }

    StringBuffer sb = new StringBuffer(pInputSize*2);
    for (int i = 0; i<zeroCount; i++) {
      sb.append("0 ");
    }
    for (int i = 0; i<oneCount; i++) {
      sb.append("1 ");
    }
    for (int i = 0; i<twoCount; i++) {
      sb.append("2 ");
    }
    System.out.println(sb.toString());
  }

  private void readInputAndSolve(int pInputSize, Scanner pScanner) {
    int[] inputArray = new int[pInputSize];
    int zIndex = 0;
    int oIndex = pInputSize-1;
    for (int i = 0; i<pInputSize; i++) {
      int x = pScanner.nextInt();
      if (x == 0) {
        inputArray[zIndex++] = x;
      }
      else {
        inputArray[oIndex--] = x;
      }
    }

    oIndex = pInputSize-1;
    for (int i = zIndex; i<oIndex; i++) {
      if (inputArray[i] == 2) {
        for (;oIndex>i; oIndex--) {
          if (inputArray[oIndex] == 1) {
            inputArray[i] = 1;
            inputArray[oIndex] = 2;
            break;
          }
        }
      }
    }

    for (int i = 0; i<pInputSize; i++) {
      System.out.print(inputArray[i]+" ");
    }
    System.out.println("");
  }
}
