package code.practice.leetcode.interview.arrays;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SubArrayWithGivenSum {
  public static void main(String[] args) {
    SubArrayWithGivenSum solution = new SubArrayWithGivenSum();
//    GFG solution = new GFG();
    solution.solve();
  }

  private void solve() {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    for (int i = 0; i<n; i++) {
      int numberOfElement = scanner.nextInt();
      int givenSum = scanner.nextInt();
      readInputAndSolve(numberOfElement, givenSum, scanner);
    }
  }

  private void readInputAndSolve(int pInputSize, int pGivenSum, Scanner pScanner) {
    int start = 0;
    int end = 0;
    boolean found = false;
    int[] inputArray = new int[pInputSize];
    int sum = 0;
    for (int i = 0; i<pInputSize; i++) {
      int x = pScanner.nextInt();
      inputArray[i] = x;
      sum += x;
      while (sum > pGivenSum) {
        sum -= inputArray[start];
        start++;
      }
      if (sum == pGivenSum) {
        end = i;
        for (i=i+1;i<pInputSize; i++) {
          pScanner.nextInt();
        }
        found = true;
      }
    }

    if (found) {
      System.out.println(String.format("%d %d", start+1, end+1));
    }
    else {
      System.out.println(-1);
    }

  }
}
