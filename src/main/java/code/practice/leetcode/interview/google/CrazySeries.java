package code.practice.leetcode.interview.google;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class CrazySeries {

  public int findNthElement(int n) {
    int five = 5;
    int seven = 7;
    int eleven = 11;

    int next = 0;
    int count = 50;
    while(count > 0) {
      if (five < seven && five < eleven) {
        next = five;
      }
      else if (seven < eleven) {
        next = seven;
      }
      else {
        next = eleven;
      }

      if (next % 5 == 0) five += 5;
      if (next % 7 == 0) seven += 7;
      if (next % 11 == 0) eleven += 11;
      count--;
      System.out.println(next);
    }

    return next;
  }
  public static void main(String[] args) {
    CrazySeries series = new CrazySeries();
    System.out.println(series.findNthElement(10));
  }
}
