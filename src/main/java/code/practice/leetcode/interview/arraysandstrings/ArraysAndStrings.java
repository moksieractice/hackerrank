package code.practice.leetcode.interview.arraysandstrings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ArraysAndStrings {

    public static void main(String[] args) {
        ArraysAndStrings testSuit = new ArraysAndStrings();

//        testSuit.testSpiralOrder();
//        testSuit.testFourSumCount();
//        testSuit.testFirstMissingPositive();
//        testSuit.testLongestConsecutive();
    }


    private void testSpiralOrder() {
        System.out.println(spiralOrder(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}));
        System.out.println(spiralOrder(new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}));
        System.out.println(spiralOrder(new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}}));
    }


    private List<Integer> spiralOrder(int[][] matrix) {
        if (matrix == null || matrix.length == 0) return new ArrayList<>();

        int size = matrix.length*matrix[0].length;
        List<Integer> result = new ArrayList<>(size);

        int right = matrix[0].length;
        int bottom = matrix.length;
        int left = -1;
        int top = 0;

        int i = 0;
        int j = 0;
        while(result.size() < size) {
            //Move right until right bound reached
            for (; j < right; j++) {
                result.add(matrix[i][j]);
            }
            i++;
            j--;
            right--;

            //Move downward until bottom bound reached
            for (; i < bottom; i++) {
                result.add(matrix[i][j]);
            }
            j--;
            i--;
            bottom--;

            //Move Left until left bound reached
            for (; j > left; j--) {
                result.add(matrix[i][j]);
            }
            i--;
            j++;
            left++;

            //Move Upward until top bound reached
            for (; i > top; i--) {
                result.add(matrix[i][j]);
            }
            j++;
            i++;
            top++;
        }
        return result;
    }

    private void testFourSumCount() {
        System.out.println(fourSumCount(new int[]{1, 2}, new int[]{-2, -1}, new int[]{-1, 2}, new int[]{0, 2}));
    }
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        if (A == null || B == null || C == null || D == null) return 0;
        int size = A.length;
        if (size != B.length || size != C.length || size != D.length) return 0;

        HashMap<Integer, MutableInteger> counter = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < B.length; j++) {
                int sum = A[i]+B[j];
                MutableInteger count = counter.get(sum);
                if (count == null) {
                    counter.put(sum, new MutableInteger(1));
                }
                else {
                    counter.get(sum).number++;
                }
            }
        }

        int result = 0;
        for (int i = 0; i < C.length; i++) {
            for (int j = 0; j < D.length; j++) {
                int sum = C[i]+D[j];
                MutableInteger count = counter.get(-sum);
                if (count != null) {
                    result += count.number;
                }
            }
        }

        return result;
    }

    private void testLongestConsecutive() {
        System.out.println(longestConsecutive(new int[]{1, 1, 1, 1, 1, 1}));
        System.out.println(longestConsecutive(new int[]{100, 4, 200, 1, 3, 2}));
    }

    private int longestConsecutive(final int[] nums) {
        HashMap<Integer, Integer> map = new HashMap<>(nums.length, 1.0f);
        int max = 0;
        for (int n : nums) {
            if (!map.containsKey(n)) {
                int left = map.getOrDefault(n-1, 0);
                int right = map.getOrDefault(n+1, 0);
                int current = left + 1 + right;
                if (left > 0) {
                    map.put(n-left, current);
                }
                if (right > 0) {
                    map.put(n+right, current);
                }
                map.put(n, current);
                max = max<current ? current : max;
            }
        }
        return max;
    }

    private void testFirstMissingPositive() {
        System.out.println(firstMissingPositive(new int[]{1, 2, 0}));
        System.out.println(firstMissingPositive(new int[]{3, 4, -1, 1}));
        System.out.println(firstMissingPositive(new int[]{7, 8, 9, 11, 12}));
        System.out.println(firstMissingPositive(new int[]{1}));
        System.out.println(firstMissingPositive(new int[]{4}));
    }

    public int firstMissingPositive(final int[] nums) {
        int temp;
        for(int i=0; i<nums.length; i++){
            if(i+1 == nums[i]) continue;
            int x = nums[i];
            while(x>=1 && x<=nums.length && x!=nums[x-1]){
                temp = nums[x-1];
                nums[x-1] = x;
                x = temp;
            }
        }
        for(int i=0; i<nums.length; i++){
            if(i+1 != nums[i]) {
                return i+1;
            }
        }
        return nums.length+1;
    }
    public int firstMissingPositive1(final int[] nums) {
        int[] guide = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i]-1;
            if (num >= 0 && num < nums.length) {
                guide[num] = 1;
            }
        }
        for (int i = 0; i<nums.length; i++) {
            if (guide[i] == 0) {
                return (i+1);
            }
        }
        return nums.length+1;
    }

    private class MutableInteger {
        int number;

        public MutableInteger(final int pNumber) {
            number = pNumber;
        }
    }


}
