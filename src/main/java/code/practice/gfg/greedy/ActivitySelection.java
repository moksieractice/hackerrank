package code.practice.gfg.greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ActivitySelection {
  public static void main(String[] args) {
    ActivitySelection solution = new ActivitySelection();
//    GFG solution = new GFG();
    solution.solve();
  }

  private void solve() {
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      int times = Integer.parseInt(reader.readLine());
      for (int i = 0; i<times; i++) {
        int n = Integer.parseInt(reader.readLine());
        readAndSolve(n, reader);
      }
    } catch (IOException pE) {
      throw new RuntimeException(pE);
    }
  }

  private void readAndSolve(int pNumber, BufferedReader pReader) throws IOException {
    Interval[] intervals = new Interval[pNumber];
    String[] starts = pReader.readLine().trim().split("\\s+");
    String[] ends = pReader.readLine().trim().split("\\s+");
    for (int i = 0; i<pNumber; i++) {
      int start = Integer.parseInt(starts[i]);
      int end = i>=ends.length? 500 : Integer.parseInt(ends[i]);
      intervals[i] = new Interval(start, end);
    }

    Arrays.sort(intervals, (a, b) -> Integer.compare(a.mEnd, b.mEnd));
    int count = 1;
    Interval current = intervals[0];
    for (int i = 1; i < pNumber; i++) {
      if (intervals[i].mStart >= current.mEnd) {
        count++;
        current = intervals[i];
      }
    }

    System.out.println(count);
  }

  private void readAndSolve1(int pNumber, BufferedReader pReader) throws IOException {
    int[] startTimes = new int[pNumber];
    int[] endTimes = new int[pNumber];
    Integer[] indexes = new Integer[pNumber];

    String[] starts = pReader.readLine().trim().split("\\s+");
    String[] ends = pReader.readLine().trim().split("\\s+");
    for (int i = 0; i<pNumber; i++) {
      startTimes[i] = Integer.parseInt(starts[i]);
      endTimes[i] = Integer.parseInt(ends[i]);
      indexes[i] = i;
    }


    Comparator.comparingInt(t -> endTimes[(int)t]);
    Arrays.sort(indexes, (a, b) -> Integer.compare(endTimes[(int) a], endTimes[(int) b]));
    int count = 1;
    int currentEnd = endTimes[indexes[0]];
    for (int i = 1; i < pNumber; i++) {
      int index = indexes[i];
      if (startTimes[index] > currentEnd) {
        count++;
        currentEnd = endTimes[index];
      }
    }

    System.out.println(count);
  }


  private class Interval {
    int mStart;
    int mEnd;
    public Interval(int pStart, int pEnd) {
      mStart = pStart;
      mEnd = pEnd;
    }

    public int getEnd() {
      return mEnd;
    }
  }
}