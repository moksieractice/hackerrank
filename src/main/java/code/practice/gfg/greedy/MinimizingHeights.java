package code.practice.gfg.greedy;

/**
 * Given an array A[ ] denoting heights of N towers and a positive integer K, modify the heights of each tower either by
 * increasing or decreasing them by K only once and then find out the minimum difference of the heights of shortest and
 * longest towers.
 *
 * https://practice.geeksforgeeks.org/problems/minimize-the-heights/0
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MinimizingHeights {
}
