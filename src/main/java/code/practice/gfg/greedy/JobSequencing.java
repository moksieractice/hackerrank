package code.practice.gfg.greedy;

import code.practice.gfg.AbstractSolution;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Given a set of n jobs where each job i has a deadline and profit associated to it. Each job takes 1 unit of time to
 * complete and only one job can be scheduled at a time. We earn the profit if and only if the job is completed by its
 * deadline. The task is to find the maximum profit and the number of jobs done.
 *
 * https://practice.geeksforgeeks.org/problems/job-sequencing-problem/0
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class JobSequencing extends AbstractSolution{
  public static void main(String[] args) {
    JobSequencing solution = new JobSequencing();
//    GFG solution = new GFG();
    solution.solve();
  }

  protected void readAndSolve(BufferedReader pReader, int[] pNumbers) throws IOException {
    int pNumber = pNumbers[0];
    Job[] jobs = new Job[pNumber];
    String[] inputs = pReader.readLine().trim().split("\\s+");
    for (int i = 0, j=0; i<pNumber; i++, j+=3) {
      jobs[i] = new Job(Integer.parseInt(inputs[j]), Integer.parseInt(inputs[j+1]), Integer.parseInt(inputs[j+2]));
    }

    Arrays.sort(jobs, (a, b) -> a.mProfit == b.mProfit ? Integer.compare(a.mDeadline, b.mDeadline) : ((a.mProfit < b.mProfit) ? 1 : -1));

    boolean[] scheduled = new boolean[pNumber];
    int count = 0;
    int profit = 0;
    for (Job job : jobs) {
      for (int i = Math.min(job.mDeadline, pNumber-1); i>0; i--) {
        if (scheduled[i] == false) {
          count++;
          profit += job.mProfit;
          scheduled[i] = true;
          break;
        }
      }
    }

    System.out.println(count+" "+profit);
  }

  private class Job {
    int mId;
    int mProfit;
    int mDeadline;

    public Job(int pId, int pDeadline, int pProfit) {
      mId = pId;
      mProfit = pProfit;
      mDeadline = pDeadline;
    }
  }
}
