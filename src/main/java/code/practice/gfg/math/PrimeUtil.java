package code.practice.gfg.math;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class PrimeUtil {
  private int mNumber;
  private ArrayList<Integer> mPrimes = new ArrayList<>(64);

  public PrimeUtil() {
    mPrimes.addAll(Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31));
    mNumber = 33;
  }

  public ArrayList<Integer> primesUpTo(final int pNumber) {
    ensurePrimesUpto(pNumber);
    return mPrimes;
  }

  public int maxPrimeUpTo(final int pNumber) {
    int index = indexOfMaxPrimeUpTo(pNumber);
    return mPrimes.get(index);
  }

  private int search(int pNumber, int pLow, int pHigh) {
    if (pHigh - pLow == 1) return pLow;
    else
    {
      int mid = (pLow + pHigh) / 2;
      int midPrime = mPrimes.get(mid);
      if (midPrime == pNumber) {
        return mid;
      }
      else if (mPrimes.get(mid) > pNumber) {
        return search(pNumber, pLow, mid);
      }
      else {
        return search(pNumber, mid, pHigh);
      }
    }
  }

  public ArrayList<Integer> getPrimes() {
    return mPrimes;
  }

  private void ensurePrimesUpto(int pNumber) {
    if (pNumber > mNumber) {
      boolean isPrime;
      for (int i = mNumber+2; i <= pNumber; i+=2) {
        isPrime = true;
        int checkNumber = (int)Math.sqrt(i) + 1;
        for (int j : mPrimes) {
          if (j<=checkNumber && i%j == 0) {
            isPrime = false;
            break;
          }
        }
        if (isPrime) {
          mPrimes.add(i);
        }
      }

      mNumber = (pNumber & 0x1) == 1? pNumber : pNumber-1;
    }
  }

  public int indexOfMaxPrimeUpTo(int pNumber) {
    ensurePrimesUpto(pNumber);
    return search(pNumber, 0, mPrimes.size());
  }
}
