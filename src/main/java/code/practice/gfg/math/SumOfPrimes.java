package code.practice.gfg.math;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SumOfPrimes {
  public static void main(String[] args) {
//    SumOfPrimes solution = new SumOfPrimes();
////    GFG solution = new GFG();
//    try {
//      solution.solve();
//    } catch (IOException pE) {
//      throw new RuntimeException(pE);
//    }

    int length = 1000001;
    boolean[] notPrimes = new boolean[length];
    int[] sums = new int[length];
    for (int i = 2; i<1001; i++) {
      for (int j = i+i; j<length; j+=i) {
        notPrimes[j] = true;
      }
    }

    sums[0] = 0;
    sums[1] = 0;
    sums[2] = 2;
    int cur = 2;
    for (int i = 3; i<length; i++) {
      if (notPrimes[i]) {
        sums[i] = cur;
      }
      else {
        cur = cur+i;
        sums[i] = cur;
      }
    }
    Scanner scanner = new Scanner(System.in);
    int t = scanner.nextInt();
    for (int i = 0; i<t; i++) {
      int n = scanner.nextInt();
      System.out.println(sums[n]);
    }
  }

  private PrimeUtil mPrimeUtil;
  private HashMap<Integer, Integer> mSumsMap = new HashMap<>();
  private void solve() throws IOException {
    mPrimeUtil = new PrimeUtil();
    mSumsMap.clear();
    mSumsMap.put(2, 2);
    mSumsMap.put(3, 5);

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int times = Integer.parseInt(reader.readLine());
    for (int i = 0; i<times; i++) {
      int n = Integer.parseInt(reader.readLine());
      readInputAndSolve(n);
    }
  }

  private void readInputAndSolve(int pInputSize) {
    int primeIndex = mPrimeUtil.indexOfMaxPrimeUpTo(pInputSize);
    System.out.println(findSum(primeIndex, mPrimeUtil.getPrimes()));
  }

  private int findSum(int pPrimeIndex, ArrayList<Integer> pPrimes) {
    int prime = pPrimes.get(pPrimeIndex);
    int sum = mSumsMap.getOrDefault(prime, 0);
    if (sum == 0) {
      sum = findSum(pPrimeIndex-1, pPrimes)+prime;
      mSumsMap.put(prime, sum);
    }

    return sum;
  }
}
