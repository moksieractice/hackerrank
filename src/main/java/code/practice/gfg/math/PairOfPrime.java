package code.practice.gfg.math;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Print all pairs (sets) of prime numbers (p,q) such that p*q <= n, where n is given number.
 *
 * https://practice.geeksforgeeks.org/problems/pairs-of-prime-number/0#ExpectOP
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class PairOfPrime {
  public static void main(String[] args) {
    PairOfPrime solution = new PairOfPrime();
//    GFG solution = new GFG();
    try {
      solution.solve();
    } catch (IOException pE) {
      throw new RuntimeException(pE);
    }
  }

  private PrimeUtil mPrimeUtil;
  private void solve() throws IOException {

    mPrimeUtil = new PrimeUtil();
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int times = Integer.parseInt(reader.readLine());
    for (int i = 0; i<times; i++) {
      int n = Integer.parseInt(reader.readLine());
      readInputAndSolve(n, reader);
    }
  }

  private void readInputAndSolve(int pInputSize, BufferedReader pReader) {
    int checkNumber = pInputSize/2;

    StringBuffer sb = new StringBuffer();
    ArrayList<Integer> primes = mPrimeUtil.primesUpTo(checkNumber);
    for (int i : primes) {
      if (i > checkNumber) break;
      for (int j : primes) {
        if (i*j <= pInputSize) {
          sb.append(i).append(" ").append(j).append(" ");
        }
        else {
          break;
        }
      }
    }
    System.out.println(sb.toString());
  }
}
