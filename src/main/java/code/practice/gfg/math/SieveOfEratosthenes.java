package code.practice.gfg.math;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Given a number N, calculate the prime numbers upto N using Sieve of Eratosthenes.
 *
 * https://practice.geeksforgeeks.org/problems/sieve-of-eratosthenes/0
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class SieveOfEratosthenes {
  PrimeUtil mPrimeUtil;

  public static void main(String[] args) {
    SieveOfEratosthenes solution = new SieveOfEratosthenes();
//    GFG solution = new GFG();
    try {
      solution.solve();
    } catch (IOException pE) {
      throw new RuntimeException(pE);
    }
  }

  private void solve() throws IOException {
    mPrimeUtil = new PrimeUtil();
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int times = Integer.parseInt(reader.readLine());
    for (int i = 0; i<times; i++) {
      int n = Integer.parseInt(reader.readLine());
      readInputAndSolve(n, reader);
    }
  }

  private void readInputAndSolve(int pInputSize, BufferedReader pReader) {
    if (pInputSize <= 2) {
      System.out.println(pInputSize);
      return;
    }

    StringBuffer sb = new StringBuffer("2");
    for (int n : mPrimeUtil.primesUpTo(pInputSize)) {
      if (n > pInputSize) break;
      sb.append(" ").append(n);
    }
    System.out.println(sb.toString());
  }
}
