package code.practice.gfg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public abstract class AbstractSolution {
  protected void solve() {
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      int times = Integer.parseInt(reader.readLine());
      for (int i = 0; i<times; i++) {
        String[] numbers = reader.readLine().trim().split("\\s+");
        int[] initials = new int[numbers.length];
        for (int j = 0; j<numbers.length; j++) {
          initials[j] = Integer.parseInt(numbers[j]);
        }
        readAndSolve(reader, initials);
      }
    } catch (IOException pE) {
      throw new RuntimeException(pE);
    }
  }

  protected abstract void readAndSolve(BufferedReader pReader, int[] pNumbers) throws IOException;
}
