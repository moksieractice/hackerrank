package code.practice.lessons;

import java.util.function.Function;

public class BinarySearch {

  private void testSqrt() {
    int[] testValues = new int[]{1, 2, 3, 4, 5, 7, 8, 9, 10, 16,  15, 17, 2147395599};
//    int[] testValues = new int[]{2147395599};
    for (int n : testValues) {
      int r = solveSqrt(n);
      int sqR = r*r;
      int sqR1 = (r+1)*(r+1);
      System.out.printf("sqrt(%d): %d; %d, %s, %s%n", n, r, sqR, n>=sqR, n<sqR1);
    }
  }

  private int solveSqrt(final int pNum) {
    return sqrt(pNum, 1, pNum);
  }

  private int sqrt(int n, int b, int e) {
    if (b > e) return 0;
    int m = (b+e)/2;
    if (m == b) return m;
    int sq = n/m;
    if (sq == m) return m;
    else if (sq < m) return sqrt(n, b, m);
    else return sqrt(n, m, e);
  }

  public static void main(String[] args) {
    BinarySearch bs = new BinarySearch();
//    bs.testSqrt();

    bs.testGuessing();
    bs.testMinRotatedArray();
  }

  private void testMinRotatedArray() {
    int[][] numbers = new int[][]{};

    for (int[] input : numbers) {
      System.out.println(findMinRotatedArray(input));
    }
  }

  private void testGuessing() {
    final int[][] numbers = new int[][] {
        {0, 1},
        {1, 1},
        {2, 1},
        {10, 10},
         {10, 6},
        {2126753390, 1702766719}
    };
    for (int[] input : numbers) {
      Function<Integer, Integer> guess = t -> Integer.compare(input[1], t);
      System.out.printf("guess(%d, %d) = %d%n%n", input[0], input[1], guessNumber(input[0], guess));
    }

  }

  private int guessNumber(final int pNumber, final Function<Integer, Integer> pGuess) {
    return (int)find(1, pNumber+1, pGuess);
  }

  private int find(final int pBegin, final int pEnd, final Function<Integer, Integer> pGuess) {
    if (pBegin >= pEnd) return 0;
    int mid = (int)(((long)pBegin+pEnd)/2);
    System.out.printf("begin, end: %d, %d, %d%n", pBegin, pEnd, mid);
    int result = pGuess.apply(mid);
    if (result == 0) return mid;
    else if (result == 1) return find(mid+1, pEnd, pGuess);
    else return find(pBegin, mid, pGuess);
  }

  public int findMinRotatedArray(int[] nums) {

    if (nums.length == 1) {
      return nums[0];
    }

    int left = 0;
    int right = nums.length - 1;

    if (nums[right] > nums[0]) {
      return nums[0];
    }

    while (right >= left) {
      int mid = left + (right - left) / 2;

      if (nums[mid] > nums[mid + 1]) {
        return nums[mid + 1];
      }

      if (nums[mid - 1] > nums[mid]) {
        return nums[mid];
      }

      if (nums[mid] > nums[0]) {
        left = mid + 1;
      } else {
        right = mid - 1;
      }
    }
    return -1;
  }
}
