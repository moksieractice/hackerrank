package code.practice.misc;

import java.util.*;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MaximumOccurrence {

  public static void main(String[] args) {
    MaximumOccurrence solution = new MaximumOccurrence();
    System.out.println(solution.findMaximumOccurrence(new int[]{1, 1, 2},
                                                      new int[]{2, 3, 3})); //2 -> 3 times
    System.out.println(solution.findMaximumOccurrence(new int[]{1, 1, 3, 4},
                                                      new int[]{6, 5, 4, 5})); //4 -> 4 times
    System.out.println(solution.findMaximumOccurrence(new int[]{1, 2, 4, 6},
                                                      new int[]{6, 6, 6, 7})); //6 -> 4 times
    System.out.println(solution.findMaximumOccurrence(new int[]{1, 1, 2, 5, 6, 5, 5},
                                                      new int[]{2, 3, 3, 6, 12, 10, 7})); //6 -> 4 times
    System.out.println(solution.findMaximumOccurrence(new int[]{1, 5, 6, 5, 1, 2},
                                                      new int[]{2, 6, 12, 10, 3, 3})); //2 -> 3 times

  }

  private int findMaximumOccurrence(final int[] pStarts, final int[] pEnds) {
    Arrays.sort(pStarts);
    Arrays.sort(pEnds);
    LinkedHashMap<Integer, Integer> counter = new LinkedHashMap<>();
    int s = 0;
    int e = 0;
    int currentCount = 0;
    int max = 0;
    while(true) {
      if (s < pStarts.length && pStarts[s] <= pEnds[e]) {
        currentCount++;
        counter.put(pStarts[s], currentCount);
        s++;
      }
      else if (e < pEnds.length){
        if (!counter.containsKey(pEnds[e])) {
          counter.put(pEnds[e], currentCount);
        }
        currentCount--;
        e++;
      }
      else {
        break;
      }
      max = max<currentCount ? currentCount : max;
    }

    for (Map.Entry<Integer, Integer> entry : counter.entrySet()) {
      if (entry.getValue() == max) {
        return entry.getKey();
      }
    }

    return -1;
  }

//  private class MutableInteger {
//
//    private int mNumber;
//    public MutableInteger(final int pNumber) {
//      mNumber = pNumber;
//    }
//
//    public int get() {
//      return mNumber;
//    }
//
//    public int increment() {
//      return ++mNumber;
//    }
//
//    public int increment(final int pBy) {
//      mNumber += pBy;
//      return mNumber;
//    }
//
//    public int decrement
//  }
}
