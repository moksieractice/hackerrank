package code.practice;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ManhattanPuzzle {
    private class Square {
        final int x;
        final int y;

        private Square(final int pX, final int pY) {
            x = pX;
            y = pY;
        }
    }

    private String solve(final int pDimension, final int[] pPositions) {
        Square[][] positions = new Square[pDimension][pDimension];
        int distance = 0;
        int k = 0;
        int posX = -1;
        int posY = -1;
        int x, y;
        for (int i = 0; i<pDimension; i++) {
            for (int j = 0; j<pDimension; j++) {
                int current = pPositions[k++];
                if (current == 6) {
                    posX = i;
                    posY = j;
                }
                x = current/pDimension;
                y = current%pDimension;
                distance += Math.abs(x-i)+Math.abs(y-j);
                positions[i][j] = new Square(x, y);
            }
        }

        if (posX < 0 || posY < 0 || distance == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder("");
        char[] moves = {'L', 'R', 'U', 'D'};
        int[] xChange = {0, 0, 1, -1};
        int[] yChange = {1, -1, 0, 0};
        int[] illegalMove = {1, 0, 3, 2};
        int change = Integer.MAX_VALUE;
        int move = -1;
        int previousMOve = -1;
        while (distance > 0) {
            for (int m = 0; m<4; m++) {
                int currentChange = calculateDistance(positions, posX, posY, posX+xChange[m], posY+yChange[m]);
                if (change > currentChange && previousMOve != illegalMove[m]) {
                    change = currentChange;
                    move = m;
                }
            }

            distance += change;
            posX += xChange[move];
            posY += yChange[move];
            sb.append(moves[move]).append(' ');
            change = Integer.MAX_VALUE;
            previousMOve = move;
            move = -1;
            System.out.println(distance);
        }
        return sb.toString();
    }

    private int calculateDistance(final Square[][] pPositions, final int pCurX, final int pCurY, final int pNewX, final int pNewY) {
        if (pNewX >= pPositions.length || pNewX < 0 || pNewY >= pPositions[0].length || pNewY < 0) return Integer.MAX_VALUE;
        Square blank = pPositions[pCurX][pCurY];
        Square nonBlank = pPositions[pNewX][pNewY];
        int previous = Math.abs(blank.x-pCurX)+Math.abs(blank.y-pCurY)+Math.abs(nonBlank.x-pNewX)+Math.abs(nonBlank.y-pNewY);
        int next     = Math.abs(blank.x-pNewX)+Math.abs(blank.y-pNewY)+Math.abs(nonBlank.x-pCurX)+Math.abs(nonBlank.y-pCurY);
        return next - previous;
    }

    public static void main(String[] args) {
        ManhattanPuzzle mp = new ManhattanPuzzle();
        System.out.println(mp.solve(3, new int[]{0, 1, 2, 5, 3, 6, 8, 4, 7}));
    }
}
