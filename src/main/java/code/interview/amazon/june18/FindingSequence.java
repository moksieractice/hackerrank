package code.interview.amazon.june18;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class FindingSequence {
    public static void main(String[] args) {
//        String literatureText = "Rose is a is are a flower red rose are flower";
//        List<String> wordsToExclude = Arrays.asList("is", "are", "a");
        String literatureText = "Jack and Jill went to the market to buy bread and cheese. Cheese is Jack's and Jill's favourite food";
        List<String> wordsToExclude = Arrays.asList("and", "he", "the", "to", "is");
        Pattern pattern = Pattern.compile("\\b([a-zA-Z]+)('s)?\\b");
        Matcher matcher = pattern.matcher(literatureText);

        HashMap<String, AtomicInteger> map = new HashMap<>();
        List<String> words = new ArrayList<>();
        while(matcher.find()) {
            String word = matcher.group(1).toLowerCase();
            if (!wordsToExclude.isEmpty() && !wordsToExclude.contains(word))
                words.add(word);
        }

        int maxFreq = -1;
        for(String word : words) {
            int wordFreq = 1;
            if (map.containsKey(word)) {
                wordFreq = map.get(word).incrementAndGet();
            }
            else {
                map.put(word, new AtomicInteger(wordFreq));
            }
            maxFreq = wordFreq>maxFreq? wordFreq : maxFreq;
        }

        List<String> result = new ArrayList<>();
//        List<Map.Entry<String, AtomicInteger>> intermediate = map.entrySet().stream().sorted((o1, o2) -> Integer.compare(o2.getValue().get(), o1.getValue().get())).collect(Collectors.toList());
//        int max = intermediate.get(0).getValue().get();
//        for(Map.Entry<String, AtomicInteger> entry : intermediate) {
//            if (entry.getValue().get() == max) {
//                result.add(entry.getKey());
//            }
//        }

        for (Map.Entry<String, AtomicInteger> entry : map.entrySet()) {
            if (entry.getValue().get() == maxFreq) {
                result.add(entry.getKey());
            }
        }

        System.out.println(map);
        System.out.println(result);
        result.removeAll(wordsToExclude);
        System.out.println(result);
    }
}
