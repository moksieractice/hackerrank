package code.interview.amazon.june18;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ReOrder {
    public static void main(String[] args) {
        int logFileSize = 4;
        List<String> logFile = new ArrayList<String>() {{
            add("mi2 jog");
            add("wz3 34 54");
            add("a1 alps cow");
            add("x4 45 ");
        }};
        List<String> strings = new ArrayList<>();
        List<String> numbers = new ArrayList<>();
        HashMap<String, String> stringLog = new HashMap<>();
        for (String log : logFile) {
            int index = log.indexOf(' ');
            if (log.length() > index) {
                char c = log.charAt(index+1);
                if (c >= '0' && c<='9') {
                    numbers.add(log);
                }
                else {
                    String id = log.substring(0, index);
                    stringLog.put(id, log.substring(index+1));
                }
            }
        }
        System.out.println(stringLog);
        if (!stringLog.isEmpty()) {
            System.out.println(stringLog.entrySet());
            strings.addAll(stringLog.entrySet().stream()
                    .sorted((o1, o2) -> o1.getValue().compareTo(o2.getValue()))
                    .map(e -> String.format("%s %s", e.getKey(), e.getValue()))
                    .collect(Collectors.toList()));
        }

        strings.addAll(numbers);
        System.out.println(strings);
    }
}
