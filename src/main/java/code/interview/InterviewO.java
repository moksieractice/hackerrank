package code.interview;

import code.practice.leetcode.interview.arraysandstrings.ArraysAndStrings;

import java.util.Arrays;
import java.util.Random;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class InterviewO {
  public static void main(String[] args) {
    InterviewO solution = new InterviewO();

//    solution.reformatString();
//    solution.simulateAddition();
    solution.testMakingAnagram();
  }

  private void testMakingAnagram() {
    String[] inputs = new String[] {
        "listen", "silent",
        "anagram", "nagaram",
        "simple", "symble"
    };

    for (int i = 0; i<inputs.length; i+=2) {
      String word = inputs[i];
      String anagram = inputs[i+1];
      int change = computeChangesToMakeAnagram(word, anagram);
      System.out.println(String.format("You need %d changes to make '%s' as an anagram of '%s'", change, anagram, word));
    }
  }

  private int computeChangesToMakeAnagram(String pWord, String pAnagram) {
    if (pWord == null || pAnagram == null || pWord.length() != pAnagram.length()) {
      return -1;
    }

    char[] word = pWord.toLowerCase().toCharArray();
    char[] anagram = pAnagram.toLowerCase().toCharArray();

    Arrays.sort(word);
    Arrays.sort(anagram);

    int change = 0;
    for (int i = 0; i<word.length; i++) {
      if (word[i] != anagram[i]) {
        change++;
      }
    }
    return change;
  }

  /**
   * Add two integer numbers. A = [3,4,5,6,1] and B= [5,4,1], Return A+B as a a list.
   * Most significant digit is the first digit (0-th) on the list.
   */
  private void simulateAddition() {
    int[] number1 = randomIntArray(10, 6);
    int[] number2 = randomIntArray(10, 6);

    int[] result = doSimulatedAddition(number1, number2);
    System.out.println(String.format("%s + %s = %s", intArrayToString(number1), intArrayToString(number2), intArrayToString(result)));

    int[][] inputs = new int[][]{
      new int[]{3, 4, 5, 6, 1}, new int[] {5, 4, 1},
      new int[]{}, new int[]{9, 9, 9},
      new int[]{9, 9, 9, 1, 4}, new int[]{9, 2}
    };

    for (int i = 0; i<inputs.length; i+=2) {
      number1 = inputs[i];
      number2 = inputs[i+1];
      result = doSimulatedAddition(number1, number2);
      System.out.println(String.format("%s + %s = %s", intArrayToString(number1), intArrayToString(number2), intArrayToString(result)));
    }
  }

  private int[] doSimulatedAddition(int[] pNumber1, int[] pNumber2) {
    int[] smaller;
    int[] larger;
    if (pNumber1.length >= pNumber2.length) {
      larger = pNumber1;
      smaller = pNumber2;
    }
    else {
      larger = pNumber2;
      smaller = pNumber1;
    }

    int[] result = new int[larger.length];
    int carry = 0;
    int i = 1;
    while (smaller.length >= i) {
      int index = larger.length - i;
      int sum = larger[index] + smaller[smaller.length - i] + carry;
      result[index] = sum >= 10? sum-10 : sum;
      carry = sum >= 10? 1 : 0;

      i++;
    }

    while (larger.length >= i) {
      int index = larger.length - i;
      int sum = larger[index] + carry;
      result[index] = sum>=10? sum-10 : sum;
      carry = sum>= 10? 1 : 0;

      i++;
    }

    if (carry == 1) {
      int[] newResult = new int[result.length+1];
      System.arraycopy(result, 0, newResult, 1, result.length);
      newResult[0] = carry;
      result = newResult;
    }

    return result;
  }

  private String intArrayToString(int[] pArray) {
    StringBuilder sb = new StringBuilder("[");
    if (pArray.length > 0) {
      sb.append(pArray[0]);
      for (int i = 1; i<pArray.length; i++) {
        sb.append(", ").append(pArray[i]);
      }
    }
    sb.append("]");

    return sb.toString();
  }

  /**
   * Medical records are stored in a certain way with dashes in between. You are given an integer number 234553353433,
   * Find Medical record number MRN split by dashes in the following format that will be given as input [4,3,4,1].
   * Return “2345-533-5343-3"
   */
  private void reformatString() {
    long recordNumber = 234553353433L;
    int[][] splitInstructions = new int[][]{
      new int[] {4, 3, 4, 1},
        new int[]{2, 6},
        new int[]{},
        new int[]{4, 4, 4, 4}
    };

    for (int i = 0; i<splitInstructions.length; i++) {
      String reFormatted = doReformat(recordNumber, splitInstructions[i]);
      System.out.println(String.format("%d%s -> '%s'", recordNumber, intArrayToString(splitInstructions[i]), reFormatted));
    }
  }

  private String doReformat(long pNumber, int[] pLocations) {
    char[] chars = Long.toString(pNumber).toCharArray();
    if (pLocations == null || pLocations.length == 0) {
      return Long.toString(pNumber);
    }

    int sumOfLocations = 0;
    for (int location : pLocations) {
      sumOfLocations += location;
    }
    if (sumOfLocations > chars.length) {
      return "Locations are not compatible with the number";
    }

    int index = 0;
    StringBuilder sb = new StringBuilder(chars.length+pLocations.length);
    for (int location : pLocations) {
      sb.append(chars, index, location).append('-');
      index += location;
    }
    if (index < chars.length) {
      sb.append(chars, index, chars.length-index);
    }
    else {
      sb.setLength(sb.length()-1);
    }
    return sb.toString();
  }

  private int[] randomIntArray(int pRange, int pLength) {
    Random random = new Random();
    int size = random.nextInt(pLength);

    int[] array = new int[size];
    for (int i = 0; i<size; i++) {
      array[i] = random.nextInt(pRange);
    }

    return array;
  }

}
