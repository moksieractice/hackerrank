package code.interview.google19;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A Purchase has an ID and cost of integer type. A Purchase may be part of another purchase. You need to compute the
 * cost of all the Purchases given to you as a list of Purchases as input.
 *
 * As part of the the design, you have a partially created data structure. It has three defined attributes - an ID of
 * integer type, cost as integer type and parent as Purchase type. The parent attribute denotes that this purchase is
 * part of parent purchase. The Partial implementation is as follows,
 *
 * class Purchase {
 *   int id;
 *   int cost;
 *   Purchase parent;
 * }
 *
 * The input to this system is
 *
 * List<Purchase> input;
 *
 * Write a program to calculate and print the actual cost of each purchase in the input. You are free to add any method
 * of attribute to the Purchase class. Only constraint is that you can not modify the existing attributes or how the
 * Purchase tree is generated. The format of the log is as follows,
 *    ID: <some_id>  cost:<total_cost>
 *
 * Analyze the complexity of your system.
 *
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class PrintPurchases {

  /**
   * The given class
   */
  class Purchase {
    int id;
    int cost;
    Purchase parent;

    //
    int totalCost;
    List<Purchase> children;
  }


  /**
   * One way to solve the problem is to extend the Purchase class to add few more attributes. Or apply some design pattern
   * like AdapterPattern, since it was a system design interview
   *
   * For this case whole tree must be recreated. It is better to ask for clarification and then proceed.
   *
   * The simplest solution is to add the necessary fields and initialize or populate those in the processing method. But
   * this has potential problem - modification of the original purchase tree. For this particular problem this may not
   * cause any trouble but provide some benefits.
   */
  class PurchaseAdapter {
    Purchase mPurchase;
    int totalCost;
    List<PurchaseAdapter> children = new ArrayList<>();

    public PurchaseAdapter(Purchase pPurchase) {
      mPurchase = pPurchase;
    }

    public boolean isRoot() {
      return mPurchase.parent == mPurchase.parent || mPurchase.parent == null;
    }
  }

  public void printCost(List<Purchase> pInput) {
    if (pInput == null) return;
    for (Purchase p : pInput) {
      if (p.parent != null || p.parent != p) {
        p.parent.children.add(p);
      }
    }

    // update the totalcost of each purchase recursively assuming that no purchase has parent outside the input
    for (Purchase p : pInput) {
      if (p.parent == p || p.parent == null) {
        updateCost(p);
      }
    }

    for (Purchase p : pInput) {
      System.out.println(String.format("ID: %d  cost: %d", p.id, p.totalCost));
    }
  }

  private void updateCost(Purchase pPurchase) {
    int tc = 0;
    for (Purchase p : pPurchase.children) {
      updateCost(p);
      tc += p.totalCost;
    }
    pPurchase.totalCost = pPurchase.cost + tc;
  }


  /**
   * Adapter
   */
  public void printCostWithAdapter(List<Purchase> pInput) {
    if (pInput == null) return;

    Map<Purchase, PurchaseAdapter> map = new HashMap<>();
    for (Purchase p : pInput) {
      getAdapter(p, map);
    }

    for (PurchaseAdapter adapter : map.values()) {
      if (adapter.isRoot()) {
        updateCost(adapter, map);
      }
    }

    for (PurchaseAdapter adapter : map.values()) {
      System.out.println(String.format("Id: %d cost: %d", adapter.mPurchase.id, adapter.totalCost));
    }
  }

  private void updateCost(PurchaseAdapter pAdapter, Map<Purchase, PurchaseAdapter> pMap) {
    int tc = 0;
    for (Purchase p : pAdapter.mPurchase.children) {
      PurchaseAdapter pA = getAdapter(p, pMap);
      updateCost(pA, pMap);
      tc += pA.totalCost;
    }

    pAdapter.totalCost = pAdapter.mPurchase.cost + tc;
  }

  private PurchaseAdapter getAdapter(Purchase pPurchase, Map<Purchase, PurchaseAdapter> pMap) {
    if (pMap.containsKey(pPurchase)) return pMap.get(pPurchase);

    PurchaseAdapter adapter = new PurchaseAdapter(pPurchase);
    pMap.put(pPurchase, adapter);
    getAdapter(pPurchase.parent, pMap).children.add(adapter);

    return adapter;
  }
}
