package code.interview.google19;

/**
 * We define the set of independent nodes as the set of nodes none of which share an edge between any two nodes in that
 * set.
 *
 * Now, you are given a tree rooted at the input "root". Each node is associated a value of integer type. Nodes may have
 * same values. You are required to find the independent set with maximum value. You just need to return the sum, not
 * the set
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MaximumSumIndependentNodes {


}
