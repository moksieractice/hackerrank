package code.interview.google19;

/**
 * You are given a Tree rooted at the input "root". Some nodes of the tree are colored. You are required to generate a
 * minimum tree that containing the colored nodes. The root of the generated tree must be the same as the input.
 *
 * Once you do that, can you find the lowest common ancestor of all the colored nodes with minimum steps?
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class MinimumColoredTree {
}
