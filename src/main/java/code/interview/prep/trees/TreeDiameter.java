package code.interview.prep.trees;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class TreeDiameter {

  private class Info {
    int maxDepth;
    int maxPath;
  }

  public int diameterOfBinaryTree(final TreeNode pTree) {
    if (pTree == null) return 0;
    return calculateDiameter(pTree).maxPath;
  }

  private Info calculateDiameter(final TreeNode pTree) {
    Info leftInfo = null;
    Info rightInfo = null;
    if (pTree.left != null) {
      leftInfo = calculateDiameter(pTree.left);
    }

    if (pTree.right != null) {
      rightInfo = calculateDiameter(pTree.right);
    }

    Info info;
    if (rightInfo != null && leftInfo != null) {
      info = leftInfo;
      info.maxPath = Math.max(Math.max(leftInfo.maxPath, rightInfo.maxPath), leftInfo.maxDepth+rightInfo.maxDepth+2);
      info.maxDepth = Math.max(leftInfo.maxDepth, rightInfo.maxDepth) + 1;
    }
    else {
      info = leftInfo == null ? rightInfo : leftInfo;
      if (info != null) {
        info.maxDepth++;
        info.maxPath = Math.max(info.maxDepth, info.maxPath);
      }
      else {
        info = new Info();
      }
    }

    return info;
  }

  private static TreeNode createTreeOne() {
//    Infix 
//    1 2 3 4 6 8 10 7 9 11 17 12 13 14 15 16
    return new TreeNode(1,
      new TreeNode(2,
          new TreeNode(3),
          new TreeNode(4,
              new TreeNode(6,
                  new TreeNode(8,
                      null,
                      new TreeNode(10)),
                  null),
              new TreeNode(7,
                  new TreeNode(9,
                      null,
                      new TreeNode(11,
                          new TreeNode(17),
                          null)),
                  null))),
      new TreeNode(12,
          new TreeNode(13,
              new TreeNode(14,
                  new TreeNode(15),
                  new TreeNode(16)),
              null),
          null));
  }

  private static TreeNode createTreeTwo() {
    return new TreeNode(1,
        new TreeNode(2,
            new TreeNode(4),
            new TreeNode(5)),
        new TreeNode(3));
  }

  public static void main(String[] args) {
    TreeDiameter treeDiameter = new TreeDiameter();
    TreeNode treeOne = createTreeOne();
    TreeNode treeTwo = createTreeTwo();

    System.out.println(treeDiameter.diameterOfBinaryTree(treeOne));
    System.out.println(treeDiameter.diameterOfBinaryTree(treeTwo));


    System.out.println("Given Solution");
    treeDiameter.ans = 1;
    System.out.println(String.format("%d, ans=%d", treeDiameter.depth(treeOne), treeDiameter.ans-1));

    treeDiameter.ans = 1;
    System.out.println(String.format("%d, ans=%d", treeDiameter.depth(treeTwo), treeDiameter.ans-1));
  }

  int ans = 0;
  public int depth(TreeNode node) {
    if (node == null) return 0;
    int L = depth(node.left);
    int R = depth(node.right);
    ans = Math.max(ans, L+R+1);
    return Math.max(L, R) + 1;
  }
}

