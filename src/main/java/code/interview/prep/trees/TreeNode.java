package code.interview.prep.trees;

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class TreeNode {
  final int mValue;
  TreeNode left = null;
  TreeNode right = null;

  public TreeNode(final int pValue) {
    mValue = pValue;
  }

  public TreeNode(final int pValue, final TreeNode pLeft, final TreeNode pRight) {
    mValue = pValue;
    left = pLeft;
    right = pRight;
  }

  public int getValue() {
    return mValue;
  }

  public TreeNode getLeft() {
    return left;
  }

  public TreeNode getRight() {
    return right;
  }
}
